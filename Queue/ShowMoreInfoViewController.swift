//
//  ShowMoreInfoViewController.swift
//  Queue
//
//  Created by Andrew on 2020/3/19.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
import MapKit


class ShowMoreInfoViewController: UIViewController {

    @IBOutlet weak var pharmacyNote: UILabel!
    @IBOutlet weak var pharmacyName: UILabel!
    var pharmacyInfo: JSON?
    let locationManager = CLLocationManager()
    var curLat = Double()
    var curLon = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func BackToPharmacyPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func navigationAddress(_ sender: Any) {
        self.coordinates(forAddress: (pharmacyInfo?["address"].rawString())!) {(location) in
            guard let location = location else { return }
            self.openMapForPlace(lat: location.latitude, long: location.longitude)
        }
    }
    @IBAction func makePhone(_ sender: Any) {
         guard let number = URL(string: "tel://" + (pharmacyInfo?["phone"].rawString())!) else { return }
         UIApplication.shared.open(number)
    }
    
    func coordinates(forAddress address: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            (placemarks, error) in
            guard error == nil else {
                print("Geocoding error: \(error!)")
                completion(nil)
                return
            }
            completion(placemarks?.first?.location?.coordinate)
        }
    }
    
    func openMapForPlace(lat:Double = 0, long:Double = 0, placeName:String = "") {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long

        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }

}

extension ShowMoreInfoViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let curLocation:CLLocationCoordinate2D = manager.location!.coordinate
        curLat = curLocation.latitude
        curLon = curLocation.longitude
    }
}
