//
//  ShowNotifyViewController.swift
//  Queue
//
//  Created by Andrew on 2019/4/17.
//

import UIKit
import UserNotifications

class ShowNotifyAlertViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: ClassLabelSizeClass!
    @IBOutlet weak var contentLabel: ClassLabelSizeClass!
    @IBOutlet weak var neverButton: UIButton!
    @IBOutlet weak var passButton: UIButton!
    @IBOutlet weak var goToAppSettingsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackView.setCustomSpacing(20, after: titleLabel)
        
    }
    
    @IBAction func neverNotify(_ sender: Any) {
        UserDefaults.standard.set("neverNotify", forKey: UserDefaultsID.notifyStatus)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func passNotify(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goToAppSettings(_ sender: Any) {
        let url = URL(string: UIApplication.openSettingsURLString)
        if let url = url, UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                })
                dismiss(animated: true, completion: nil)
            } else {
                UIApplication.shared.openURL(url)
                dismiss(animated: true, completion: nil)
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
