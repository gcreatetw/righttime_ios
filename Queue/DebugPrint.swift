//
//  DebugPrint.swift
//  Queue
//
//  Created by AndrewPeng on 2020/9/21.
//

import Foundation

func dPrint(_ item: Any..., function: String = #function) {
    #if DEBUG
        print("\(function): \(item)")
    #endif
}

func aPrint(_ item: Any..., function: String = #function) {
    #if DEBUG
        print("*---Andrew---*: \(item)")
    #endif
}
