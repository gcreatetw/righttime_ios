//
//  DateUtil.swift
//  Queue
//
//  Created by Andrew on 2019/3/18.
//

import UIKit

class DateUtil: NSObject {
    
    static let shared = DateUtil()
    
    /// 依指定的日期格式取得當前的系統日期
    ///
    /// - Parameter strFormat: 日期格式
    /// - Returns: String
    func getStrSysDate(withFormat strFormat: String) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = strFormat
        dateFormatter.locale = Locale.ReferenceType.system
        dateFormatter.timeZone = TimeZone.ReferenceType.system
        return dateFormatter.string(from: Date())
    }
    
    /// - 定義目前時間區間
    /// - Returns:String (M or A or E)
    func getCurrentTime() -> String {
        switch Int(getCurrentHr())! {
        case 0 ..< 13:
            return "M"
        case 13 ..< 18:
            return "A"
        case 18 ..< 24:
            return "E"
        default:
            return ""
        }
    }
    
    func getStrSysWeekDay() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "eeee"
        dateFormatter.locale = Locale.ReferenceType.system
        dateFormatter.timeZone = TimeZone.ReferenceType.system
        let weekDay = dateFormatter.string(from: Date())
        if weekDay == "Mon" {
            return "monday"
        } else if weekDay == "Tue" {
            return "tuesday"
        } else if weekDay == "Wed" {
            return "wednesday"
        } else if weekDay == "Thu" {
            return "thursday"
        } else if weekDay == "Fri" {
            return "friday"
        } else if weekDay == "Sat" {
            return "saturday"
        } else if weekDay == "Sun" {
            return "sunday"
        } else {
            return weekDay
        }
    }
    
    func getStrFromDate(strFormat: String, date: Date) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = strFormat
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.republicOfChina)
        return dateFormatter.string(from: date)
    }
    
    func getStringFromDate(date: Date) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd HH:mm"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.republicOfChina)
        return dateFormatter.string(from: date)
    }
    
    func getDateFromString() -> Date {
        let strDate = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd HH:mm"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let tmp: String = dateFormatter.string(from: strDate)
        return dateFormatter.date(from: tmp)!
    }
    
    /// 依傳入的日期格式、日期轉換成日期
    ///
    /// - Parameters:
    ///   - strFormat: 日期格式
    ///   - strDate: 日期
    /// - Returns: Date
    func getDateFromString(withFormat strFormat: String, strDate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strFormat
        dateFormatter.locale = Locale.ReferenceType.system
        dateFormatter.timeZone = TimeZone.ReferenceType.system
        return dateFormatter.date(from: strDate)!
    }
    
    /// 取得民國日期
    ///
    /// - Returns: String
    func getRepublicOfChinaDate() -> String {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "民國 yyy 年 MM 月 dd 日 HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.republicOfChina)
        return dateFormatter.string(from: date)
    }
    
    func getCurrentHr() -> String {
        let strDate = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        return dateFormatter.string(from: strDate)
    }
    
    func getCurrentHHmm() -> String {
        let strDate = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        return dateFormatter.string(from: strDate)
    }
    
}


