//
//  ShowLocationAlertViewController.swift
//  Queue
//
//  Created by Andrew on 2019/5/20.
//

import UIKit

class ShowLocationAlertViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titlelabel: ClassLabelSizeClass!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stackView.setCustomSpacing(20, after: titlelabel)
    }
    
    @IBAction func passClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goToSetting(_ sender: Any) {
        let url = URL(string: UIApplication.openSettingsURLString)
        if let url = url, UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                })
                dismiss(animated: true, completion: nil)
            } else {
                UIApplication.shared.openURL(url)
                dismiss(animated: true, completion: nil)
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
