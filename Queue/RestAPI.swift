//
//  RestAPI.swift
//  Queue
//
//  Created by Andrew on 2019/3/28.
//

import Foundation
import Alamofire
import SwiftyJSON

class RestAPI {
    
    static let shared = RestAPI()
    
    /// - Remark: tempURL = "http://demoweb.gcreate.com.tw:8080/numberCallService/api/"
    /// - Remark: TestURL = https://apptest.right-time.com.tw:8443/api/
    /// - Remark: ReleaseURL =  https://app.right-time.com.tw:443/api/
    var url = "https://app.right-time.com.tw:443/api/"
    
    var latestNewsURL = "https://graph.facebook.com/v4.0/644371786013960/posts?fields=picture.type(large),message,created_time&access_token="
    
    var fbToken = "EAAJNwmizULoBAIuaPRO6bTxOo1cZBZC7Oe4PFlOajXv5JJRAuJtVFZAjcP9O0AE8cQNWtfSl2R1Ub99wReYWvW5syVaM9AcxX4qrELGHWlqCNIwZBmTjAWbU92GC3hE6vvNZCpBfh3ZBQTWkphGlud9xZAxJhvT6zuPFftpiSqrQ5m89XpzruSFyur0HhoWHqra0oTyfSEdZAJlD15on5SXf6TmzZCDrXnOArZC1wOkbi8KAZDZD"
    
    //MARK: - Pharmacy List
    func getPharmacyList(city_name: String, district_name: String, completion: @escaping  (JSON) -> Void) {
        let parameters: [String: Any] = [
            "city_name" : city_name,
            "district_name" : district_name
        ]
        Alamofire.request(url+"pharmacy/list", method: .post, parameters: parameters, encoding: JSONEncoding.default)
        .responseJSON { (response) in
            switch response.result {
            case .success(let value):
                completion(JSON(value))
            case .failure:
                completion(JSON.null)
            }
        }
    }
    
    //MARK: - tracking clinic FB page posts
    func getLatestNews(clinic_FB_ID: String, completion: @escaping  (JSON) -> Void) {        Alamofire.request("https://graph.facebook.com/v2.7/\(clinic_FB_ID)/posts?fields=full_picture,message,created_time&access_token="+fbToken, method: .get, encoding: JSONEncoding.default)
        .responseJSON { (response) in
            print("response", response)
            
            switch response.result {
            case .success(let value):
                completion(JSON(value))
            case .failure:
                completion(JSON.null)
            }
        }
    }
    //MARK: - tracking room info
    func getTrackingInfo(roomNumArray: [Int16], completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any] = ["rooms" : roomNumArray]
        Alamofire.request(url+"clinicRoom/", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - update clinic list
    func getClinicList(city_id: String, type_id: String, center: Bool, curLat: Double, curLon: Double, completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any]
        if center {
            parameters = [
                "city_id" : "",
                "type_id" : type_id,
                "center"  : ["lat": curLat, "lng": curLon, "distance": 10000]
            ]
        } else {
            parameters = [
                "city_id" : city_id,
                "type_id" : type_id,
                "center"  : ""
            ]
        }
        Alamofire.request(url+"clinic/list", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - get Favorite list
    func getFavoriteList(clinic_id: [String], completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any] = [
            "city_id" : "",
            "type_id" : "",
            "clinic_id_list": clinic_id,
            "center"  : ""
        ]
        Alamofire.request(url+"clinic/list", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - update city list
    func getCityList(completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request(url+"city/list", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - update type list
    func getTypeList(completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request(url+"clinicType/list", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - update clinic info
    func getClinicInfo(clinicID: String, completion: @escaping  (JSON) -> Void){
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request(url+"clinic/"+clinicID+"/info", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - post tracking to service
    func postTrackingInfo(roomID: String, deviceToken: String, targetNum: String, notifyNum: String, period: String, date: String, completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any] = [
            "room_id": roomID,
            "deviceToken": deviceToken,
            "targetNum": targetNum,
            "notifyNum": notifyNum,
            "period": period,
            "date": date
        ]
        Alamofire.request(url+"setDeviceNotify", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - get version from RightTime
    func getRightTimeVersion(completion: @escaping  (JSON) -> Void) {
        Alamofire.request(url+"version", method: .get, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - post version from store
    func getStoreVersion(completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request("https://itunes.apple.com/lookup?bundleId=tw.gCreate.quee", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - post recommend
    func postRecommend(name: String, phone: String, address: String, completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any] = [
            "title":"",
            "content":"診所名稱:"+name+"\n診所電話:"+phone+"\n診所地址:"+address,
            "type":"recommend",
            "email":""
        ]
        Alamofire.request(url+"feedBack/add", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    //MARK: - post suggest
    func postSuggest(title: String, content: String, email: String, completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any] = [
            "title":title,
            "content":content,
            "type":"suggest",
            "email":email
        ]
        Alamofire.request(url+"feedBack/add", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    func postFavoriteList(clinics: [String], completion: @escaping (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        let parameters: [String: Any] = ["clinics": clinics]
        Alamofire.request(url+"setDeviceClinicFavorite", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    func getBannerListcompletion(completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request(url+"appFile/banner/list", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    func getHeHoCategory(completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request("https://app.right-time.com.tw:443/api/articleType/list", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    func getHeHoArticle(type_id: String, before: String, completion: @escaping  (JSON) -> Void) {
        let headers = ["Device-ID": DEVICE_UUID!, "FCM-token": firebaseToken]
        Alamofire.request("https://app.right-time.com.tw:443/api/article/list?type_id="+type_id+"&day_before="+before, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    completion(JSON(value))
                case .failure:
                    completion(JSON.null)
                }
        }
    }
    
    
}
