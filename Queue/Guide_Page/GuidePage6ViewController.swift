//
//  GuidePage6ViewController.swift
//  Queue
//
//  Created by Andrew on 2019/5/22.
//

import UIKit

class GuidePage6ViewController: UIViewController {
    
    @IBOutlet weak var goToHomePageButton: ClassButtonSizeClass!
    
    override func viewDidLayoutSubviews() {
        goToHomePageButton.layer.cornerRadius = goToHomePageButton.frame.size.height / 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func goToHomePage(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.tabBarPage) as?
            TabBarViewController {
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
//            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
