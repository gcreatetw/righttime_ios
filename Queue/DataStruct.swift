//
//  DataStruct.swift
//  Queue
//
//  Created by Andrew on 2019/6/4.
//

import Foundation
import UIKit
import StoreKit

struct clinicsType {
    var imageView: String
    var title: String
    var content: String
    var city: String
    var type: String
}

struct clinicTimeTableType {
    var imageView: String
    var title: String
    var content: String
}

struct doctorCollection {
    var image: UIImage?
    var doctorName: String = ""
}

struct doctorInfo {
    var doctorName: String = ""
    var doctorExperience: String = ""
    var doctorSkill: String = ""
}

struct SegueID {
    static let backToHomePage = "backToHomePage"
    static let backToClinicListPage = "backToClinicListPage"
}

struct CellID {
    static let apptTodayCell = "apptTodayCell"
    static let apptAfterTodayTableCell = "apptAfterTodayTableCell"
    static let apptAfterTodayCollectionCell = "apptAfterTodayCollectionCell"
    static let clinicsTypeCell = "clinicsTypeCell"
    static let clinicsListCell = "clinicsListCell"
    static let clinicTimeTableCell = "clinicTimeTableCell"
    static let clinicRoomsListCell = "clinicRoomsListCell"
    static let favoriteClinicsListCell = "favoriteClinicsListCell"
    static let doctorHeadShotCell = "doctorHeadShotCell"
    static let doctorsHeadShotCell = "doctorsHeadShotCell"
    static let doctorSkillCell = "doctorSkillCell"
    static let doctorExperienceCell = "doctorExperienceCell"
    static let headerCell = "headerCell"
    static let latestNewsWithPicCell = "latestNewsWithPicCell"
    static let latestNewsWithoutPicCell = "latestNewsWithoutPicCell"
    static let scheduleImgCell = "scheduleImgCell"
    static let pharmacyListCell = "pharmacyListCell"
}

struct StoryboardID {
    static let guidePage1 = "guidePage1"
    static let guidePage2 = "guidePage2"
    static let guidePage3 = "guidePage3"
    static let guidePage3_1 = "guidePage3_1"
    static let guidePage4 = "guidePage4"
    static let guidePage5 = "guidePage5"
    static let guidePage6 = "guidePage6"
    
    static let tabBarPage = "tabBarPage"
    static let homePage = "homePage"
    static let clinicListPage = "clinicListPage"
    static let clinicPickerPage = "clinicPickerPage"
    static let clinicDetailPage = "clinicDetailPage"
    static let apptSettingsPage = "apptSettingsPage"
    static let favoriteClinicsPage = "favoriteClinicsPage"
    static let teachingPage = "teachingPage"
    static let clinicIntroductionPage = "clinicIntroductionPage"
    static let reservationPage = "reservationPage"
    static let memberCentrePage = "memberCentrePage"
    static let memberAlterPage = "memberAlterPage"
    static let managementPage = "managementPage"
    static let regdRecordPage = "regdRecordPage"
    static let chooseAlertPage = "chooseAlertPage"
    static let apptPickerPage = "apptPickerPage"
    static let relativesPage = "relativesPage"
    static let signInPage = "signInPage"
    static let birthPickerPage = "birthPickerPage"
    static let qRcodePage = "qRcodePage"
    static let schedulePage = "schedulePage"
    static let pharmacyListPage = "pharmacyListPage"
    static let pharmacyPickerPage = "pharmacyPickerPage"
    static let moreInfoPage = "moreInfoPage"
    
    /// - Remark: AlertID
    static let alertPage = "alertPage"
    static let feedbackCompletePage = "feedbackCompletePage"
    static let notifyAlertPage = "notifyAlertPage"
    static let versionAlertPage = "versionAlertPage"
    static let locationAlertPage = "locationAlertPage"
    static let serverStopPage = "serverStopPage"
}

struct UserDefaultsID {
    static let isNotFirstOpenApp = "isNotFirstOpenApp"
    static let isCenter = "isCenter"
    static let cityID = "cityID"
    static let typeID = "typeID"
    static let notifyStatus = "notifyStatus"
    static let numberOfTimesLaunchedkey = "numberOfTimesLaunched"
    static let pharmacyCity = "pharmacyCity"
    static let pharmacyDistrict = "pharmacyDistrict"
}

struct StoreKitHelper {
    static func displayStoreKit() {
        guard let currentVersion = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String else {
            return
        }
        
        let lastVersionPromptedForReview = UserDefaults.standard.string(forKey: "lastVersion")
        /// - Note:print("currentVersion",currentVersion,"lastVersionPromptedForReview",lastVersionPromptedForReview)
        let numberOfTimesLaunched: Int = UserDefaults.standard.integer(forKey: UserDefaultsID.numberOfTimesLaunchedkey)
        
        if numberOfTimesLaunched > 3 && currentVersion != lastVersionPromptedForReview {
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
                UserDefaults.standard.set(currentVersion, forKey: "lastVersion")
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    static func incrementNumberOfTimesLaunched() {
        let numberOfTimesLaunched: Int = UserDefaults.standard.integer(forKey: UserDefaultsID.numberOfTimesLaunchedkey) + 1
        UserDefaults.standard.set(numberOfTimesLaunched, forKey: UserDefaultsID.numberOfTimesLaunchedkey)
        /// - Note: print("numberOfTimesLaunched",numberOfTimesLaunched)
    }
}

struct PharmacyList {
    static let addressDict = [
        "基隆市01": ["中正區", "七堵區", "暖暖區", "仁愛區", "中山區", "安樂區", "信義區"],
        "台北市02": ["松山區", "大安區", "大同區", "中山區", "內湖區", "南港區", "士林區", "北投區", "信義區", "中正區", "萬華區", "文山區"],
        "新北市03": ["板橋區", "三重區", "永和區", "中和區","新店區", "新莊區", "樹林區","鶯歌區", "三峽區", "淡水區", "汐止區", "瑞芳區", "土城區", "蘆洲區", "五股區", "泰山區", "林口區", "深坑區", "石碇區", "三芝區", "石門區", "八里區", "雙溪區", "金山區", "萬里區"],
        "桃園市04": ["桃園區", "中壢區", "大溪區", "楊梅區", "蘆竹區", "大園區", "龜山區", "八德區", "龍潭區", "平鎮區", "新屋區", "觀音區", "復興區"],
        "新竹市05": ["東區", "北區", "香山區"],
        "新竹縣06": ["關西鎮", "新埔鎮", "竹東鎮", "竹北市", "湖口鄉", "新豐鄉", "芎林鄉", "寶山鄉", "北埔鄉", "尖石鄉"],
        "苗栗縣07": ["苗栗市", "苑裡鎮", "通霄鎮", "竹南鎮", "頭份市", "後龍鎮", "卓蘭鎮", "大湖鄉", "公館鄉", "銅鑼鄉", "頭屋鄉", "三義鄉", "西湖鄉", "三灣鄉"],
        "台中市08": ["豐原區", "東勢區", "大甲區", "清水區", "沙鹿區", "梧棲區", "后里區", "神岡區", "潭子區", "大雅區", "新社區", "外埔區", "大安區", "烏日區", "大肚區", "龍井區", "霧峰區", "太平區", "大里區", "中區", "東區", "西區", "南區", "北區", "西屯區", "南屯區", "北屯區", "石岡區", "和平區"],
        "彰化縣09": ["彰化市", "鹿港鎮", "和美鎮", "北斗鎮", "員林市", "溪湖鎮", "田中鎮", "二林鎮", "線西鄉", "伸港鄉", "福興鄉", "秀水鄉", "花壇鄉", "芬園鄉", "大村鄉", "埔鹽鄉", "埔心鄉", "永靖鄉", "社頭鄉", "二水鄉", "田尾鄉", "埤頭鄉", "芳苑鄉", "大城鄉", "竹塘鄉", "溪州鄉"],
        "南投縣10": ["南投市", "埔里鎮", "草屯鎮", "竹山鎮", "集集鎮", "名間鄉", "中寮鄉", "魚池鄉", "國姓鄉", "水里鄉", "信義鄉", "仁愛鄉"],
        "雲林縣11": ["斗六市", "斗南鎮", "虎尾鎮", "西螺鎮", "土庫鎮", "北港鎮", "古坑鄉", "大埤鄉", "莿桐鄉", "林內鄉", "二崙鄉", "崙背鄉", "麥寮鄉", "東勢鄉", "褒忠鄉", "臺西鄉", "元長鄉", "四湖鄉", "口湖鄉", "水林鄉"],
        "嘉義市12": ["東區", "西區"],
        "嘉義縣13": ["朴子市", "布袋鎮", "大林鎮", "民雄鄉", "溪口鄉", "新港鄉", "六腳鄉", "東石鄉", "義竹鄉", "鹿草鄉", "太保市", "水上鄉", "中埔鄉", "竹崎鄉", "梅山鄉", "番路鄉", "阿里山鄉"],
        "台南市14": ["新營區", "鹽水區", "白河區", "麻豆區", "佳里區", "新化區", "善化區", "學甲區", "柳營區", "後壁區", "下營區", "六甲區", "官田區", "七股區", "將軍區", "新市區", "安定區", "仁德區", "歸仁區", "關廟區", "永康區", "東區", "南區", "中西區", "北區", "安南區", "安平區", "東山區", "大內區", "西港區", "北門區", "山上區", "玉井區", "楠西區", "左鎮區"],
        "高雄市15": ["鹽埕區", "鼓山區", "左營區", "楠梓區", "三民區", "新興區", "前金區", "苓雅區", "前鎮區", "旗津區", "小港區", "鳳山區", "岡山區", "旗山區", "美濃區", "林園區", "大寮區", "大樹區", "仁武區", "大社區", "鳥松區", "橋頭區", "燕巢區", "阿蓮區", "路竹區", "湖內區", "茄萣區", "梓官區", "六龜區", "杉林區", "內門區", "彌陀區", "甲仙區"],
        "屏東縣16": ["屏東市", "潮州鎮", "東港鎮", "恆春鎮", "萬丹鄉", "長治鄉", "麟洛鄉", "九如鄉", "里港鄉", "鹽埔鄉", "高樹鄉", "萬巒鄉", "內埔鄉", "竹田鄉", "新埤鄉", "枋寮鄉", "新園鄉", "崁頂鄉", "林邊鄉", "南州鄉", "佳冬鄉", "琉球鄉", "車城鄉", "枋山鄉", "春日鄉"],
        "台東縣17": ["台東市", "成功鎮", "關山鎮", "卑南鄉", "太麻里鄉", "長濱鄉", "鹿野鄉", "池上鄉"],
        "花蓮縣18": ["花蓮市", "鳳林鎮", "玉里鎮", "新城鄉", "吉安鄉", "壽豐鄉", "光復鄉", "瑞穗鄉"],
        "宜蘭縣19": ["宜蘭市", "羅東鎮", "蘇澳鎮", "頭城鎮", "礁溪鄉", "壯圍鄉", "員山鄉", "冬山鄉", "五結鄉", "三星鄉", "南澳鄉"],
        "澎湖縣20": ["馬公市", "湖西鄉", "西嶼鄉"],
        "金門縣22": ["金城鎮", "金沙鎮", "金湖鎮"]
    ]
}
