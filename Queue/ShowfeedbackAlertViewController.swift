//
//  ShowThxAlertViewController.swift
//  Queue
//
//  Created by Andrew on 2019/4/10.
//

import UIKit

class ShowfeedbackAlertViewController: UIViewController {
    
    @IBOutlet weak var contentLabel: UILabel!
    var content: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentLabel.text = content
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
