//
//  ShowUpdateViewController.swift
//  Queue
//
//  Created by Andrew on 2019/4/2.
//

import UIKit

class ShowVersionAlertViewController: UIViewController {
    
    @IBOutlet weak var cancelButtn: UIButton!
    var isNeedUpdate: Bool?
    var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isNeedUpdate == true {
            cancelButtn.isHidden = true
        } else {
            cancelButtn.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func updateClicked(_ sender: Any) {
        UIApplication.shared.open(URL(string: url!)!)
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
