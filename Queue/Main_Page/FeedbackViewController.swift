//
//  FeedbackViewController.swift
//  Queue
//
//  Created by Andrew on 2019/4/3.
//

import UIKit
import SwiftyJSON
import Alamofire

class FeedbackViewController: UIViewController {
    
    @IBOutlet weak var recommendButton: UIButton!
    @IBOutlet weak var bugReportButton: UIButton!
    @IBOutlet weak var recommendView: UIView!
    @IBOutlet weak var bugReportView: UIView!
    @IBOutlet weak var bugScrollView: UIScrollView!
    @IBOutlet weak var recommendScrollView: UIScrollView!
    
    @IBOutlet weak var clinicNameTextField: UITextField! {
        didSet {
            clinicNameTextField.setIcon(UIImage())
        }
    }
    @IBOutlet weak var clinicPhoneTextField: UITextField! {
        didSet {
            clinicPhoneTextField.setIcon(UIImage())
        }
    }
    @IBOutlet weak var clinicAddressTextField: UITextField! {
        didSet {
            clinicAddressTextField.setIcon(UIImage())
        }
    }
    
    @IBOutlet weak var titleTextField: UITextField! {
        didSet {
            titleTextField.setIcon(UIImage())
        }
    }
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.setIcon(UIImage())
        }
    }
    @IBOutlet weak var contentTextView: UITextView!
    
    var currentSelectButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedbackViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        intiTxtField()
        currentSelectButton = recommendButton
        setCurrentSelectButton(currentSelect: recommendButton)
        
        
        contentTextView.textContainerInset = UIEdgeInsets(top: 12, left: 20, bottom: 5, right: 20)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.cleanTxtContent), name: NSNotification.Name(rawValue: "cleanTxtContent"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cleanTxtContent()
    }
    
    @objc func cleanTxtContent() {
        clinicNameTextField.text = ""
        clinicPhoneTextField.text = ""
        clinicAddressTextField.text = ""
        titleTextField.text = ""
        emailTextField.text = ""
        contentTextView.text = ""
    }
    
    func setCurrentSelectButton(currentSelect: UIButton) {
        if currentSelectButton.isSelected == false {
            currentSelectButton.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        }
        currentSelectButton = currentSelect
        currentSelectButton.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    }
    
    func intiTxtField() {
        clinicNameTextField.layer.borderColor = UIColor.lightGray.cgColor
        clinicNameTextField.layer.backgroundColor = UIColor.white.cgColor
        clinicNameTextField.layer.borderWidth = 0.8
        
        clinicPhoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        clinicPhoneTextField.layer.backgroundColor = UIColor.white.cgColor
        clinicPhoneTextField.layer.borderWidth = 0.8
        
        clinicAddressTextField.layer.borderColor = UIColor.lightGray.cgColor
        clinicAddressTextField.layer.backgroundColor = UIColor.white.cgColor
        clinicAddressTextField.layer.borderWidth = 0.8
        
        titleTextField.layer.borderColor = UIColor.lightGray.cgColor
        titleTextField.layer.backgroundColor = UIColor.white.cgColor
        titleTextField.layer.borderWidth = 0.8
        
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.backgroundColor = UIColor.white.cgColor
        emailTextField.layer.borderWidth = 0.8
        
        contentTextView.layer.borderColor = UIColor.lightGray.cgColor
        contentTextView.layer.backgroundColor = UIColor.white.cgColor
        contentTextView.layer.borderWidth = 0.8
        contentTextView.layer.cornerRadius = 20
        
    }
    
    func switchTxtFieldColor(withTag tag: Int, withIsInput isInput: Bool) {
        if clinicNameTextField.tag == tag {
            clinicNameTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            clinicNameTextField.layer.backgroundColor = UIColor.white.cgColor
            clinicNameTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        } else if clinicPhoneTextField.tag == tag {
            clinicPhoneTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            clinicPhoneTextField.layer.backgroundColor = UIColor.white.cgColor
            clinicPhoneTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        } else if clinicAddressTextField.tag == tag {
            clinicAddressTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            clinicAddressTextField.layer.backgroundColor = UIColor.white.cgColor
            clinicAddressTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        } else if titleTextField.tag == tag {
            titleTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            titleTextField.layer.backgroundColor = UIColor.white.cgColor
            titleTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        } else if emailTextField.tag == tag {
            emailTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            emailTextField.layer.backgroundColor = UIColor.white.cgColor
            emailTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        }
    }
    
    func switchTxtViewColor(withTag tag: Int, withIsInput isInput: Bool) {
        if contentTextView.tag == tag {
            contentTextView.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            contentTextView.layer.backgroundColor = UIColor.white.cgColor
            contentTextView.layer.borderWidth = isInput ? 1.5 : 0.8
        }
    }
    
    @IBAction func showViewClicked(_ sender: UIButton) {
        setCurrentSelectButton(currentSelect: sender)
        switch sender {
        case recommendButton:
            recommendScrollView.isHidden = false
            bugScrollView.isHidden = true
        case bugReportButton:
            recommendScrollView.isHidden = true
            bugScrollView.isHidden = false
        default:
            print("")
        }
    }
    @IBAction func bugPostClicked(_ sender: Any) {
        guard !titleTextField.text!.isEmpty else {
            self.showAlert(msg: "似乎有欄位並未填寫喔！")
            return
        }
        guard !emailTextField.text!.isEmpty else {
            self.showAlert(msg: "似乎有欄位並未填寫喔！")
            return
        }
        guard ValidateUtil.shared.isValidEmail(withEmail: emailTextField.text!) else {
            self.showAlert(msg: "請輸入正確的信箱格式")
            return
        }
        guard !contentTextView.text!.isEmpty else {
            self.showAlert(msg: "似乎有欄位並未填寫喔！")
            return
        }
        RestAPI.shared.postSuggest(title: titleTextField.text ?? "", content: contentTextView.text ?? "", email: emailTextField.text ?? "") {(response)->Void in
            if (response != JSON.null) {
                let notiName = Notification.Name("cleanTxtContent")
                NotificationCenter.default.post(name: notiName, object: nil)
                self.showAlert(msg: "感謝您的建議與回覆!")
            } else {
                self.showAlert(msg: "提醒請傳送純文字!")
            }
        }
    }
    
    @IBAction func recommendPostClicked(_ sender: Any) {
        guard !clinicNameTextField.text!.isEmpty else {
            self.showAlert(msg: "似乎有欄位並未填寫喔！")
            return
        }
        guard !clinicPhoneTextField.text!.isEmpty else {
            self.showAlert(msg: "似乎有欄位並未填寫喔！")
            return
        }
        guard ValidateUtil.shared.isVaildTelPhone(withCellPhone: clinicPhoneTextField.text!) else {
            self.showAlert(msg: "請輸入正確的電話格式")
            return
        }
        guard !clinicAddressTextField.text!.isEmpty else {
            self.showAlert(msg: "似乎有欄位並未填寫喔！")
            return
        }
        RestAPI.shared.postRecommend(name: clinicNameTextField.text ?? "", phone: clinicPhoneTextField.text ?? "", address: clinicAddressTextField.text ?? "")  {(response)->Void in
            if (response != JSON.null) {
                let notiName = Notification.Name("cleanTxtContent")
                NotificationCenter.default.post(name: notiName, object: nil)
                self.showAlert(msg: "感謝您的建議與回覆!")
            } else {
                self.showAlert(msg: "提醒請傳送純文字!")
            }
        }
    }
    
    func showAlert(msg:String) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.feedbackCompletePage) as?
            ShowfeedbackAlertViewController {
            controller.content = msg
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension FeedbackViewController: UITextFieldDelegate, UITextViewDelegate{
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    // 當按下右下角的return鍵時觸發
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isContainsSpaceCharacters() {
            return false
        }
        if string.isContainsEmoji() {
            return false
        }
        let text = textField.text
        let textLength = text!.characters.count + string.characters.count - range.length
        
        if textField == clinicPhoneTextField {
            return textLength <= 10
        } else if textField == titleTextField {
            return textLength <= 13
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.isContainsSpaceCharacters() {
            return false
        }
        if text.isContainsEmoji() {
            return false
        }
        /// - Remark: *textview.text 原本的字串* *text 輸入的字串* *range 第幾個字元*
        let textLength = textView.text.characters.count + text.characters.count - range.length
        if textView == contentTextView {
            return textLength <= 56
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.switchTxtFieldColor(withTag: textField.tag, withIsInput: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.switchTxtFieldColor(withTag: textField.tag, withIsInput: false)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if contentTextView.text == "請輸入問題內容..." {
            contentTextView.text = ""
            contentTextView.textColor = UIColor.black
        }
        self.switchTxtViewColor(withTag: textView.tag, withIsInput: true)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if contentTextView.text == "" {
            contentTextView.text = "請輸入問題內容..."
            contentTextView.textColor = UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        }
        self.switchTxtViewColor(withTag: textView.tag, withIsInput: false)
    }
    
}

extension String {
    /// *是否含有注音*
    func isContainsPhoneticCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if (scalar.value >= 12549 && scalar.value <= 12582) || (scalar.value == 12584 || scalar.value == 12585 || scalar.value == 19968) {
                return true
            }
        }
        return false
    }
    /// *是否含有空白字元*
    func isContainsSpaceCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if scalar.value == 32 {
                return true
            }
        }
        return false
    }
    /// ＊是否含有emoji＊
    func isContainsEmoji() -> Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x1F1E6...0x1F1FF, // Regional country flags
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
            127000...127600, // Various asian characters
            65024...65039, // Variation selector
            9100...9300, // Misc items
            8400...8447: // Combining Diacritical Marks for Symbols
                return true
            default:
                continue
            }
        }
        return false
    }
}

extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 10, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
}


