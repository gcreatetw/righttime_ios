//
//  AppDelegate.swift
//  Queue
//
//  Created by Andrew on 2019/3/5.
//

import UIKit
import CoreData
import FirebaseCore
import FirebaseMessaging
import UserNotifications
import CoreLocation
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift

var cityLists: [JSON]?
var typeLists: [JSON]?
var firebaseToken = String()
var DEVICE_UUID: String?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.set("台北市02", forKey: UserDefaultsID.pharmacyCity)
        UserDefaults.standard.set("松山區", forKey: UserDefaultsID.pharmacyDistrict)
        
        StoreKitHelper.incrementNumberOfTimesLaunched() // 紀錄開啟 APP 幾次
        IQKeyboardManager.shared.enable = true // 開啟控制 keyboard 高度套件
        DEVICE_UUID = UIDevice.current.identifierForVendor?.uuidString ?? UUID().uuidString
        
        /// - Note: 與 server download clinics list
        RestAPI.shared.getCityList { (response)->Void in
            if (response != JSON.null) {
                cityLists = response.array!
                cityLists?.insert(["name": "周邊熱搜","city_id": ""], at: 0)
            }
        }
        RestAPI.shared.getTypeList { (response)->Void in
            if (response != JSON.null) {
                typeLists = response.array!
                typeLists?.insert(["type_id": "","name": "全部科別"], at: 0)
            }
        }
        
        
        
        FirebaseApp.configure() // 載入 firebase SDK
        /// - Note: 是否允許開啟通知，選擇完接續問是否允許開啟定位
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            Messaging.messaging().delegate = self // For iOS 10 data message (sent via FCM)
            /// - Remark: 在程式一啟動即詢問使用者是否接受圖文(alert)、聲音(sound)、數字(badge)三種類型的通知
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { granted, error in
                if granted { } else { }
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            //            self.locationManager.requestWhenInUseAuthorization()
            //            self.locationManager.startMonitoringVisits()
            //            self.locationManager.delegate = self as? CLLocationManagerDelegate
        }
        
        application.registerForRemoteNotifications() // 註冊遠程通知
        
        /// - Note: 如果開啟過 APP 就直接進入主畫面，不再秀導覽畫面
        let storyboard = self.window?.rootViewController?.storyboard
        if UserDefaults.standard.bool(forKey: UserDefaultsID.isNotFirstOpenApp) == true {
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.tabBarPage) {
                self.window?.rootViewController = controller
            }
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    /// iOS10 以下的版本接收推播訊息的 delegate
    ///
    /// - Parameters:
    ///   - application: _
    ///   - userInfo: _
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        // 印出後台送出的推播訊息(JOSN 格式)
        print("userInfo: \(userInfo)")
    }
    
    /// iOS10 以下的版本接收推播訊息的 delegate
    ///
    /// - Parameters:
    ///   - application: _
    ///   - userInfo: _
    ///   - completionHandler: _
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // 印出後台送出的推播訊息(JOSN 格式)
        print("userInfo: \(userInfo)")
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    /// 推播失敗的訊息
    ///
    /// - Parameters:
    ///   - application: _
    ///   - error: _
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    /// 取得 DeviceToken，通常 for 後台人員推播用
    ///
    /// - Parameters:
    ///   - application: _
    ///   - deviceToken: _
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // 將 Data 轉成 String
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString: \(deviceTokenString)")
        
        // 將 Device Token 送到 Server 端...
        
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Queue")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    /// App 在前景時，推播送出時即會觸發的 delegate
    ///
    /// - Parameters:
    ///   - center: _
    ///   - notification: _
    ///   - completionHandler: _
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // 印出後台送出的推播訊息(JOSN 格式)
        let userInfo = notification.request.content.userInfo
        print("userInfo: \(userInfo)")
        
        // 可設定要收到什麼樣式的推播訊息，至少要打開 alert，不然會收不到推播訊息
        completionHandler([.badge, .sound, .alert])
    }
    
    /// App 在關掉的狀態下或 App 在背景或前景的狀態下，點擊推播訊息時所會觸發的 delegate
    ///
    /// - Parameters:
    ///   - center: _
    ///   - response: _
    ///   - completionHandler: _
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // 印出後台送出的推播訊息(JOSN 格式)
        let userInfo = response.notification.request.content.userInfo
        print("userInfo: \(userInfo)")
        
        completionHandler()
    }
}

extension AppDelegate: MessagingDelegate {
    
    /// iOS10 含以上的版本用來接收 firebase token 的 delegate
    ///
    /// - Parameters:
    ///   - messaging: _
    ///   - fcmToken: _
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        // 用來從 firebase 後台推送單一裝置所必須的 firebase token
        firebaseToken = fcmToken
        print("Firebase registration token: \(fcmToken)")
    }
}


