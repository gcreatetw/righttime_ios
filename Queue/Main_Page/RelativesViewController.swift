//
//  RelativesViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/13.
//

import UIKit

class RelativesViewController: UIViewController {
    var patientName:String = ""
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var RelativesTableView: UITableView!
    //假資料
    var patientList = ["林oo","黃oo","王oo","黃oo","王oo"]
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.layer.cornerRadius = 24
        bgView.layer.shadowRadius = 5
        bgView.layer.shadowOpacity = 0.5
        
        if patientList.count >= 5{
            bgView.heightAnchor.constraint(equalToConstant: CGFloat(170+30+45*5)).isActive = true
            RelativesTableView.heightAnchor.constraint(equalToConstant: CGFloat(30+45*5)).isActive = true
        }
        else{
            bgView.heightAnchor.constraint(equalToConstant: CGFloat(170+30+45*patientList.count)).isActive = true
            RelativesTableView.heightAnchor.constraint(equalToConstant: CGFloat(30+45*patientList.count)).isActive = true
        }
        let indexPath = IndexPath(row: 0, section: 0)
        
        RelativesTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        _ = RelativesTableView.delegate?.tableView?(RelativesTableView, willSelectRowAt: indexPath)
        patientName = patientList[0]
        
        print("print == \(patientName)")
    }
    var isAddBtn:Bool = false
    @IBAction func addRelatives(_ sender: UIButton) {
         isAddBtn = true
    }
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension RelativesViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(RelativesTableViewCell.self)", for: indexPath) as? RelativesTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.relativesNameLabel.text = patientList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = RelativesTableView.cellForRow(at: indexPath) as? RelativesTableViewCell else { return }
        if cell.isCheck == false{
            cell.relativesView.layer.backgroundColor = UIColor(named: "main_color")?.cgColor
            cell.relativesView.layer.borderWidth = 0
            cell.isCheck = true
        }
        patientName = cell.relativesNameLabel.text!
        print("print = \(patientName)")
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = RelativesTableView.cellForRow(at: indexPath) as? RelativesTableViewCell else { return }
        cell.relativesView.layer.backgroundColor = UIColor.clear.cgColor
        cell.relativesView.layer.borderWidth = 1
        cell.isCheck = false
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = RelativesTableView.cellForRow(at: indexPath) as! RelativesTableViewCell
        cell.relativesView.backgroundColor = UIColor(named: "main_color")
        cell.relativesView.layer.borderWidth = 0
        return indexPath
    }
}
