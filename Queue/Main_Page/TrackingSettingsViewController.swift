//
//  LightBoxViewController.swift
//  Queue
//
//  Created by Andrew on 2019/3/5.
//

import UIKit
import CoreData
import SwiftyJSON
import Alamofire
import BetterSegmentedControl

class ApptSettingsViewController: UIViewController {

    @IBOutlet weak var circleContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.borderWidth = 2
            containerView.layer.borderColor = UIColor(red: 255/255, green: 60/255, blue: 60/255, alpha: 1).cgColor
        }
    }
    
    @IBOutlet weak var circleContainerView: UIView!
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    @IBOutlet weak var scheduleButton: UIButton! {
        didSet {
            scheduleButton.layer.cornerRadius = 3
            scheduleButton.setTitle(self.selectDate, for: .normal)
        }
    }
    
    @IBOutlet weak var apptNumTextField: UITextField! {
        didSet {
            apptNumTextField.layer.cornerRadius = 3
//            myNumTextField.becomeFirstResponder()
        }
    }
    
    @IBOutlet weak var bookingNumTextField: UITextField! {
        didSet {
            bookingNumTextField.layer.cornerRadius = 3
            bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
        }
    }
    
    @IBOutlet weak var roomCurNumLabel: UILabel! {
        didSet {
            roomCurNumLabel.text = roomCurNum
        }
    }
    
    @IBOutlet weak var alertLabel: UILabel!    
    @IBOutlet weak var segmentContainerView: UIView!
    
    
    var clinicName: String?
    var roomID: Int = 0
    var roomName: String?
    var roomCurNum: String?
    var bookingNum: Int = 0
    
    var restApi = RestAPI()
    var selectTimeString: String?
    var selectTimeInt: Int?
    var currentTimeString: String?
    var currentTimeInt: Int?
    var selectDate = DateUtil.shared.getStrSysDate(withFormat: "YYYY-MM-dd")

   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        circleContainerConstraint.constant = -circleContainerView.frame.size.width * 0.36
        let aDegree = CGFloat.pi / 180
        var semiCircleLayer   = CAShapeLayer()
        let center = CGPoint (x: circleContainerView.frame.size.width / 2, y: circleContainerView.frame.size.height / 2)
        let circleRadius = circleContainerView.frame.size.width / 2
        let circlePath = UIBezierPath(arcCenter: center, radius: circleRadius, startAngle: aDegree * 345, endAngle: aDegree * 195, clockwise: false)
        semiCircleLayer.path = circlePath.cgPath
        semiCircleLayer.strokeColor = UIColor(red: 255/255, green: 60/255, blue: 60/255, alpha: 1).cgColor
        semiCircleLayer.fillColor = UIColor.clear.cgColor
        semiCircleLayer.lineWidth = 2
        semiCircleLayer.strokeStart = 0
        semiCircleLayer.strokeEnd  = 1
        circleContainerView.layer.addSublayer(semiCircleLayer)
        circleContainerView.layer.cornerRadius = circleContainerView.frame.size.width * 0.5
        
        var w: CGFloat = 198
        var h: CGFloat = 26.5
        
        switch UIDevice().type {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C:
           w = 198
           h = 26.5
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
           w = segmentContainerView.frame.size.width
           h = segmentContainerView.frame.size.height
        case .iPhone6plus, .iPhone6Splus, .iPhone7plus, .iPhone8plus:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        case .iPhoneXR,.iPhoneXSMax:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        case .iPhoneXS,.iPhoneX:
            w = 219
            h = segmentContainerView.frame.size.height
        default:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        }
        
        let SegmentedControl = BetterSegmentedControl (
            frame: CGRect(x: 0, y: 0, width: w, height: h),
            segments: LabelSegment.segments(withTitles: ["上午診", "下午診", "夜間診"],
                                            normalFont: UIFont.systemFont(ofSize: 12),
                                            normalTextColor: UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1),
                                            selectedFont: UIFont.systemFont(ofSize: 12),
                                            selectedTextColor: UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)),
            index: UInt(selectTimeInt!),
            options: [.backgroundColor(UIColor(red:255/255, green:199/255, blue:5/255, alpha:1.00)),
                      .indicatorViewBackgroundColor(UIColor(red:1, green:1, blue:1, alpha:1.00)),
                      .cornerRadius(h/2),
                      .bouncesOnChange(false),
                      .panningDisabled(true)])
        SegmentedControl.indicatorViewInset = 3
        SegmentedControl.addTarget(self, action: #selector(ApptSettingsViewController.SegmentedControlValueChanged(_:)), for: .valueChanged)
        segmentContainerView.addSubview(SegmentedControl)
    }

    @objc func SegmentedControlValueChanged(_ sender: BetterSegmentedControl) {
        switch sender.index {
        case 0:
            let oldselectTimeInt = selectTimeInt
            selectTimeString = "M"
            selectTimeInt = 0
            apptNumTextField.text = ""
            bookingNumTextField.text = ""
            alertLabel.isHidden = true
            if selectTimeInt ?? 0 < oldselectTimeInt ?? 0 && selectTimeInt ?? 0 < currentTimeInt ?? 0 {
                selectTimeInt = oldselectTimeInt
            }
        case 1:
            let oldselectTimeInt = selectTimeInt
            selectTimeString = "A"
            selectTimeInt = 1
            apptNumTextField.text = ""
            bookingNumTextField.text = ""
            alertLabel.isHidden = true
            if selectTimeInt ?? 0 < oldselectTimeInt ?? 0 && selectTimeInt ?? 0 < currentTimeInt ?? 0 {
                selectTimeInt = oldselectTimeInt
            }
        case 2:
            let oldselectTimeInt = selectTimeInt
            selectTimeString = "E"
            selectTimeInt = 2
            apptNumTextField.text = ""
            bookingNumTextField.text = ""
            alertLabel.isHidden = true
            if selectTimeInt ?? 0 < oldselectTimeInt ?? 0 &&  selectTimeInt ?? 0 < currentTimeInt ?? 0 {
                selectTimeInt = oldselectTimeInt
            }
        default:
            print("default")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch currentTimeString {
        case "M":
            selectTimeInt = 0
            selectTimeString = "M"
            currentTimeInt = 0
        case "A":
            selectTimeInt = 1
            selectTimeString = "A"
            currentTimeInt = 1
        case "E":
            selectTimeInt = 2
            selectTimeString = "E"
            currentTimeInt = 2
        default:
            print("default")
        }
    }
    
    @IBAction func scheduleButtonClicked(_ sender: Any) {
        let today = Date()
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        let dateFrom = calendar.startOfDay(for: today)
        let dateTo = calendar.date(byAdding: .day, value: 60, to: dateFrom)
        var tmpSelectDate: String?
        
        let alert = UIAlertController(title: "請選擇您預約的日期", message: "兩個月內的時段", preferredStyle: .actionSheet)
        alert.addDatePicker(mode: .date, date: dateFrom, minimumDate: dateFrom, maximumDate: dateTo) { date in
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
            dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
            tmpSelectDate = dateFormatter.string(from: date)
        }
        alert.addAction(UIAlertAction(title: "完成", style: .cancel, handler: { (UIAlertAction) in
            if let tmpSelectDate = tmpSelectDate {
                self.selectDate = tmpSelectDate
            } else {
                self.selectDate = DateUtil.shared.getStrSysDate(withFormat: "YYYY-MM-dd")
            }
            
            if DateUtil.shared.getStrSysDate(withFormat: "YYYY-MM-dd") != self.selectDate {
                self.selectTimeInt = 0
                self.selectTimeString = "M"
                self.currentTimeInt = 0
                self.roomCurNumLabel.text = "預約"
            } else {
                self.roomCurNumLabel.text = self.roomCurNum
                switch self.currentTimeString {
                case "M":
                    self.selectTimeInt = 0
                    self.selectTimeString = "M"
                    self.currentTimeInt = 0
                case "A":
                    self.selectTimeInt = 1
                    self.selectTimeString = "A"
                    self.currentTimeInt = 1
                case "E":
                    self.selectTimeInt = 2
                    self.selectTimeString = "E"
                    self.currentTimeInt = 2
                default:
                    print("default")
                }
            }
            self.scheduleButton.setTitle(self.selectDate, for: .normal)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func goBackVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    func switchTxtFieldColor(withTag tag: Int, withIsInput isInput: Bool) {
        if apptNumTextField.tag == tag {
            apptNumTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            apptNumTextField.layer.backgroundColor = UIColor.white.cgColor
            apptNumTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        } else if bookingNumTextField.tag == tag {
            bookingNumTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            bookingNumTextField.layer.backgroundColor = UIColor.white.cgColor
            bookingNumTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        }
    }
    
    @IBAction func goToHomePage(_ sender: Any) {
        guard !apptNumTextField.text!.isEmpty else {
            alertLabel.text = "請設定您的號碼"
            alertLabel.isHidden = false
            return
        }
        
        guard !bookingNumTextField.text!.isEmpty else {
            alertLabel.text = "請設定提前幾號提醒"
            alertLabel.isHidden = false
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request:NSFetchRequest<TrackingNum> = TrackingNum.fetchRequest()
        var readTrackingNum: [TrackingNum]?
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        calendar.locale = Locale(identifier: "zh_Hant_TW")
        let dateFrom = calendar.startOfDay(for: Date())
        let dateTo = calendar.date(byAdding: .day, value: 60, to: dateFrom)
        
        request.predicate = NSPredicate(format: "roomID == %@ && bookingTime BETWEEN {%@, %@}", argumentArray:[self.roomID, dateFrom, dateTo])
        do {
            readTrackingNum = try context.fetch(request)
        } catch {
            print("error")
        }
        
        bookingNum = Int(apptNumTextField.text!)! - Int(bookingNumTextField.text!)!
        var tmpBookingTimeArray = [String]()
        for i in readTrackingNum! {
            tmpBookingTimeArray.append(DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: i.bookingTime!))
        }
        
        if tmpBookingTimeArray.contains(self.selectDate) {
            for i in readTrackingNum! {
                if DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: i.bookingTime!) == self.selectDate {
                    i.myNum = apptNumTextField.text
                    i.createTime = selectTimeString
                    i.bookingTime = DateUtil.shared.getDateFromString(withFormat: "YYYY-MM-dd", strDate: self.selectDate)
                    appDelegate.saveContext()
                    restApi.postTrackingInfo(roomID: String(self.roomID), deviceToken: firebaseToken, targetNum: apptNumTextField.text!, notifyNum: String(bookingNum), period: self.selectTimeString!, date: self.selectDate) {(response)->Void in
                        if (response != JSON.null) {
                            print("postTrackingInfo:",response)
                        }
                    }
                }
            }
        } else {
            let tracking = TrackingNum(context: context)
            tracking.roomID = Int16(self.roomID)
            tracking.createTime = selectTimeString
            tracking.clinicName = self.clinicName
            tracking.myNum = apptNumTextField.text
            tracking.roomName = self.roomName
            tracking.updateTime = DateUtil.shared.getDateFromString()
            tracking.bookingTime = DateUtil.shared.getDateFromString(withFormat: "YYYY-MM-dd", strDate: self.selectDate)
            tracking.currentNum = roomCurNumLabel.text
            appDelegate.saveContext()
            restApi.postTrackingInfo(roomID: String(self.roomID), deviceToken: firebaseToken, targetNum: apptNumTextField.text!, notifyNum: String(bookingNum), period: self.selectTimeString!, date: self.selectDate) {(response)->Void in
                if (response != JSON.null) {
                    print("postTrackingInfo:",response)
                }
            }
        }
//        var tmpReadTrackingIDs:[Int16] = []
//        for i in readTrackingNum {
//            print("result:", i)
//            tmpReadTrackingIDs.append(i.roomID)
//        }
//        bookingNum = Int(myNumTextField.text!)! - Int(bookingNumTextField.text!)!
//        /// - Remark: if array contains roomID print_error，else save to coreData
//        if tmpReadTrackingIDs.contains(Int16(self.roomID)) {
//            for i in readTrackingNum {
//                if i.roomID == Int16(self.roomID) {
//                    i.myNum = myNumTextField.text
//                    i.createTime = selectTimeString
//                    i.bookingTime = DateUtil.shared.getDateFromString(withFormat: "YYYY-MM-dd", strDate: self.selectDate)
//                    appDelegate.saveContext()
//                    print("firebaseToken",firebaseToken)
//                    restApi.postTrackingInfo(roomID: String(self.roomID), deviceToken: firebaseToken, targetNum: myNumTextField.text!, notifyNum: String(bookingNum), period: self.selectTimeString!, date: self.selectDate) {(response)->Void in
//                        if (response != JSON.null) {
//                            print("postTrackingInfo_success:",response)
//                        } else {
//                            print("postTrackingInfo_Error:",response)
//                        }
//                    }
//                }
//            }
//        } else {
//            let tracking = TrackingNum(context: context)
//            tracking.roomID = Int16(self.roomID)
//            tracking.createTime = selectTimeString
//            tracking.clinicName = self.clinicName
//            tracking.myNum = myNumTextField.text
//            tracking.roomName = self.roomName
//            tracking.updateTime = DateUtil.shared.getDateFromString()
//            tracking.bookingTime = DateUtil.shared.getDateFromString(withFormat: "YYYY-MM-dd", strDate: self.selectDate)
//            tracking.currentNum = roomCurNumLabel.text
//            appDelegate.saveContext()
//            restApi.postTrackingInfo(roomID: String(self.roomID), deviceToken: firebaseToken, targetNum: myNumTextField.text!, notifyNum: String(bookingNum), period: self.selectTimeString!, date: self.selectDate) {(response)->Void in
//                if (response != JSON.null) {
//                    print("postTrackingInfo_success:",response)
//                } else {
//                    print("postTrackingInfo_Error:",response)
//                }
//            }
//        }
        let notiName = Notification.Name("refreshHomePage")
        NotificationCenter.default.post(name: notiName, object: nil)
        self.performSegue(withIdentifier: SegueID.backToHomePage, sender: nil)
    }
}

extension ApptSettingsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var apptNumMinusCurNum: Int?
        if let tmp = Int(apptNumTextField.text ?? "0"), let tmp2 = Int(roomCurNum ?? "0") {
             apptNumMinusCurNum = tmp - tmp2
        }
        
        switch textField {
        case bookingNumTextField:
            if let text = textField.text {
                let newStr = (text as NSString).replacingCharacters(in: range, with: string)
                if newStr.isEmpty {
                    return true
                }
                let intvalue = Int(newStr)
                let textLength = text.characters.count + string.characters.count - range.length
                /// - Remark: myNumTextField.text!沒輸入就不給輸入，有輸入不限制只要兩位數大於1小於10
                if apptNumTextField.text! == "" || apptNumTextField.text! == "0" || apptNumTextField.text! == "00" || apptNumTextField.text! == "000" {
                    return (textLength == 0)
                } else if Int(apptNumTextField.text!)! < 10 {
                    return (intvalue! >= 1 && intvalue! <= Int(apptNumTextField.text!)! && textLength<=2)
                } else {
                    return (intvalue! >= 1 && intvalue! <= 10 && textLength<=2)
                }
            }
            return true
        case apptNumTextField:
            if let text = textField.text, let Range = Range(range, in: text) {
                var newText = "0"
                /// - Remark: 判斷myNum是否為空
                if text.replacingCharacters(in: Range, with: string) == "" {
                    newText = ""
                } else {
                    newText = text.replacingCharacters(in: Range, with: string)
                }
                
                if newText == "0" || newText == "00" || newText == "000" || Int(newText) == 0 {
                    alertLabel.text = "您的號碼不可以等於0"
                    alertLabel.isHidden = false
                    bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"☓", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    let textLength = text.characters.count + string.characters.count - range.length
                    return textLength <= 3
                } else if newText == "" {
                    alertLabel.isHidden = true
                    bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    let textLength = text.characters.count + string.characters.count - range.length
                    return textLength <= 3
                } else if Int(newText)! < 10 {
                    alertLabel.isHidden = true
                    bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~"+String(Int(newText)!), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                } else {
                    alertLabel.isHidden = true
                    bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    let textLength = text.characters.count + string.characters.count - range.length
                    return textLength <= 3
                }
            }
            
            let textLength = textField.text!.characters.count + string.characters.count - range.length
            return textLength <= 3
        default:
            print("default")
            return true
        }
        /*
        if textField == bookingNumTextField {
            guard currentTimeString == selectTimeString else {
                /// - Remark: bookingNumTextField 如果選擇時刻不同於真實時間
                if let text = textField.text {
                    let newStr = (text as NSString).replacingCharacters(in: range, with: string)
                    if newStr.isEmpty {
                        return true
                    }
                    let intvalue = Int(newStr)
                    let textLength = text.characters.count + string.characters.count - range.length
                    /// - Remark: myNumTextField.text!沒輸入就不給輸入，有輸入不限制只要兩位數大於1小於10
                    if myNumTextField.text! == "" || myNumTextField.text! == "0" || myNumTextField.text! == "00" || myNumTextField.text! == "000" {
                        return (textLength == 0)
                    } else if Int(myNumTextField.text!)! < 10 {
                        return (intvalue! >= 1 && intvalue! <= Int(myNumTextField.text!)! && textLength<=2)
                    } else {
                        return (intvalue! >= 1 && intvalue! <= 10 && textLength<=2)
                    }
                }
                return true
            }
            /// - Remark: bookingNumTextField 如果選擇時刻跟真實時間相同
            if let text = textField.text {
                let newStr = (text as NSString).replacingCharacters(in: range, with: string)
                if newStr.isEmpty {
                    return true
                }
                let intvalue = Int(newStr)
                let textLength = text.characters.count + string.characters.count - range.length
                if myNumTextField.text! == "" {
                    return (textLength == 0)
                } else if myNumMinusCurNum! > 10 {
                        return (intvalue! >= 1 && intvalue! <= 10 && textLength <= 2)
                } else if myNumMinusCurNum! > 1 && myNumMinusCurNum! <= 10 {
                    return (intvalue! >= 1 && intvalue! <= myNumMinusCurNum! - 1 && textLength <= 2)
                } else {
                    return (textLength == 0)
                }
            }
            return true
        } else {
            if currentTimeString == selectTimeString {
                guard let text = textField.text else {
                    return true
                }

                if let text = textField.text, let Range = Range(range, in: text) {
                    var newText = "0"
                    /// - Remark: 判斷myNum是否為空
                    if text.replacingCharacters(in: Range, with: string) == "" {
                        newText = ""
                        myNumMinusCurNum = Int(newText) ?? 0 - Int(roomCurNum!)!
                    } else {
                        newText = text.replacingCharacters(in: Range, with: string)
                        myNumMinusCurNum = Int(newText)! - Int(roomCurNum!)!
                    }

                    if myNumMinusCurNum! > 10 {
                        alertLabel.isHidden = true
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    } else if myNumMinusCurNum! > 1 && myNumMinusCurNum! <= 10 {
                        alertLabel.isHidden = true
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~"+String(myNumMinusCurNum!-1), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    } else if myNumMinusCurNum! == 1 {
                        alertLabel.text = "下一號就是您的號碼，請儘速就診!"
                        alertLabel.isHidden = false
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"☓", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    } else if myNumMinusCurNum! == 0 {
                        alertLabel.text = "您的號碼不可以等於0"
                        alertLabel.isHidden = false
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"☓", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    } else if newText =


                    {
                        /// - Remark: myNumMinusCurNum 只剩下0、“”、<0
                        guard newText == "" else {
                            /// - Remark: myNumMinusCurNum <= 0
                            alertLabel.text = "您的號碼不可以小於等於當前號碼"
                            alertLabel.isHidden = false
                            bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"☓", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                            let textLength = text.characters.count + string.characters.count - range.length
                            return textLength <= 3
                        }
                        /// - Remark: newText = nil
                        alertLabel.isHidden = true
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    }
                }
            } else {
                if let text = textField.text, let Range = Range(range, in: text) {
                    var newText = "0"
                    /// - Remark: 判斷myNum是否為空
                    if text.replacingCharacters(in: Range, with: string) == "" {
                        newText = ""
                    } else {
                        newText = text.replacingCharacters(in: Range, with: string)
                    }
                    if newText == "0" || newText == "00" || newText == "000" || Int(newText) == 0 {
                        alertLabel.text = "您的號碼不可以等於0"
                        alertLabel.isHidden = false
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"☓", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                        let textLength = text.characters.count + string.characters.count - range.length
                        return textLength <= 3
                    } else if newText == "" {
                        alertLabel.isHidden = true
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                        let textLength = text.characters.count + string.characters.count - range.length
                        return textLength <= 3
                    } else if Int(newText)! < 10 {
                        alertLabel.isHidden = true
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~"+String(Int(newText)!), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    } else {
                        alertLabel.isHidden = true
                        bookingNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                        let textLength = text.characters.count + string.characters.count - range.length
                        return textLength <= 3
                    }
                }
            }

            let textLength = textField.text!.characters.count + string.characters.count - range.length
            return textLength <= 3
        }
        */
    }
 
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.switchTxtFieldColor(withTag: textField.tag, withIsInput: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.switchTxtFieldColor(withTag: textField.tag, withIsInput: false)
    }
}



