//
//  HealthKnowledgeTbCell.swift
//  Queue
//
//  Created by Andrew on 2020/8/11.
//

import UIKit

class HealthKnowledgeTbCell: UITableViewCell {

    @IBOutlet weak var heHoCategoryCollView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reloadColl() {
        heHoCategoryCollView.reloadData()
    }

}
