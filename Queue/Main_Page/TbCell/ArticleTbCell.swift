//
//  ArticleTbCell.swift
//  Queue
//
//  Created by Andrew on 2020/8/30.
//

import UIKit

class ArticleTbCell: UITableViewCell {

    @IBOutlet weak var bannerImg: UIImageView! {
        didSet {
            bannerImg.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
