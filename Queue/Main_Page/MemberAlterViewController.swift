//
//  MemberAlterViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/14.
//

import UIKit

class MemberAlterViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var memberLabelTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap) // to Replace "TouchesBegan"
    }
    @objc func dismissKeyBoard() {
            self.view.endEditing(true)
        }

    //存修改資料
    @IBAction func saveMemberClicked(_ sender: UIButton) {
        
    }

}
