//
//  HospitalDetailViewController.swift
//  Queue
//
//  Created by Andrew on 2019/2/25.
//

import UIKit
import MarqueeLabel
import SwiftyJSON
import Alamofire
import CoreData
import SDWebImage
import Lottie

class ClinicDetailViewController: UIViewController {
    @IBOutlet weak var clinicDetailNameLabel: UILabel!
    @IBOutlet weak var clinicDetailImageView: UIImageView!
    @IBOutlet weak var clinicDetailAddressTextView: UITextView!
    @IBOutlet weak var clinicDetailPhoneTextView: UITextView! {
        didSet {
            clinicDetailPhoneTextView.textColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)
            clinicDetailPhoneTextView.linkTextAttributes = [NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue,NSAttributedString.Key.underlineColor: UIColor.lightGray]
        }
    }
    @IBOutlet weak var clinicDetailExphone: UILabel! 
    @IBOutlet weak var clinicDetailTableView: UITableView!
    @IBOutlet weak var clinicAnnouncementLabel: MarqueeLabel! {
        didSet {
            clinicAnnouncementLabel.type = .continuous
            clinicAnnouncementLabel.animationCurve = .easeInOut
            clinicAnnouncementLabel.restartLabel()
            clinicAnnouncementLabel.labelize = false
        }
    }
    @IBOutlet weak var clinicWeekSegmented: UISegmentedControl!
    @IBOutlet weak var headerBgView: UIView!
    @IBOutlet weak var clinicDetailImageRingView: UIView!
    @IBOutlet weak var clinicDetailImgShadowView: UIView!
    @IBOutlet weak var calenderBtn: UIButton!
    
    var clinicID: String?
    var clininImgURL: String?
    var clinicInfo: JSON?
    var roomInfo: [JSON]?
    var schedulePic = [JSON]()
    var weekTabValue = String()
    var categories = [
        clinicTimeTableType(imageView:"icon-Morning",title: "上午診",content:"morning"),
        clinicTimeTableType(imageView:"icon-Afternoon",title: "下午診",content:"afternoon"),
        clinicTimeTableType(imageView:"icon-Night",title: "晚上診",content:"evening")]
    var refreshControl: UIRefreshControl!
    var readFavoriteInfos: [Favorite] = []
    let addFavoriteBtn = UIButton()
    let toMemberBtn = UIButton()
    let favoriteAnimationView = AnimationView(name: "1835-like")
    var clinicStartEndTime = [["", ""], ["", ""], ["", ""]]
    var currentTimeString: String?
    var isQRcode: Bool?
    var mTimes = 0
    
    @IBAction func unwindToClinicDetail(_ unwindSegue: UIStoryboardSegue) {
        //let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var HEIGHT_VIEW: CGFloat = 0
        var leftPoint: CGFloat = 0
        switch UIDevice().type {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C:
            HEIGHT_VIEW = 188
            leftPoint = 30
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            HEIGHT_VIEW = 204
            leftPoint = 60
        case .iPhone6plus, .iPhone6Splus, .iPhone7plus, .iPhone8plus:
            HEIGHT_VIEW = 204
            leftPoint = 60
        case .iPhoneX, .iPhoneXR, .iPhoneXS, .iPhoneXSMax:
            HEIGHT_VIEW = 204
            leftPoint = 60
        default:
            HEIGHT_VIEW = 204
            leftPoint = 60
        }
        
        clinicDetailTableView.tableHeaderView?.frame.size = CGSize(width:clinicDetailTableView.frame.width, height: CGFloat(HEIGHT_VIEW))
        
        let w: CGFloat = self.view.frame.size.width
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: w, y: 0))
        path.addLine(to: CGPoint(x: w, y: HEIGHT_VIEW))
        path.addCurve(to: CGPoint(x: 0, y: leftPoint), controlPoint1: CGPoint(x: w * 0.90, y: 40), controlPoint2: CGPoint(x: w * 0.30, y: HEIGHT_VIEW+20))
        path.addLine(to: CGPoint(x: 0, y: 0))
        let frame = CGRect(x: 0, y: 0, width: w, height: HEIGHT_VIEW)
        let view = UIView(frame: frame)
        let triangleLayer = CAShapeLayer()
        view.backgroundColor = UIColor(red: 0/255, green: 199/255, blue: 162/255, alpha: 1)
        triangleLayer.path = path.cgPath
        triangleLayer.fillColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1).cgColor
        view.layer.addSublayer(triangleLayer)
        headerBgView.addSubview(view)
        
        let clinicImageWidth: CGFloat = clinicDetailImageView.frame.size.width
        clinicDetailImageView.layer.cornerRadius = clinicImageWidth / 2
        clinicDetailImgShadowView.layer.cornerRadius = clinicImageWidth / 2
        clinicDetailImgShadowView.layer.shadowOffset = CGSize(width: -2, height: 5)
        clinicDetailImgShadowView.layer.shadowColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 0.6).cgColor
        clinicDetailImgShadowView.layer.shadowOpacity = 0.6
        
        let clinicImageRingWidth: CGFloat = clinicDetailImageRingView.frame.size.width
        clinicDetailImageRingView.layer.cornerRadius = clinicImageRingWidth / 2
        clinicDetailImageRingView.layer.borderColor = UIColor(red: 0/255, green: 199/255, blue: 162/255, alpha: 1).cgColor
        clinicDetailImageRingView.layer.borderWidth = 2
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initClinicInfo()
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(ClinicDetailViewController.refreshClinicDetailPage), for: UIControl.Event.valueChanged)
        self.clinicDetailTableView?.addSubview(self.refreshControl)
        clinicWeekSegmented.addTarget(self, action: #selector(ClinicDetailViewController.segmentedValueChanged(_:)), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ClinicDetailViewController.popPage), name: NSNotification.Name(rawValue: "popPage"), object: nil)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(ClinicDetailViewController.initClinicInfo), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initClinicInfo()
        
    }
    
    @objc func popPage() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @objc func goToLogin(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.memberCentrePage) as? MemberCentreViewController{
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    @objc func goToClinicIntrodctionPage() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicIntroductionPage) as?
            ClinicIntroductionViewController {
            controller.clinicInfo = self.clinicInfo
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.76)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @objc func refreshClinicDetailPage() {
        initClinicInfo()
        self.refreshControl.endRefreshing()
    }
    
    @IBAction func showCalendar(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.schedulePage) as?
            ScheduleViewController {
            controller.schedulePics = self.schedulePic
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.76)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
        
    func isQRcodeNavigation() {
        if isQRcode == true && self.addFavoriteBtn.isSelected == false {
            let controller = UIAlertController(title: "是否要加入最愛", message: nil, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
            controller.addAction(cancelAction)
            
            let okAction = UIAlertAction(title: "確定", style: .default) { (_) in
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                let FavoriteInfo = Favorite(context: context)
                FavoriteInfo.clinicAddress = self.clinicInfo!["address"].rawString()
                FavoriteInfo.clinicName = self.clinicInfo!["name"].rawString()
                FavoriteInfo.clinicPhone = self.clinicInfo!["contact"].rawString()
                FavoriteInfo.clinicID = self.clinicInfo!["clinic_id"].rawString()
                //            FavoriteInfo.clinicImg = self.clinicImageView.image!.pngData()
                FavoriteInfo.clinicPicAddress = self.clinicInfo!["pic"].rawString()
                FavoriteInfo.clinicUpdateTime = self.clinicInfo!["updated_time"].rawString()
                appDelegate.saveContext()
                let notiName = Notification.Name("addBadge")
                NotificationCenter.default.post(name: notiName, object: nil)
                
                /// - Remark: animation 開啟執行完畢後關閉
                self.favoriteAnimationView.isHidden = false
                self.favoriteAnimationView.play { (false) in
                    self.favoriteAnimationView.isHidden = true
                }
                self.addFavoriteBtn.isSelected = true
            }
            controller.addAction(okAction)
            
            present(controller, animated: true, completion: nil)
        }
    }
    var isLogin = true
    
    @IBAction func gotoAppt(_ sender: UIButton) {
        if isLogin == false{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier:StoryboardID.signInPage) as? SignInViewController{
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(controller, animated: true, completion: nil)
            }
        }
        else{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier:StoryboardID.reservationPage) as? ReservationViewController{
                controller.clinicName = clinicDetailNameLabel.text!
                navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func initUINavRightButton() {
        favoriteAnimationView.frame = CGRect(x: -44, y: -44, width: 120, height: 120)
        favoriteAnimationView.contentMode = .scaleAspectFill
        favoriteAnimationView.animationSpeed = 0.5
        favoriteAnimationView.isHidden = true
        
        addFavoriteBtn.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        addFavoriteBtn.setImage(UIImage(named: "icon_like"), for: UIControl.State.normal)
        addFavoriteBtn.setImage(UIImage(named: "like"), for: UIControl.State.selected)
        addFavoriteBtn.addSubview(favoriteAnimationView)
        addFavoriteBtn.addTarget(self, action: #selector(ClinicDetailViewController.favoriteBtnPressed(_:)), for: .touchUpInside)
        
        toMemberBtn.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        toMemberBtn.setImage(UIImage(named: "icon_member"), for: .normal)
        toMemberBtn.addTarget(self, action: #selector(ClinicDetailViewController.goToLogin), for: .touchUpInside)
        
        let clinicDetailInfoBtn = UIButton()
        clinicDetailInfoBtn.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        clinicDetailInfoBtn.setImage(UIImage(named: "icon_doctor"), for: UIControl.State.normal)
        clinicDetailInfoBtn.addTarget(self, action: #selector(ClinicDetailViewController.goToClinicIntrodctionPage), for: .touchUpInside)
        
        let favoriteBtn = UIBarButtonItem(customView: addFavoriteBtn)
        let detailInfoBtn = UIBarButtonItem(customView: clinicDetailInfoBtn)
        let memberBtn = UIBarButtonItem(customView: toMemberBtn)
        
        if self.clinicInfo!["isDetailOpen"].rawString() == "true" {
            self.navigationItem.rightBarButtonItems = [memberBtn, favoriteBtn, detailInfoBtn]
        } else {
            self.navigationItem.rightBarButtonItems = [memberBtn, favoriteBtn]
        }
    }
    
    @objc func favoriteBtnPressed(_ sender: UIButton) {
        if sender.isSelected == true {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                readFavoriteInfos = try context.fetch(Favorite.fetchRequest())
            } catch {
                print("error")
            }
            
            for i in readFavoriteInfos {
                if i.clinicName == clinicDetailNameLabel.text {
                    context.delete(i)
                }
            }
            let notiName = Notification.Name("cancelBadge")
            NotificationCenter.default.post(name: notiName, object: nil)
            sender.isSelected = false
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let FavoriteInfo = Favorite(context: context)
            FavoriteInfo.clinicAddress = self.clinicInfo!["address"].rawString()
            FavoriteInfo.clinicName = self.clinicInfo!["name"].rawString()
            FavoriteInfo.clinicPhone = self.clinicInfo!["contact"].rawString()
            FavoriteInfo.clinicID = self.clinicInfo!["clinic_id"].rawString()
            //            FavoriteInfo.clinicImg = self.clinicImageView.image!.pngData()
            for i in self.clinicInfo!["pics"].array! {
                if i["type"].string == "Cover" {
                    FavoriteInfo.clinicPicAddress = i["url"].string!
                }
            }
            FavoriteInfo.clinicUpdateTime = self.clinicInfo!["updated_time"].rawString()
            appDelegate.saveContext()
            let notiName = Notification.Name("addBadge")
            NotificationCenter.default.post(name: notiName, object: nil)
            
            /// - Remark: animation 開啟執行完畢後關閉
            favoriteAnimationView.isHidden = false
            favoriteAnimationView.play { (false) in
                self.favoriteAnimationView.isHidden = true
            }
            sender.isSelected = true
        }
    }
    
    @objc func initClinicInfo() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        do {
            readFavoriteInfos = try context.fetch(Favorite.fetchRequest())
        } catch {
            print("error")
        }
        RestAPI.shared.getClinicInfo(clinicID: self.clinicID!) {(response)->Void in
            if (response != JSON.null) {
                self.clinicInfo = JSON(response)
                self.clinicDetailNameLabel.text = self.clinicInfo!["name"].rawString()
                self.clinicDetailAddressTextView.text = self.clinicInfo!["address"].rawString()
                
                if self.clinicInfo!["extension"].rawString() == "null" {
                    self.clinicDetailPhoneTextView.text = self.clinicInfo!["contact"].rawString()! + ""
                } else {
                    self.clinicDetailPhoneTextView.text = self.clinicInfo!["contact"].rawString()! + "" + self.clinicInfo!["extension"].rawString()!
                }
                
                if self.mTimes == 0 {
                    for i in self.clinicInfo!["pics"].array! {
                        if i["type"].string == "Cover" {
                            self.clinicDetailImageView.sd_setImage(with: URL(string: i["url"].string!), placeholderImage: UIImage(named: "page-icon"))
                        } else if i["type"].string == "Schedule" {
                            self.schedulePic.append(i)
                            self.calenderBtn.isHidden = false
                        }
                    }
                    self.mTimes += 1
                }
                
                
                self.clinicAnnouncementLabel.text = self.clinicInfo!["announcement"].rawString()
                self.roomInfo = self.clinicInfo!["room"].array
                self.weekTabValueInti()
                
                let morningFrom = self.clinicInfo!["morningFrom"].rawString()
                let morningTo = self.clinicInfo!["morningTo"].rawString()
                let afternoonFrom = self.clinicInfo!["afternoonFrom"].rawString()
                let afternoonTo = self.clinicInfo!["afternoonTo"].rawString()
                let eveningFrom = self.clinicInfo!["eveningFrom"].rawString()
                let eveningTo = self.clinicInfo!["eveningTo"].rawString()
                
                self.clinicStartEndTime = [[morningFrom, morningTo],[afternoonFrom, afternoonTo],[eveningFrom, eveningTo]] as! [[String]]
                
                self.initUINavRightButton()
                self.defineCurrentTimePeriod(self.clinicStartEndTime)
                self.clinicDetailTableView.reloadData()
                
                var tmpReadFavoriteInfos:[String] = []
                for i in self.readFavoriteInfos {
                    tmpReadFavoriteInfos.append(i.clinicName!)
                }
                /// - Precondition: if array contains roomID print_error，else save to coreData
                if tmpReadFavoriteInfos.contains(self.clinicDetailNameLabel.text!) {
                    self.addFavoriteBtn.isSelected = true
                } else {
                    self.addFavoriteBtn.isSelected = false
                }
                self.isQRcodeNavigation()
            } else {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.alertPage) as?
                    ShowAlertViewController {
                    controller.content = "網路連線不穩，請確認網路狀態！"
                    controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                    controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                    controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(controller, animated: true, completion: nil)
                }
                print("clinicList Error:",response)
            }
        }
    }
    
    func defineCurrentTimePeriod(_ timeArray: [[String]]) {
        var currentTime = DateUtil.shared.getCurrentHHmm()
        switch currentTime.convertStringToInt() {
        case timeArray[0][0].convertStringToInt()..<timeArray[1][0].convertStringToInt():
            currentTimeString = "M"
        case timeArray[1][0].convertStringToInt()..<timeArray[2][0].convertStringToInt():
            currentTimeString = "A"
        case timeArray[2][0].convertStringToInt()..<2400:
            currentTimeString = "E"
        default:
            currentTimeString = "M"
        }
    }
    
    func weekTabValueInti() {
        weekTabValue = DateUtil.shared.getStrSysWeekDay()
        if weekTabValue == "monday" {
            clinicWeekSegmented.selectedSegmentIndex = 0
        } else if weekTabValue == "tuesday" {
            clinicWeekSegmented.selectedSegmentIndex = 1
        } else if weekTabValue == "wednesday" {
            clinicWeekSegmented.selectedSegmentIndex = 2
        } else if weekTabValue == "thursday" {
            clinicWeekSegmented.selectedSegmentIndex = 3
        } else if weekTabValue == "friday" {
            clinicWeekSegmented.selectedSegmentIndex = 4
        } else if weekTabValue == "saturday" {
            clinicWeekSegmented.selectedSegmentIndex = 5
        } else if weekTabValue == "sunday" {
            clinicWeekSegmented.selectedSegmentIndex = 6
        } else {
            clinicWeekSegmented.selectedSegmentIndex = 7
        }
    }
    
    @objc func segmentedValueChanged(_ sender: UISegmentedControl!) {
        switch sender.selectedSegmentIndex {
        case 0:
            weekTabValue = "monday"
            self.clinicDetailTableView.reloadData()
        case 1:
            weekTabValue = "tuesday"
            self.clinicDetailTableView.reloadData()
        case 2:
            weekTabValue = "wednesday"
            self.clinicDetailTableView.reloadData()
        case 3:
            weekTabValue = "thursday"
            self.clinicDetailTableView.reloadData()
        case 4:
            weekTabValue = "friday"
            self.clinicDetailTableView.reloadData()
        case 5:
            weekTabValue = "saturday"
            self.clinicDetailTableView.reloadData()
        case 6:
            weekTabValue = "sunday"
            self.clinicDetailTableView.reloadData()
        default:
            weekTabValue = "monday"
            self.clinicDetailTableView.reloadData()
        }
    }
}

extension ClinicDetailViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else {
            return self.roomInfo?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellID.clinicTimeTableCell, for: indexPath) as! ClinicTimeTbCell
            let schedule = categories[indexPath.row]
            cell.clinicTimeTableImg.image = UIImage(named: schedule.imageView)
            cell.clinicTimeTableImg.contentMode = .scaleAspectFit
            cell.clinicTimeTitleLabel.text = schedule.title
            cell.clinicStartTime.text = self.clinicStartEndTime[indexPath.row][0]
            cell.clinicEndTime.text = self.clinicStartEndTime[indexPath.row][1]
            
            let doctors = self.clinicInfo?["schedule"][self.weekTabValue][schedule.content].array
            var tmp = String()
            cell.clinicTimeContentLabel.text = "休診,"
            for i in doctors ?? [] {
                tmp.append(i.rawString()!+"\n")
                cell.clinicTimeContentLabel.text = tmp
            }
            cell.clinicTimeContentLabel.text?.removeLast()
            cell.isUserInteractionEnabled = false
            
            let tmpBool:[Bool] = [false, false, true]
            cell.underLine.isHidden = tmpBool[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellID.clinicRoomsListCell, for: indexPath) as! ClinicRoomsTbCell
            cell.roomNameLabel.text = self.roomInfo![indexPath.row]["name"].rawString()
            cell.roomNameLabel.addCharactersSpacing(6)
            //            cell.roomNameLabel.text?.removeLast()
            cell.roomUpdateTimeLabel.text = "更新於:"+DateUtil.shared.getStrSysDate(withFormat: "yyyy/MM/dd HH:mm")
            cell.roomCurrentNumLabel.text = self.roomInfo![indexPath.row]["currentNum"].rawString()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.apptSettingsPage) as?
            ApptSettingsViewController {
            
            controller.clinicID = self.clinicID
            controller.clinicName = self.clinicDetailNameLabel.text
            for i in self.clinicInfo!["pics"].array! {
                if i["type"].string == "Cover" {
                    controller.clinicPicAddress = i["url"].string!
                }
            }
            controller.roomID = self.roomInfo![indexPath.row]["room_id"].int!
            controller.roomName = self.roomInfo![indexPath.row]["name"].rawString()
            controller.roomCurNum = self.roomInfo![indexPath.row]["currentNum"].rawString()
            controller.curDayPartsString = self.currentTimeString
            
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            present(controller, animated: true, completion: nil)
        }
    }
}

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func convertStringToInt() -> Int {
        var tmp = self
        tmp.remove(at: self.index(self.startIndex, offsetBy:2))
        return Int(tmp) ?? 0
    }
}

extension UITextView {
    func centerContentVertically() {
        let fitSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fitSize)
        let heightOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(0, heightOffset)
        contentOffset.y = -positiveTopOffset
    }
}






