//
//  BirthDatePickerViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/28.
//

import UIKit

class BirthDatePickerViewController: UIViewController {

    @IBOutlet weak var birthDatePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        birthDatePicker.timeZone = TimeZone(identifier: "Asia/Taipei")!
        birthDatePicker.locale = Locale(identifier: "zh_Hant_TW")
        if #available(iOS 13.4, *){
            birthDatePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    @IBAction func gobackNew(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    var dateString:String = ""
    @IBAction func dateClicked(_ sender: UIButton) {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW")
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        dateFormatter.dateFormat = "YYYY/MM/dd"
        dateString = dateFormatter.string(from: birthDatePicker.date)
        print("\(dateString)")
    }
}
