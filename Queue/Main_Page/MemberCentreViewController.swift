//
//  MemberCentreViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/14.
//

import UIKit

class MemberCentreViewController: UIViewController {
    var dataDescription = ["會員資料修改", "親友管理", "掛號記錄"]
    @IBOutlet weak var memberCentreTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backButtonTitle = "列表"
    }
    @IBAction func unwindToMemberCentre(_ unwindSegue: UIStoryboardSegue) {
        //let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MemberCentreViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(MemberCentreTableViewCell.self)", for: indexPath) as? MemberCentreTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.recordLabel.text = dataDescription[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = memberCentreTableView.cellForRow(at: indexPath) as? MemberCentreTableViewCell else { return }
        if cell.recordLabel.text == "會員資料修改"{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.memberAlterPage) as? MemberAlterViewController{
                navigationController?.pushViewController(controller, animated: true)
            }
        }
        else if cell.recordLabel.text == "親友管理"{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.managementPage) as? ManagementViewController{
                navigationController?.pushViewController(controller, animated: true)
            }
        }
        else{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.regdRecordPage) as? RegdRecordViewController{
                navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    
}
