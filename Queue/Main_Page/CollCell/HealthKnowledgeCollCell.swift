//
//  HealthKnowledgeCollCell.swift
//  Queue
//
//  Created by Andrew on 2020/8/11.
//

import UIKit

class HealthKnowledgeCollCell: UICollectionViewCell {
    @IBOutlet weak var HealthTypeLabel: UILabel!
    @IBOutlet weak var HealthSelectedView: UIView!
    
}
