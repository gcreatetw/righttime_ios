//
//  HealthKnowledgeVC.swift
//  Queue
//
//  Created by Andrew on 2020/8/11.
//

import UIKit
import SwiftyJSON

enum TableCategory: String, CaseIterable {
    case subCategory = "subCategory"
    case titleCategory = "titleCategory"
    case articleCategory = "articleCategory"
}

class HealthKnowledgeVC: UIViewController {
    
    @IBOutlet weak var HealthKnowledgeTbView: UITableView!
    
    var selectedIndex = Int()
    let sections = TableCategory.allCases
    let viewModel = HealthKnowledgeViewModel()
    var heHoCategory: HeHoCategroyDataModel?
    var heHoArticle: HeHoArticleDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        viewModel.paretController = self
        viewModel.callHeHoCategory()
        viewModel.callHeHoArticle(type_id: "6", before: "")
        
        
    }
    
}

extension HealthKnowledgeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let totalSections = sections[section]
        
        switch totalSections {
        case .titleCategory:
            return 1
        case .subCategory:
            return 1
        case .articleCategory:
            return self.heHoArticle?.articleArr.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let totalSections = sections[indexPath.section]
        
        switch totalSections {
        case .subCategory:
            return 44
        case .titleCategory:
            return 70
        case .articleCategory:
            if selectedIndex == 0 {
                return 450
            } else {
                return 300
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let totalSections = sections[indexPath.section]
        
        switch totalSections {
        case .subCategory:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HealthKnowledgeTbCell", for: indexPath) as! HealthKnowledgeTbCell
            cell.reloadColl()
            
            return cell
            
        case .titleCategory:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleTbCell", for: indexPath) as! TitleTbCell
            cell.titleLabel.text = self.heHoCategory?.categoryArr[selectedIndex].name
            
            return cell
            
        case .articleCategory:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTbCell", for: indexPath) as! ArticleTbCell
            cell.postDateLabel.text = self.heHoArticle?.articleArr[indexPath.row].postDate
            cell.titleLabel.text = self.heHoArticle?.articleArr[indexPath.row].postTitle
            let newUrl = self.heHoArticle?.articleArr[indexPath.row].feturedImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? ""
            
            cell.bannerImg.sd_setImage(with: URL(string: newUrl), placeholderImage: UIImage(named: "imageDemo"))
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let heHoDetailVC = storyboard?.instantiateViewController(withIdentifier: "HeHoDetailVC") as! HeHoDetailVC
            heHoDetailVC.url = self.heHoArticle?.articleArr[indexPath.row].guid
            self.navigationController?.pushViewController(heHoDetailVC, animated: true)
        }
    }
}


extension HealthKnowledgeVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.heHoCategory?.categoryArr.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HealthKnowledgeCollCell", for: indexPath) as! HealthKnowledgeCollCell
        cell.HealthTypeLabel.text = self.heHoCategory?.categoryArr[indexPath.row].name
        cell.HealthSelectedView.isHidden = selectedIndex == indexPath.row ? false : true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        
        let indexPath = IndexPath.init(row: 0, section: 1)
        self.HealthKnowledgeTbView.reloadRows(at: [indexPath], with: .fade)
        
        var typeId = String(self.heHoCategory?.categoryArr[selectedIndex].type_id ?? 0)
        viewModel.callHeHoArticle(type_id: typeId, before: "")
        
        collectionView.reloadData()
    }
}

extension HealthKnowledgeVC: HealthKnowledgeViewModelDelegate {
    
    func onFetchCategoryCompleted(json: JSON) {
        self.heHoCategory = HeHoCategroyDataModel(fromJson: json)
        self.HealthKnowledgeTbView.reloadData()
    }
    
    func onFetchArticleCompleted(json: JSON) {
        self.heHoArticle = HeHoArticleDataModel(fromJson: json)
        self.HealthKnowledgeTbView.reloadSections(IndexSet(integersIn: 2...2), with: .fade)
    }
    
    func onFetchFailed() {
        
    }
    
}

extension String {
    func utf8EncodedString()-> String {
         let messageData = self.data(using: .nonLossyASCII)
         let text = String(data: messageData!, encoding: .utf8)
        return text ?? ""
    }
}
