//
//  PharmacyPickerViewController.swift
//  Queue
//
//  Created by Andrew on 2020/2/6.
//

import UIKit

class PharmacyPickerViewController: UIViewController {

    
    @IBOutlet weak var pharmacyPicker: UIPickerView!
    @IBOutlet weak var titleLabel: ClassLabelSizeClass!
    var isCitiesPicker: Bool?
    var citiesArrSorted = [String]()
    var districtArr = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whichOneIsChosen()
        


        // Do any additional setup after loading the view.
    }
    
    func whichOneIsChosen() {
        switch isCitiesPicker {
        case true:
            let tmpArray = sortedPharmacyArray()
            var tmpInt: Int?
            for i in 0 ..< tmpArray.count {
                if tmpArray[i] == UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyCity)! {
                    pharmacyPicker.selectRow(i, inComponent: 0, animated: true)
                    pharmacyPicker.reloadAllComponents()
                }
            }
        case false:
            let districtArr = [String](PharmacyList.addressDict[UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyCity)!]!)
            var tmpInt: Int?
            for i in 0 ..< districtArr.count {
                if districtArr[i] == UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyDistrict)! {
                    pharmacyPicker.selectRow(i, inComponent: 0, animated: true)
                    pharmacyPicker.reloadAllComponents()
                }
            }
        default:
            print("default")
        }
    }
    
    func sortedPharmacyArray() -> [String] {
        let sortedData = PharmacyList.addressDict.keys.sorted(by: { firstKey, secondKey in
            let key1Index = firstKey.index(firstKey.startIndex, offsetBy: 3) // 取 Item 字串後的值出來做排序
            let key1 = Int(firstKey.substring(from: key1Index)) // 5
            
            let key2Index = secondKey.index(secondKey.startIndex, offsetBy: 3) // 取 Item 字串後的值出來做排序
            let key2 = Int(secondKey.substring(from: key2Index)) // 6
            
            return key1! < key2! // 由小到大排序
        })
        return [String](sortedData)
    }
    
    func organizePharmacyList() -> [String] {
        citiesArrSorted = sortedPharmacyArray()
        var citiesArrSortedAndTrim = [String]()
        for item in citiesArrSorted {
            let startIndex = item.index(item.startIndex, offsetBy: 3)
            let endIndex = item.index(item.startIndex, offsetBy: 4)
            var tmpItme = item
            tmpItme.removeSubrange(startIndex...endIndex)
            citiesArrSortedAndTrim.append(tmpItme)
        }
        return citiesArrSortedAndTrim
    }
    
    @IBAction func backToPharmacyListPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        if isCitiesPicker! {
            UserDefaults.standard.set(citiesArrSorted[pharmacyPicker.selectedRow(inComponent: 0)], forKey: UserDefaultsID.pharmacyCity)
            districtArr = [String](PharmacyList.addressDict[UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyCity)!]!)
            UserDefaults.standard.set(districtArr[0], forKey: UserDefaultsID.pharmacyDistrict)
        } else {
            UserDefaults.standard.set(districtArr[pharmacyPicker.selectedRow(inComponent: 0)], forKey: UserDefaultsID.pharmacyDistrict)
        }
        NotificationCenter.default.post(name: Notification.Name("changeConditions"), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

extension PharmacyPickerViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isCitiesPicker! {
            return PharmacyList.addressDict.count
        } else {
            return PharmacyList.addressDict[UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyCity)!]!.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
           return 36
       }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        pickerLabel.textAlignment = NSTextAlignment.center

        if isCitiesPicker! {
            let tmpList = self.organizePharmacyList()
            pickerLabel.text = tmpList[row]
            return pickerLabel
        } else {
            districtArr = [String](PharmacyList.addressDict[UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyCity)!]!)
            pickerLabel.text = districtArr[row]
            return pickerLabel
        }
    }
    
}
