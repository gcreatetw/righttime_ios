//
//  SectionView.swift
//  Queue
//
//  Created by Andrew on 2019/7/25.
//

import UIKit

protocol SectionViewDelegate: class {
    func sectionView(_ sectionView: SectionView, _ didPressTag: Int, _ isExpand: Bool)
}

class SectionView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    
    weak var delegate: SectionViewDelegate?
    var buttonTag: Int!
    var isExpand: Bool! // cell 的狀態(展開/縮合)
    
    @IBAction func arrowBtnPressed(_ sender: UIButton) {
        self.delegate?.sectionView(self, self.buttonTag, self.isExpand)
    }
}
