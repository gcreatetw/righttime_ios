//
//  HomeCategoryCollectionViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/2/21.
//

import UIKit

class ClinicsTypeCollCell: UICollectionViewCell {
    
    @IBOutlet weak var CategoryImageView: UIImageView!
    @IBOutlet weak var CategoryTitle: UILabel!
    @IBOutlet weak var CategoryContent: UILabel!
    
    
}
