//
//  ScheduleCollCell.swift
//  Queue
//
//  Created by Andrew on 2019/11/18.
//

import UIKit

class ScheduleCollCell: UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var scheduleImg: UIImageView!
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
       return scheduleImg
    }
}


