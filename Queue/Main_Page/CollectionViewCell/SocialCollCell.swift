//
//  SocialCollCell.swift
//  Queue
//
//  Created by Andrew on 2020/7/28.
//

import UIKit

class SocialCollCell: UICollectionViewCell {
    
    @IBOutlet weak var socialHeadShotBgView: UIView!
    @IBOutlet weak var socialHeadShotImg: UIImageView!
    @IBOutlet weak var socialHeadShotName: UILabel!
    
    override func layoutSubviews() {
        socialHeadShotImg.layer.cornerRadius = socialHeadShotImg.frame.size.height/2
        socialHeadShotBgView.layer.cornerRadius = socialHeadShotBgView.frame.size.height/2
    }
}
