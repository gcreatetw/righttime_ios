//
//  DoctorDetailCollectionViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/7/1.
//

import UIKit

class DoctorsHeadShotCollCell: UICollectionViewCell {
    
    
    @IBOutlet weak var doctorHeadShotBG: UIView!
    @IBOutlet weak var doctorHeadShot: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    
    override func layoutSubviews() {
        doctorHeadShot.layer.cornerRadius = doctorHeadShot.frame.size.height/2
        doctorHeadShotBG.layer.cornerRadius = doctorHeadShotBG.frame.size.height/2
    }
}
