//
//  ClinicIntroCollectionViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/7/10.
//

import UIKit

class ClinicIntroCollCell: UICollectionViewCell {
    
    @IBOutlet weak var clinicIntroImg: UIImageView!
    @IBOutlet weak var clinicIntroTitle: UILabel!
    @IBOutlet weak var clinicIntroContent: ClassCellLabelSizeClass!
}
