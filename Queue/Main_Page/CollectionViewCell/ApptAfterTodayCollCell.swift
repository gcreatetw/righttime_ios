//
//  TrackingRoomsCollectionViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/7/22.
//

import UIKit

class ApptAfterTodayCollCell: UICollectionViewCell {
    
    @IBOutlet weak var apptClinicName: UILabel!
    @IBOutlet weak var apptClinicPicBgView: UIView! {
        didSet {
            //            apptClinicPicBgView.layer.cornerRadius = apptClinicPicBgView.frame.size.width / 2
            apptClinicPicBgView.layer.borderColor = UIColor(red: 0, green: 199/255, blue: 162/255, alpha: 1).cgColor
            apptClinicPicBgView.layer.borderWidth = 1.6
        }
    }
    @IBOutlet weak var apptClinicPicImg: UIImageView! {
        didSet {
            //            apptClinicPicImg.layer.cornerRadius = apptClinicPicImg.frame.size.width / 2
            apptClinicPicImg.layer.borderColor = UIColor.lightGray.cgColor
            apptClinicPicImg.layer.borderWidth = 0.3
        }
    }
    @IBOutlet weak var apptDayParts: UILabel!
    @IBOutlet weak var apptDate: UILabel!
    @IBOutlet weak var apptClinicRoomName: UILabel!
    @IBOutlet weak var apptNum: UILabel!
    
    override func layoutSubviews() {
        apptClinicPicBgView.layer.cornerRadius = apptClinicPicBgView.frame.size.width / 2
        //apptClinicPicBgView.center = self.view.center
        apptClinicPicImg.layer.cornerRadius = apptClinicPicImg.frame.size.width / 2
    }
    
    var tapAction: ((UICollectionViewCell) -> Void)?
    
    @IBAction func deleteApptBtnPressed(_ sender: UIButton) {
        tapAction?(self)
    }
    
    
}
