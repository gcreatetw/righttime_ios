//
//  SignInViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/13.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var subLogin: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        registerView.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap) // to Replace "TouchesBegan"
    }
    @objc func dismissKeyBoard() {
            self.view.endEditing(true)
    }
   
    @IBAction func cancelLogin(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelRegister(_ sender: UIButton) {
        bgView.isHidden = false
        registerView.isHidden = true
    }
    @IBAction func showRegister(_ sender: UIButton) {
        bgView.isHidden = true
        registerView.isHidden = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
