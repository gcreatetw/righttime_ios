//
//  PickerViewController.swift
//  Queue
//
//  Created by Andrew on 2019/3/11.
//

import UIKit
import SwiftyJSON
import Alamofire

class ClinicPickerViewController: UIViewController {
    
    @IBOutlet weak var clinicListPicker: UIPickerView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var distanceNotifyLabel: ClassLabelSizeClass!
    
    /// - Remark: 判斷是 cityList or typeList
    var isCityListPicker: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        getPickerList()
    }
    
    func getPickerList() {
        if isCityListPicker! {
            distanceNotifyLabel.isHidden = false
            if cityLists == nil {
                RestAPI.shared.getCityList { (response)->Void in
                    if (response != JSON.null) {
                        cityLists = response.array!
                        cityLists?.insert(["name": "周邊熱搜","city_id": ""], at: 0)
                        self.selectedCityRow()
                    }
                }
            } else {
                self.selectedCityRow()
            }
        } else {
            distanceNotifyLabel.isHidden = true
            if typeLists == nil {
                RestAPI.shared.getCityList { (response)->Void in
                    if (response != JSON.null) {
                        typeLists = response.array!
                        typeLists?.insert(["type_id": "","name": "全部科別"], at: 0)
                        self.selectedTypeRow()
                    }
                }
            } else {
                self.selectedTypeRow()
            }
        }
    }
    
    @IBAction func goBackToClinicListPage(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        if cityLists == nil || typeLists == nil {
            
        } else {
            if isCityListPicker! {
                guard clinicListPicker.selectedRow(inComponent: 0) == 0 else {
                    UserDefaults.standard.set(cityLists![clinicListPicker.selectedRow(inComponent: 0)]["city_id"].rawString(), forKey: UserDefaultsID.cityID)
                    UserDefaults.standard.set(false, forKey: UserDefaultsID.isCenter)
                    NotificationCenter.default.post(name: Notification.Name("changeCondition"), object: nil)
                    self.performSegue(withIdentifier: SegueID.backToClinicListPage, sender: nil)
                    return
                }
                UserDefaults.standard.set("", forKey: UserDefaultsID.cityID)
                UserDefaults.standard.set(true, forKey: UserDefaultsID.isCenter)
            } else {
                UserDefaults.standard.set(typeLists![clinicListPicker.selectedRow(inComponent: 0)]["type_id"].rawString(), forKey: UserDefaultsID.typeID)
            }
        }
        NotificationCenter.default.post(name: Notification.Name("changeCondition"), object: nil)
        self.performSegue(withIdentifier: SegueID.backToClinicListPage, sender: nil)
    }
    
}

extension ClinicPickerViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isCityListPicker! {
            return cityLists?.count ?? 1
        } else {
            return typeLists?.count ?? 1
        }
    }
    
    /**
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
     
     let pickerLabel = UILabel()
     pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 4)
     pickerLabel.textAlignment = NSTextAlignment.center
     
     if isCityPicker! {
     return cityLists?[row]["name"].rawString() ?? "請確認您的網路狀態"
     } else {
     return typeLists?[row]["name"].rawString() ?? "請確認您的網路狀態"
     }
     }
     */
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 36
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let pickerLabel = UILabel()
        pickerLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        pickerLabel.textAlignment = NSTextAlignment.center
        
        if isCityListPicker! {
            pickerLabel.text = cityLists?[row]["name"].rawString() ?? "請確認您的網路狀態"
            return pickerLabel
        } else {
            pickerLabel.text = typeLists?[row]["name"].rawString() ?? "請確認您的網路狀態"
            return pickerLabel
        }
    }
    
    /// - Todo: 改在這邊寫入 userdefaults，改善選擇效率
    /**
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     if isCityListPicker! {
     citySelectedValue = row
     if cityLists != nil {
     guard citySelectedValue == 0 else {
     UserDefaults.standard.set(cityLists![row]["city_id"].rawString(), forKey: UserDefaultsID.cityID)
     UserDefaults.standard.set(false, forKey: UserDefaultsID.isCenter)
     return
     }
     UserDefaults.standard.set("", forKey: UserDefaultsID.cityID)
     UserDefaults.standard.set(true, forKey: UserDefaultsID.isCenter)
     }
     } else {
     typeSelectedValue = row
     if typeLists != nil {
     UserDefaults.standard.set(typeLists![row]["type_id"].rawString(), forKey: UserDefaultsID.typeID)
     }
     }
     }
     */
    
}

extension ClinicPickerViewController {
    func selectedCityRow() {
        for i in 0 ..< cityLists!.count {
            if UserDefaults.standard.string(forKey: UserDefaultsID.cityID) == cityLists![i]["city_id"].rawString()! {
                self.clinicListPicker.selectRow(i, inComponent: 0, animated: true)
                self.clinicListPicker.reloadAllComponents()
                return
            }
        }
    }
    
    func selectedTypeRow() {
        for i in 0 ..< typeLists!.count {
            if UserDefaults.standard.string(forKey: UserDefaultsID.typeID) == typeLists![i]["type_id"].rawString()! {
                self.clinicListPicker.selectRow(i, inComponent: 0, animated: true)
                self.clinicListPicker.reloadAllComponents()
                return
            }
        }
    }
}


