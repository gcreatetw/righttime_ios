//
//  TeachingViewController.swift
//  Queue
//
//  Created by Andrew on 2019/6/18.
//

import UIKit

class TeachingViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollviewContainer: UIView!
    @IBOutlet weak var nextPageButton: UIButton!
    
    let teachingImage: [String] = [
        "teach_page_1",
        "teach_page_2",
        "teach_page_3",
        "teach_page_3_1",
        "teach_page_4",
        "teach_page_5",
        "teach_page_6"
    ]
    var scrollView: UIScrollView!
    var pageControl: UIPageControl!
    var currentPageIndex: Int = 0
    
    
    override func viewDidLayoutSubviews() {
        
        let w: CGFloat = scrollviewContainer.frame.size.width
        let h: CGFloat = scrollviewContainer.frame.size.height
        
        // 產生UIScrollView
        self.scrollView = UIScrollView()
        self.scrollView!.frame = CGRect(x: 0, y: 0, width: w, height: h)
        self.scrollView!.contentSize = CGSize(width: w * CGFloat(self.teachingImage.count), height: h)
        self.scrollView!.backgroundColor = UIColor.clear
        self.scrollView.layer.cornerRadius = 5
        self.scrollView!.showsVerticalScrollIndicator = true // 顯示右側垂直拉bar條
        self.scrollView!.showsHorizontalScrollIndicator = false // 隱藏底部平行拉bar條
        self.scrollView!.isPagingEnabled = true // 設定能以本身容器的寬度做切頁(橫向的scrollView)
        self.scrollView!.delegate = self
        
        // 生成UIPageControl(頁面數量顯示器)
        self.pageControl = UIPageControl()
        self.pageControl.frame = CGRect(x: 0, y: h+6, width: w, height: 20)
        self.pageControl.numberOfPages = teachingImage.count
        self.pageControl.currentPage = 0
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.currentPageIndicatorTintColor = UIColor(red: 1, green: 199/255, blue: 5/255, alpha: 1)
        
        self.scrollviewContainer.addSubview(self.scrollView)
        self.scrollviewContainer.addSubview(self.pageControl)
        
        // 動態產生三張電影圖片
        var imageView: UIImageView!
        for i in 0 ..< teachingImage.count {
            imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.frame = CGRect(x: w * CGFloat(i), y: 0, width: w, height: h)
            imageView.image = UIImage(named: self.teachingImage[i])
            self.scrollView!.addSubview(imageView)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func goBackHomePage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // 利用scrollView的容器內X座標算出UIPageController要顯示第幾頁
        let currentPage: CGFloat = (scrollView.contentOffset.x / scrollviewContainer.frame.size.width) + 0.1
        currentPageIndex = Int(currentPage)
        self.pageControl.currentPage = Int(currentPage)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if currentPageIndex < 5 {
            currentPageIndex += 1
            scrollView.contentOffset.x += scrollviewContainer.frame.size.width
            self.pageControl.currentPage = currentPageIndex
        } else {
            currentPageIndex = 0
            scrollView.contentOffset.x = 0
            self.pageControl.currentPage = 0
        }
    }
    
    @IBAction func previewClicked(_ sender: Any) {
        if currentPageIndex > 0 {
            currentPageIndex -= 1
            scrollView.contentOffset.x -= scrollviewContainer.frame.size.width
            self.pageControl.currentPage = currentPageIndex
        } else {
            currentPageIndex = 5
            scrollView.contentOffset.x = scrollviewContainer.frame.size.width * 5
            self.pageControl.currentPage = 5
        }
    }
    
}
