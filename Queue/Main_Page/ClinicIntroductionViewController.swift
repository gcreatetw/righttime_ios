//
//  ClinicIntroductionViewController.swift
//  Queue
//
//  Created by Andrew on 2019/7/2.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

class ClinicIntroductionViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var clinicIntroTableView: UITableView! {
        didSet {
            self.clinicIntroTableView.clipsToBounds = true
            self.clinicIntroTableView.layer.cornerRadius = 15
        }
    }
    
    @IBOutlet weak var backgroundView: UIView! {
        didSet {
            self.backgroundView.clipsToBounds = true
            self.backgroundView.layer.cornerRadius = 15
        }
    }
    
    var currentPageIndex: Int = 0
    var selectedIndex = Int()
    var clinicInfo: JSON?
    var doctors: [JSON]?
    var pics: [JSON]?
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    var isExpand = true
    var isLatestNews = false
    var latestNews = [JSON]()
    var updateSocailTimes = 0
    
    struct socialType {
        var type: String
        var ID: String
        var Img: String
        var color: UIColor
    }
    
//    var socialTypes = [
//        socialType(type: "官方網站", ID: "", Img: "doctor-web", color: UIColor(red: 25/255, green: 119/255, blue: 243/255, alpha: 1)),
//        socialType(type: "Line", ID: "", Img: "line2019", color: UIColor(red: 0/255, green: 195/255, blue: 0/255, alpha: 1)),
//        socialType(type: "Facebook", ID: "", Img: "facebook2019", color: UIColor(red: 25/255, green: 119/255, blue: 243/255, alpha: 1)),
//        socialType(type: "最新消息", ID: "", Img: "doctor-news", color: UIColor(red: 255/255, green: 199/255, blue: 5/255, alpha: 1))
//    ]
    
    var socialTypes = [
           socialType(type: "官方網站", ID: "", Img: "doctor-web", color: UIColor(red: 25/255, green: 119/255, blue: 243/255, alpha: 1)),
           socialType(type: "Line", ID: "", Img: "line2019", color: UIColor(red: 0/255, green: 195/255, blue: 0/255, alpha: 1)),
           socialType(type: "Facebook", ID: "", Img: "facebook2019", color: UIColor(red: 25/255, green: 119/255, blue: 243/255, alpha: 1))
       ]
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateUI()
    }
    
    func updateUI() {
        doctors = clinicInfo!["doctors"].array
        pics = clinicInfo!["pics"].array
        
        if let socialInfos = clinicInfo!["socialInfo"].array {
            if updateSocailTimes == 0 {
                socialTypes[0].ID = socialInfos[2]["website"].rawString()!
                socialTypes[1].ID = socialInfos[1]["line"].rawString()!
                socialTypes[2].ID = socialInfos[0]["facebook"].rawString()!
                //socialTypes[3].ID = socialInfos[0]["facebook"].rawString()!
                socialTypes = socialTypes.filter(){$0.ID != ""}
                socialTypes = socialTypes.filter(){$0.ID != "null"}
                updateSocailTimes += 1
            }
        }
        
        let FB_ID = clinicInfo!["socialInfo"][0]["facebook"].rawString()
        
        if FB_ID != "null" {
            
            RestAPI.shared.getLatestNews(clinic_FB_ID: FB_ID!) { (response)->Void in
                if (response != JSON.null) {
                    self.latestNews = response["data"].array ?? [JSON]()
                    self.clinicIntroTableView.reloadData()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage: CGFloat = scrollView.contentOffset.x / scrollView.frame.size.width + 0.3
        currentPageIndex = Int(currentPage)
        //        print("currentPageIndex", currentPageIndex)
    }
    
    @IBAction func backToClinicDetailPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ClinicIntroductionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLatestNews == false {
            return 5
        } else {
            return 3+latestNews.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if socialTypes.count == 0 && indexPath.section == 1 {
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "clinicIntroCell", for: indexPath) as! ClinicIntroTbCell
            var introPics = [JSON]()
            for i in pics! {
                if i["type"].string == "Intro" {
                    introPics.append(i)
                }
            }
            cell.setCollectionViewWith(picsArray: introPics, screenWidth: self.view.frame.size.width)
            
            return cell
        }
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "socialHeadShotCell", for: indexPath) as! SocialTbCell

            return cell
        }
        
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellID.doctorHeadShotCell, for: indexPath) as! DoctorsHeadShotTbCell
            
            return cell
        }
        
        if isLatestNews == false {
            if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellID.doctorSkillCell, for: indexPath) as!
                DoctorSkillTbCell
                
                cell.contentLabel.text = ""
                if self.doctors?.count > 0 {
                    if let tmpEduactionArray = self.doctors?[selectedIndex]["education"].array {
                        var tmpEduactionString = ""
                        for i in tmpEduactionArray {
                            tmpEduactionString.append(i.rawString()!+"\n")
                            cell.contentLabel.text = tmpEduactionString
                        }
                    }
                }
                
                return cell
            }
            
            if indexPath.section == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellID.doctorExperienceCell, for: indexPath) as!
                DoctorExperienceTbCell
                
                cell.contentLabel.text = ""
                if self.doctors?.count > 0 {
                    if let tmpExperienceArray = self.doctors?[selectedIndex]["experience"].array {
                        var tmpExperienceString = ""
                        for i in tmpExperienceArray {
                            tmpExperienceString.append(i.rawString()!+"\n")
                            cell.contentLabel.text = tmpExperienceString
                        }
                    }
                }
                
                return cell
            }
        } else {
            //print("indexPath.section", indexPath.section, latestNews.count)
            if (self.latestNews[indexPath.section-2]["full_picture"].rawString()! == "null") {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellID.latestNewsWithoutPicCell, for: indexPath) as! LatestNewsWithoutPicTbCell
                
                cell.clinicHeadShotImg.sd_setImage(with: URL(string: self.clinicInfo!["pic"].rawString()!), placeholderImage: UIImage(named: "page-icon"))
                cell.clinicHeadShotImg.layer.borderWidth = 0.3
                cell.clinicHeadShotImg.layer.borderColor = UIColor.lightGray.cgColor
                cell.clinicNameLabel.text = self.clinicInfo!["name"].rawString()
                cell.postedTimeLabel.text = self.latestNews[indexPath.section-2]["created_time"].rawString()
                cell.postedContentLabel.text = self.latestNews[indexPath.section-2]["message"].rawString()
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellID.latestNewsWithPicCell, for: indexPath) as! LatestNewsWithPicTbCell
                
                cell.clinicHeadShotImg.sd_setImage(with: URL(string: self.clinicInfo!["pic"].rawString()!), placeholderImage: UIImage(named: "page-icon"))
                cell.clinicHeadShotImg.layer.borderWidth = 0.3
                cell.clinicHeadShotImg.layer.borderColor = UIColor.lightGray.cgColor
                cell.clinicNameLabel.text = self.clinicInfo!["name"].rawString()
                
                var str = self.latestNews[indexPath.section-2]["created_time"].rawString()
                let otherRange = str!.index(str!.startIndex, offsetBy: 10)..<str!.endIndex
                str!.removeSubrange(otherRange)
                
                cell.postedTimeLabel.text = str
                cell.postedContentLabel.text = self.latestNews[indexPath.section-2]["message"].rawString()
                cell.postedPictureImg.sd_setImage(with: URL(string: self.latestNews[indexPath.section-2]["full_picture"].rawString()!), placeholderImage: UIImage(named: "doctor-cover"))
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 3:
            let headerView = UIView.init(frame: CGRect.init(x: 16, y: 0, width: 60, height: 26))
            headerView.backgroundColor = UIColor.white
            let label = UILabel()
            label.frame = CGRect.init(x: 10, y: 0, width: 70, height: 26)
            label.text = "專長"
            label.font = UIFont.systemFont(ofSize: 16)
            label.textColor = UIColor.white
            label.backgroundColor = UIColor.init(red: 0, green: 199/255, blue: 162/255, alpha: 1)
            label.clipsToBounds = true
            label.layer.cornerRadius = 13
            label.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
            label.textAlignment = .center
            label.addCharactersSpacing()
            headerView.addSubview(label)
            return headerView
        case 4:
            let headerView = UIView.init(frame: CGRect.init(x: 16, y: 0, width: 60, height: 26))
            headerView.backgroundColor = UIColor.white
            let label = UILabel()
            label.frame = CGRect.init(x: 10, y: 0, width: 70, height: 26)
            label.text = "經歷"
            label.font = UIFont.systemFont(ofSize: 16)
            label.textColor = UIColor.white
            label.backgroundColor = UIColor.init(red: 0, green: 199/255, blue: 162/255, alpha: 1)
            label.clipsToBounds = true
            label.layer.cornerRadius = 13
            label.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
            label.textAlignment = .center
            label.addCharactersSpacing()
            headerView.addSubview(label)
            return headerView
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isExpand {
            switch section {
            case 3: return 26
            case 4: return 26
            default: return 0
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height =  self.cellHeightsDictionary[indexPath] {
            return height
        }
        return UITableView.automaticDimension
    }
}

extension ClinicIntroductionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 1:
            return socialTypes.count
        case 2:
            return doctors?.count ?? 0
        default:
            return doctors?.count ?? 0
        }
    }
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 2
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScoialCollCell", for: indexPath) as! SocialCollCell
            
            cell.socialHeadShotImg.image = UIImage(named: self.socialTypes[indexPath.row].Img)
            cell.socialHeadShotName.text = self.socialTypes[indexPath.row].type
            cell.socialHeadShotBgView.layer.borderColor = self.socialTypes[indexPath.row].color.cgColor
            cell.socialHeadShotName.textColor = self.socialTypes[indexPath.row].color
            
            cell.layer.masksToBounds = true
            cell.socialHeadShotBgView.layer.borderWidth = 0.8
            cell.socialHeadShotImg.layer.borderWidth = 0.3
            cell.socialHeadShotImg.layer.borderColor = UIColor.white.cgColor
            return cell
            
        }
        
        if collectionView.tag == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.doctorsHeadShotCell, for: indexPath) as! DoctorsHeadShotCollCell
            cell.doctorHeadShot.sd_setImage(with: URL(string: self.doctors![indexPath.row]["pic"].rawString()!), placeholderImage: UIImage(named: "doctor-cover"))
            cell.doctorName.text = self.doctors![indexPath.row]["name"].rawString() ?? ""
            
            cell.layer.masksToBounds = true
            cell.doctorHeadShotBG.layer.borderWidth = selectedIndex == indexPath.row ? 1.6 : 0.8
            cell.doctorHeadShotBG.layer.borderColor = selectedIndex == indexPath.row ? UIColor(red: 255/255, green: 199/255, blue: 5/255, alpha: 1).cgColor : UIColor.lightGray.cgColor
            cell.doctorHeadShot.layer.borderWidth = 0.3
            cell.doctorHeadShot.layer.borderColor = UIColor.lightGray.cgColor
            cell.doctorName.textColor = selectedIndex == indexPath.row ? UIColor(red: 255/255, green: 199/255, blue: 5/255, alpha: 1) : UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            switch socialTypes[indexPath.row].type {
            case "Facebook":
                let fbURL =  "profile/" + socialTypes[indexPath.row].ID // Your FB Username here
                let appURL = URL(string: "fb://\(fbURL)")!
                if UIApplication.shared.canOpenURL(appURL) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                } else {
                    let webURL = URL(string: "https://www.facebook.com/\(fbURL)")!
                    UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                }
            case "最新消息":
                isExpand = false
                isLatestNews = true
                clinicIntroTableView.reloadData()
                
            case "Line":
                let lineURL = "ti/p/" + socialTypes[indexPath.row].ID
                let appURL = URL(string: "line://\(lineURL)")! //URL(string: "line://ti/p/%40vshsw") // Line 群組或朋友 ID
                if UIApplication.shared.canOpenURL(appURL) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                } else {
                    let lineURL = URL(string: "itms-apps://itunes.apple.com/app/id443904275")!
                    UIApplication.shared.open(lineURL, options: [:], completionHandler: nil)
                }
            case "官方網站":
                guard let url = URL(string: socialTypes[indexPath.row].ID) else { return }
                UIApplication.shared.open(url)
            default:
                print("***")
            }
        }
        
        if collectionView.tag == 2 {
            selectedIndex = indexPath.row
            isLatestNews = false
            if (self.doctors![indexPath.row]["name"].rawString() == "其他") {
                isExpand = false
            } else {
                isExpand = true
            }
            collectionView.reloadData()
            clinicIntroTableView.reloadData()
        }
    }
}

extension ClinicIntroductionViewController: UICollectionViewDelegateFlowLayout {
    /// - Remark: 設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)
    }
    /// - Remark: 滑動方向為「垂直」的話即「上下」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -2
    }
}

extension UILabel {
    func addCharactersSpacing(_ value: CGFloat = 10) {
        if let textString = text {
            let attrs: [NSAttributedString.Key : Any] = [.kern: value]
            attributedText = NSAttributedString(string: textString, attributes: attrs)
        }
    }
}
