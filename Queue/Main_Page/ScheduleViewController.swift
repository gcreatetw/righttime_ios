//
//  ScheduleViewController.swift
//  Queue
//
//  Created by Andrew on 2019/11/5.
//

import UIKit
import WebKit
import SDWebImage
import SwiftyJSON

class ScheduleViewController: UIViewController, UIScrollViewDelegate {
    
    var schedulePics = [JSON]()
    var picURL = ""
    var currentPage = 0
    @IBOutlet weak var mPageControl: UIPageControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mPageControl.numberOfPages = schedulePics.count
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        //print("currentPage:Cell", currentPage)
        self.mPageControl.currentPage = self.currentPage
    }
    
    @IBAction func backToPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ScheduleViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return schedulePics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.scheduleImgCell, for: indexPath) as! ScheduleCollCell
        
        cell.scheduleImg.sd_setImage(with: URL(string: self.schedulePics[indexPath.row]["url"].rawString()!), placeholderImage: UIImage(named: "page-icon"))
        
        return cell
    }
}

// MARK: - 設定 CollectionView Cell 與 Cell 之間的間距、距確 Super View 的距離等等
extension ScheduleViewController: UICollectionViewDelegateFlowLayout {
    
    /// - Remark: 設定 Collection View 距離 Super View上、下、左、下間的距離
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    /// - Remark: 設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: super.view.frame.size.width-30, height: super.view.frame.size.height*0.6)
        
    }
    
    //    /// - Remark: 滑動方向為「垂直」的話即「上下」的間距(預設為重直)
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 20
    //    }
    //
    //    /// - Remark: 滑動方向為「垂直」的話即「左右」的間距(預設為重直)
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 10
    //    }
}




