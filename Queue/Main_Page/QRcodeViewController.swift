//
//  QRcodeViewController.swift
//  Queue
//
//  Created by Andrew on 2019/8/26.
//

import UIKit
import AVFoundation

class QRcodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var qRcodeView: UIView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        initQRcodeReader()
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            initQRcodeReader()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                // 如果一開始不允許使用相機權限，那麼就會再次向使用者要求權限
                // 如果允許則建置 QR code 掃瞄器
                if granted {
                    self.initQRcodeReader()
                } else {
                    // 若是沒有允許使用相機權限，則跳出一個 Alert
                    // 點選取消，那麼就 dismiss 回首頁
                    let alertController = UIAlertController(title: "開啟失敗", message: "請先開啟相機權限", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: { _ in
                        self.navigationController?.popViewController(animated: true)
                    })
                    // 若點選設定，那麼則會跳到此 App 的設定畫面，可以對 App 開啟權限的設定
                    let okAction = UIAlertAction(title: "設定", style: .default, handler: { _ in
                        let url = URL(string: UIApplication.openSettingsURLString)
                        if let url = url, UIApplication.shared.canOpenURL(url) {
                            if #available(iOS 10, *) {
                                UIApplication.shared.open(url, options: [:],
                                                          completionHandler: {
                                                            (success) in
                                })
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                    })
                    alertController.addAction(cancelAction)
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
    }
    
    func initQRcodeReader() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            //failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            //failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        qRcodeView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        qRcodeView.backgroundColor = UIColor(white: 0, alpha: 0.74)
        
        // create path
        let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        
        // MARK: roundRectanglePath
        path.append(UIBezierPath(roundedRect: CGRect(x: screenWidth/2-screenWidth*0.3, y: screenHeight/2 - screenWidth*0.3, width: screenWidth*0.6, height: screenWidth*0.6), cornerRadius: 5).reversing())
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        
        qRcodeView.layer.mask = shapeLayer
        view.bringSubviewToFront(qRcodeView)
        
        captureSession.startRunning()
    }
    
    @IBAction func backToHomePage(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
            
            let myClinic = stringValue.components(separatedBy: "#")
            
            if myClinic.count > 1 && myClinic[1] == "clinicID" {
                if let controllers = navigationController?.viewControllers {
                    var newStack = [UIViewController]()
                    if controllers.count > 0 {
                        let rootVC = controllers[0]
                        newStack.append(rootVC)
                    }
                    if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicDetailPage) as? ClinicDetailViewController {
                        controller.clinicID = myClinic[2]
                        controller.isQRcode = true
                        newStack.append(controller)
                    }
                    navigationController?.setViewControllers(newStack, animated: true)
                    captureSession!.stopRunning()
                }
            } else {
                let controller = UIAlertController(title: "無效的QR碼", message: "請掃描診所特殊QR碼", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "確定", style: .default) { (_) in
                    self.navigationController?.popViewController(animated: true)
//                    self.dismiss(animated: true)
                }
                controller.addAction(okAction)
                present(controller, animated: true, completion: nil)
                
            }
        }
    }
    
    func found(code: String) {
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

//    // 用來管理擷取活動和協調輸入及輸出數據流的對象。
//    var captureSession: AVCaptureSession?
//    // 核心動畫層，可以在擷取視頻時顯示視頻。
//    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
//    // 稍後我們為了替我們的 QR Code 加上掃描框使用。
//    var qrCodeFrameView: UIView?
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
//            // 如果一開始就允許使用相機權限，那麼就建置 QR code 掃瞄器
//            configurationScanner()
//            self.addMask()
//        } else {
//            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
//                // 如果一開始不允許使用相機權限，那麼就會再次向使用者要求權限
//                // 如果允許則建置 QR code 掃瞄器
//                if granted {
//                    self.configurationScanner()
//                    self.addMask()
//                } else {
//                    // 若是沒有允許使用相機權限，則跳出一個 Alert
//                    // 點選取消，那麼就 dismiss 回首頁
//                    let alertController = UIAlertController(title: "開啟失敗", message: "請先開啟相機權限", preferredStyle: .alert)
//                    let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: { _ in
//                        self.dismiss(animated: true, completion: nil)
//                    })
//                    // 若點選設定，那麼則會跳到此 App 的設定畫面，可以對 App 開啟權限的設定
//                    let okAction = UIAlertAction(title: "設定", style: .default, handler: { _ in
//                        let url = URL(string: UIApplication.openSettingsURLString)
//                        if let url = url, UIApplication.shared.canOpenURL(url) {
//                            if #available(iOS 10, *) {
//                                UIApplication.shared.open(url, options: [:],
//                                                          completionHandler: {
//                                                            (success) in
//                                })
//                            } else {
//                                UIApplication.shared.openURL(url)
//                            }
//                        }
//                    })
//                    alertController.addAction(cancelAction)
//                    alertController.addAction(okAction)
//                    self.present(alertController, animated: true, completion: nil)
//                }
//            })
//        }
//
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        self.tabBarController?.tabBar.isHidden = true
//        //        configurationScanner()
//        //        addMask()
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
//            // 如果一開始就允許使用相機權限，那麼就建置 QR code 掃瞄器
//            configurationScanner()
//            self.addMask()
//        }
//    }
//
//
//    @IBAction func goBackToHome(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    func configurationScanner() {
//        // 取得後置鏡頭來擷取影片
//        //        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
//        let deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
//
//
//        guard let captureDevice = deviceDiscoverySession else {
//            print("Failed to get the camera device")
//            return
//        }
//        //        guard let captureDevice = deviceDiscoverySession.devices.first else {
//        //            print("無法獲取相機裝置")
//        //            return
//        //        }
//        do {
//            // 使用前一個裝置物件 captureDevice 來取得 AVCaptureDeviceInput 類別的實例
//            let input = try AVCaptureDeviceInput(device: captureDevice)
//            // 實例化 AVCaptureSession，設定 captureSession 的輸入裝置
//            captureSession = AVCaptureSession()
//            captureSession?.addInput(input)
//            // 實例化 AVCaptureMetadataOutput 物件並將其設定做為 captureSession 的輸出
//            let captureMetadataOutput = AVCaptureMetadataOutput()
//            captureSession?.addOutput(captureMetadataOutput)
//            // 設置 delegate 為 self，並使用預設的調度佇列來執行 Call back
//            // 當一個新的元資料被擷取時，便會將其轉交給委派物件做進一步處理
//            // 依照 Apple 的文件，我們這邊使用 DispatchQueue.main 來取得預設的主佇列
//            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
//            // 告訴 App 我們所想要處理 metadata 的對象對象類型
//            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
//            // 用於顯示我們的相機畫面
//            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
//            // 設置影片在 videoPreivewLayer 的顯示方式
//            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
//            videoPreviewLayer?.frame = view.layer.bounds
//            view.layer.addSublayer(videoPreviewLayer!)
//            // 設置 QR Code 掃描框
//            settingScannerFrame()
//            // 開始擷取畫面
//            captureSession!.startRunning()
//        } catch {
//            // 假如有錯誤發生、印出錯誤描述並且 return
//            print(error.localizedDescription)
//            return
//        }
//    }
//
//    func addMask() {
//        let screenWidth = UIScreen.main.bounds.size.width
//        let screenHeight = UIScreen.main.bounds.size.height
//
//        qRcodeView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
//        qRcodeView.backgroundColor = UIColor(white: 0, alpha: 0.74)
//
//        //create path
//        let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
//
//        // MARK: roundRectanglePath
//        path.append(UIBezierPath(roundedRect: CGRect(x: screenWidth/2-screenWidth*0.3, y: screenHeight/2 - screenWidth*0.3, width: screenWidth*0.6, height: screenWidth*0.6), cornerRadius: 5).reversing())
//
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.path = path.cgPath
//
//        qRcodeView.layer.mask = shapeLayer
//    }
//
//    func settingScannerFrame() {
//        qrCodeFrameView = UIView()
//        if let qrCodeFrameView = qrCodeFrameView {
//            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
//            qrCodeFrameView.layer.borderWidth = 2
////            view.addSubview(qrCodeFrameView)
//            view.addSubview(qRcodeView)
//            // 將 qrCodeFrameView 排至畫面最前方
////            view.bringSubviewToFront(qrCodeFrameView)
//            view.bringSubviewToFront(qRcodeView)
//        }
//    }
//
//
//    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//        // 如果 metadataObjects 是空陣列
//        // 那麼將我們搜尋框的 frame 設為 zero，並且 return
//        if metadataObjects.isEmpty {
//            qrCodeFrameView?.frame = CGRect.zero
//            return
//        }
//        // 如果能夠取得 metadataObjects 並且能夠轉換成 AVMetadataMachineReadableCodeObject（條碼訊息）
//        if let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
//            // 判斷 metadataObj 的類型是否為 QR Code
//            if metadataObj.type == AVMetadataObject.ObjectType.qr {
//                //  如果 metadata 與 QR code metadata 相同，則更新搜尋框的 frame
//                let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
//                //                qrCodeFrameView?.frame = barCodeObject!.bounds
//                if let value = metadataObj.stringValue {
//                    print(value)
//                    let myClinic = value.components(separatedBy: "#")
//                    if myClinic[1] == "clinicID" {
//                        if let controllers = navigationController?.viewControllers {
//                            var newStack = [UIViewController]()
//                            if controllers.count > 0 {
//                                let rootVC = controllers[0]
//                                newStack.append(rootVC)
//                            }
//                            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//                            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicDetailPage) as? ClinicDetailViewController {
//                                controller.clinicID = myClinic[2]
//                                controller.isQRcode = true
//                                newStack.append(controller)
//                            }
//                            navigationController?.setViewControllers(newStack, animated: true)
//                            captureSession!.stopRunning()
//                        }
//                    } else {
//                        let controller = UIAlertController(title: "QR碼無效", message: nil, preferredStyle: .alert)
//                        let okAction = UIAlertAction(title: "確定", style: .default, handler: nil)
//                        controller.addAction(okAction)
//                        present(controller, animated: true, completion: nil)
//                    }
//                }
//            }
//        }
//    }
//
//
//}
