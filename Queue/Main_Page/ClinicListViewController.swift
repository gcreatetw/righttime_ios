//
//  HospitalViewController.swift
//  Queue
//
//  Created by Andrew on 2019/2/22.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage
import CoreLocation

class ClinicListViewController: UIViewController {
    
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var hospitalsTableView: UITableView!
    @IBOutlet weak var nullClinicView: UIView!
    
    var clinicLists: [JSON]?
    var cityID: String?
    var typeID: String?
    let locationManager = CLLocationManager()
    var curLat = Double()
    var curLon = Double()
    var refreshControl: UIRefreshControl!
    
    @IBAction func unwindToClinicListPage(segue: UIStoryboardSegue) {
        print("unwindToClinicListPage...")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(ClinicListViewController.refreshClinicListPage), for: UIControl.Event.valueChanged)
        self.hospitalsTableView?.addSubview(self.refreshControl)
        
        changeConditions()
        let notiName = Notification.Name("changeCondition")
        NotificationCenter.default.addObserver(self, selector: #selector(ClinicListViewController.changeConditions), name: notiName, object: nil)
        
    }
    
    @objc func refreshClinicListPage() {
        changeConditions()
        self.refreshControl.endRefreshing()
    }
    
    @objc func changeConditions() {
        cityID = UserDefaults.standard.string(forKey: UserDefaultsID.cityID) ?? ""
        typeID = UserDefaults.standard.string(forKey: UserDefaultsID.typeID) ?? ""
        //        print("cityID:",cityID,"typeID:",typeID,"UserDefaults.standard",UserDefaults.standard.bool(forKey: "isCenter"))
        //        print("self.curLat",self.curLat,"self.curLon",self.curLon)
        /// - Remark: 是否搜尋週邊診所 10 km
        RestAPI.shared.getClinicList(city_id: cityID!, type_id: typeID!, center: UserDefaults.standard.bool(forKey: UserDefaultsID.isCenter), curLat: self.curLat, curLon: self.curLon){(response)->Void in
            if (response != JSON.null) {
                let json = JSON(response)
                
                var tmpClinicLists: [JSON]?
                var openList = [JSON]()
                var closeList = [JSON]()
                tmpClinicLists = json.array
                
                for i in 0 ..< tmpClinicLists!.count {
                    let myLocation = CLLocation(latitude: self.curLat, longitude: self.curLon)
                    let storeLat = tmpClinicLists![i]["lat"].double!
                    let storeLon = tmpClinicLists![i]["lng"].double!
                    let storeLocation = CLLocation(latitude: storeLat, longitude: storeLon)
                    let distance = String(format: "%.1f", myLocation.distance(from: storeLocation) / 1000)
                    tmpClinicLists![i]["distance"].stringValue = distance
                }
                
                tmpClinicLists!.sort { $0["distance"].doubleValue < $1["distance"].doubleValue }
                
                for i in tmpClinicLists! {
                    if i["open"].rawString() == "true" {
                        openList.append(i)
                    } else {
                        closeList.append(i)
                    }
                }
                
                self.clinicLists = openList + closeList  //排序
//                self.clinicLists = tmpClinicLists
                
//                                self.clinicLists = json.array
//                                /// - Returns: self.clinicLists 尚未排序
//                                for i in 0..<self.clinicLists!.count {
//                                    let myLocation = CLLocation(latitude: self.curLat, longitude: self.curLon)
//                                    let storeLat = self.clinicLists![i]["lat"].double!
//                                    let storeLon = self.clinicLists![i]["lng"].double!
//                                    let storeLocation = CLLocation(latitude: storeLat, longitude: storeLon)
//                                    let distance = String(format: "%.1f", myLocation.distance(from: storeLocation) / 1000)
//                                    self.clinicLists![i]["distance"].stringValue = distance
//                                }
//                                /// - Returns: self.clinicLists 加入距離排序
//                                self.clinicLists!.sort { $0["distance"].doubleValue < $1["distance"].doubleValue }
                
                guard self.clinicLists?.count ?? 0 > 0 else {
                    if CLLocationManager.authorizationStatus() != .authorizedWhenInUse && UserDefaults.standard.bool(forKey: UserDefaultsID.isCenter) == true {
                        self.showLocationAlert()
                    }
                    self.hospitalsTableView.backgroundColor = UIColor.clear
                    self.hospitalsTableView.reloadData()
                    return
                }
                if CLLocationManager.authorizationStatus() != .authorizedWhenInUse && UserDefaults.standard.bool(forKey: UserDefaultsID.isCenter) == true {
                    self.clinicLists = []
                    self.hospitalsTableView.backgroundColor = UIColor.clear
                    self.hospitalsTableView.reloadData()
                    self.showLocationAlert()
                } else {
                    self.hospitalsTableView.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
                    self.hospitalsTableView.reloadData()
                    let topIndex = IndexPath(row: 0, section: 0)
                    self.hospitalsTableView.scrollToRow(at: topIndex, at: .top, animated: false)
                }
            } else {
                self.clinicLists = []
                self.hospitalsTableView.reloadData()
                self.hospitalsTableView.backgroundColor = UIColor.clear
                self.showAlert(content: "網路連線不穩，請確認網路狀態！")
                print("clinicList Error:",response)
            }
        }
        /// - Precondition: 判斷兩個條件的 list 是否為空，如果為空則重新與server要求一次
        if cityLists == nil || typeLists == nil {
            RestAPI.shared.getCityList { (response)->Void in
                if (response != JSON.null) {
                    cityLists = response.array!
                    cityLists?.insert(["name": "周邊熱搜","city_id": ""], at: 0)
                    for i in 0..<cityLists!.count {
                        if UserDefaults.standard.string(forKey: UserDefaultsID.cityID) == cityLists![i]["city_id"].rawString() {
                            self.cityButton.setTitle(cityLists![i]["name"].rawString()! + " ▼", for: .normal)
                        }
                    }
                } else {
                    /// - Todo: 需要防呆機制提醒使用者server掛掉
                    self.showAlert(content: "網路連線不穩，請確認網路狀態！")
                }
            }
            RestAPI.shared.getTypeList { (response)->Void in
                if (response != JSON.null) {
                    typeLists = response.array!
                    typeLists?.insert(["type_id": "","name": "全部科別"], at: 0)
                    for i in 0..<typeLists!.count {
                        if self.typeID == typeLists![i]["type_id"].rawString() {
                            self.typeButton.setTitle(typeLists![i]["name"].rawString()! + " ▼", for: .normal)
                        }
                    }
                } else {
                    self.showAlert(content: "網路連線不穩，請確認網路狀態！")
                }
            }
        } else {
            for i in 0..<cityLists!.count {
                if UserDefaults.standard.string(forKey: UserDefaultsID.cityID) == cityLists![i]["city_id"].rawString() {
                    self.cityButton.setTitle(cityLists![i]["name"].rawString()! + " ▼", for: .normal)
                }
            }
            for i in 0..<typeLists!.count {
                if self.typeID == typeLists![i]["type_id"].rawString() {
                    self.typeButton.setTitle(typeLists![i]["name"].rawString()! + " ▼", for: .normal)
                }
            }
        }
    }
    
    @IBAction func goToClinicPickerPage(_ sender: UIButton) {
        switch sender {
        case cityButton:
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicPickerPage) as?
                ClinicPickerViewController {
                controller.isCityListPicker = true
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                controller.titleLabel.text = "請選擇地區"
                present(controller, animated: true, completion: nil)
            }
        case typeButton:
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicPickerPage) as?
                ClinicPickerViewController {
                controller.isCityListPicker = false
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                controller.titleLabel.text = "請選擇科別"
                present(controller, animated: true, completion: nil)
            }
        default:
            break
        }
    }
}

extension ClinicListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.clinicLists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.clinicsListCell, for: indexPath) as! ClinicsListTbCell
        let content = self.clinicLists?[indexPath.row]
        cell.clinicListName.text = content?["name"].rawString()
        cell.clinicListAddress.text = content?["address"].rawString()
        cell.clinicListPhone.text = content?["contact"].rawString()
        cell.clinicListImageView.sd_setImage(with: URL(string: (content?["pic"].rawString())!), placeholderImage: UIImage(named: "page-icon"))
        
        if content?["open"].rawString() == "true" {
            cell.maskImgView.isHidden = true
        } else {
            cell.maskImgView.isHidden = false
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            cell.distance.text = "距離" + (content?["distance"].rawString())! + "km"
        } else {
            cell.distance.isHidden = true
        }
        
        if let clinicListPhoneExtension = content?["extension"].rawString() {
            guard clinicListPhoneExtension == "null" else {
                cell.clinicListPhoneExtension.text = clinicListPhoneExtension
                return cell
            }
            cell.clinicListPhoneExtension.text = ""
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicDetailPage) as? ClinicDetailViewController {
            controller.clinicID = self.clinicLists?[indexPath.row]["id"].rawString()
            controller.clininImgURL = self.clinicLists?[indexPath.row]["pic"].rawString()
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension ClinicListViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let curLocation:CLLocationCoordinate2D = manager.location!.coordinate
        curLat = curLocation.latitude
        curLon = curLocation.longitude
    }
    
    func showAlert(content: String) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.alertPage) as?
            ShowAlertViewController {
            controller.content = content
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func showLocationAlert() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.locationAlertPage) as?
            ShowLocationAlertViewController {
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
}

