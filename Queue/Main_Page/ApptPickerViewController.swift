//
//  ApptPickerViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/22.
//

import UIKit

class ApptPickerViewController: UIViewController {
    //假資料
    var judgmentlist:Int = 0
    var doctorList = ["林OO","黃OO","王OO"]
    var timeList = ["09:00","09:30","10:00","10:30","12:00","14:00"]
    var symptomList = ["頭痛","腹瀉","咳嗽","鼻塞"]
    var doctorName = String()
    var timeperiod = String()
    var symptom = String()
    @IBOutlet weak var apptPickerView: UIPickerView!
    @IBOutlet weak var pickerTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if judgmentlist == 0{
            pickerTitle.text = "請選擇門診醫生"
            selectedDoctorRow()
        }
        else if judgmentlist == 1{
            pickerTitle.text = "請選擇門診時段"
            selectedTimeRow()
        }
        else if judgmentlist == 2{
            pickerTitle.text = "請選擇主要症狀"
            selectedsyncRow()
        }
    }
    
    @IBAction func goBackAppt(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
extension ApptPickerViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if judgmentlist == 1{
            return timeList.count
        }
        else if judgmentlist == 2{
            return symptomList.count
        }
        return doctorList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if judgmentlist == 1{
            return timeList[row]
        }
        else if judgmentlist == 2{
            return symptomList[row]
        }
        return doctorList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if judgmentlist == 0{
            doctorName = doctorList[row]
        }
        else if judgmentlist == 1{
            timeperiod = timeList[row]
        }
        else{
            symptom = symptomList[row]
        }
    }
}
extension ApptPickerViewController {
    func selectedDoctorRow() {
        for i in 0 ..< doctorList.count {
            if UserDefaults.standard.string(forKey: "doctor_name") == "     \(doctorList[i])" {
                doctorName = doctorList[i]
                self.apptPickerView.selectRow(i, inComponent: 0, animated: true)
                self.apptPickerView.reloadAllComponents()
                return
            }
        }
    }
    func selectedTimeRow() {
        for i in 0 ..< timeList.count {
            if UserDefaults.standard.string(forKey: "time_period") == "     \(timeList[i])" {
                timeperiod = timeList[i]
                self.apptPickerView.selectRow(i, inComponent: 0, animated: true)
                self.apptPickerView.reloadAllComponents()
                return
            }
        }
    }
    func selectedsyncRow() {
        for i in 0 ..< timeList.count {
            if UserDefaults.standard.string(forKey: "sync_name") == "     \(symptomList[i])" {
                symptom = symptomList[i]
                self.apptPickerView.selectRow(i, inComponent: 0, animated: true)
                self.apptPickerView.reloadAllComponents()
                return
            }
        }
    }
}
