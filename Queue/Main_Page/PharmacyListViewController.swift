//
//  PharmacyListViewController.swift
//  Queue
//
//  Created by Andrew on 2020/2/6.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
import MapKit


class PharmacyListViewController: UIViewController {

    @IBOutlet weak var alarmLabel: UILabel!{
        didSet {
            
        }
    }
    @IBOutlet weak var pharmacyListTableView: UITableView!
    @IBOutlet weak var cityButton: ClassButtonSizeClass!
    @IBOutlet weak var districtButton: ClassButtonSizeClass!
    var refreshControl: UIRefreshControl!
    var pharmacyLists: [JSON]?
    let locationManager = CLLocationManager()
    var curLat = Double()
    var curLon = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //創建NSMutableAttributedString實體，並填入要顯示的字串
        let attributedString = NSMutableAttributedString(string: "此資訊為健保署所提供口罩數量相關資料。\n開放購買時間及方式，視各家健保特約藥局作業而定。\n實際庫存依照藥局數量為主，線上查詢之數量僅供參考。")
        
        //從第1個字開始，5個字都設置為藍色
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSMakeRange(65, 4))

        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()

        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 6 // Whatever line spacing you want in points

        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        // *** Set Attributed String to your label ***
        alarmLabel.attributedText = attributedString

        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(PharmacyListViewController.refreshClinicListPage), for: UIControl.Event.valueChanged)
        self.pharmacyListTableView?.addSubview(self.refreshControl)
        
        changeConditions()
        
        let notiName = Notification.Name("changeConditions")
        NotificationCenter.default.addObserver(self, selector: #selector(ClinicListViewController.changeConditions), name: notiName, object: nil)
    }
    
    @objc func refreshClinicListPage() {
        changeConditions()
        self.refreshControl.endRefreshing()
    }
    
    @objc func changeConditions() {
        var item = UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyCity)!
        let startIndex = item.index(item.startIndex, offsetBy: 3)
        let endIndex = item.index(item.startIndex, offsetBy: 4)
        item.removeSubrange(startIndex...endIndex)
        self.cityButton.setTitle(item + " ▼", for: .normal)
        self.districtButton.setTitle(UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyDistrict)! + " ▼", for: .normal)
        
        RestAPI.shared.getPharmacyList(city_name: item, district_name: UserDefaults.standard.string(forKey: UserDefaultsID.pharmacyDistrict)!) {(response)->Void in
            if (response != JSON.null) {
                var tmpPharmacyLists = response.array!
               
                for i in 0 ..< tmpPharmacyLists.count {
                    let pharmacyLat = tmpPharmacyLists[i]["lat"].double
                    let pharmacyLon = tmpPharmacyLists[i]["lng"].double
                    let pharmacyLocation = CLLocation(latitude: pharmacyLat!, longitude: pharmacyLon!)
                    let myLocation = CLLocation(latitude: self.curLat, longitude: self.curLon)
                    let distance = String(format: "%.1f", myLocation.distance(from: pharmacyLocation) / 1000)
                    tmpPharmacyLists[i]["distance"].stringValue = distance
                    
//                    self.coordinates(forAddress: (self.pharmacyLists![i]["address"].rawString())!) {(location) in
//                        guard let location = location else { return }
//                        let pharmacyLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
//                        let myLocation = CLLocation(latitude: self.curLat, longitude: self.curLon)
//                        let distance = String(format: "%.1f", myLocation.distance(from: pharmacyLocation) / 1000)
//                        self.pharmacyLists![i]["distance"].stringValue = distance
//                    }
                }
                tmpPharmacyLists.sort { $0["distance"].doubleValue < $1["distance"].doubleValue }
                
                var inventoryArray = [JSON]()
                var saleoutArray = [JSON]()
                var noDataArray = [JSON]()
                
                for pharmacyItem in tmpPharmacyLists {
                    if pharmacyItem["mask_adult"].int > 0 || pharmacyItem["mask_child"].int > 0 {
                        inventoryArray.append(pharmacyItem)
                    } else if pharmacyItem["mask_adult"].int == 0 && pharmacyItem["mask_child"].int == 0 {
                        saleoutArray.append(pharmacyItem)
                    } else {
                        noDataArray.append(pharmacyItem)
                    }
                }
                self.pharmacyLists = inventoryArray + saleoutArray + noDataArray
                self.pharmacyListTableView.reloadData()

                
            } else {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.alertPage) as?
                    ShowAlertViewController {
                    controller.content = "網路連線不穩，請確認網路狀態！"
                    controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                    controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                    controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func goToClinicPickerPage(_ sender: UIButton) {
        switch sender {
        case cityButton:
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.pharmacyPickerPage) as?
                PharmacyPickerViewController {
                controller.isCitiesPicker = true
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                controller.titleLabel.text = "請選擇縣市"
                present(controller, animated: true, completion: nil)
            }
        case districtButton:
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.pharmacyPickerPage) as?
                PharmacyPickerViewController {
                controller.isCitiesPicker = false
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                controller.titleLabel.text = "請選擇地區"
                present(controller, animated: true, completion: nil)
            }
        default:
            break
        }
    }
}
extension PharmacyListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.pharmacyLists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.pharmacyListCell, for: indexPath) as! PharmacyListTbCell
        let content = self.pharmacyLists?[indexPath.row]
//        cell.pharmacyAddress.text = "地址: " + (content?["address"].rawString())!
        cell.pharmacyAddress.text = content?["address"].rawString()
        //cell.pharmacyAddress.text!.transformingHalfwidthFullwidth(from: CharacterSet.alphanumerics.inverted)
        cell.moreInfoButton.imageView?.contentMode = .scaleAspectFit
        cell.pharmacyName.text = content?["name"].rawString()
        cell.pharmacyPhone.text = "電話: " + (content?["phone"].rawString())!
    
        //cell.updateTimeLabel.text = "來源資料更新 " + ((content?["mask_update_time"].rawString())!.prefix(16))
        //cell.updateTimeLabel.text!.trimmingCharacters(in: CharacterSet(charactersIn: "T"))
        var tmpInt = 0
        var tmpString = ""
        for i in (content?["mask_update_time"].rawString())!.prefix(16) {
            if tmpInt != 10 {
                tmpString = tmpString + String(i)
                print(tmpString)
            } else {
                tmpString = tmpString + " "
            }
            tmpInt += 1
            print(tmpInt)
        }
        
        cell.updateTimeLabel.text = "來源資料更新 " + tmpString

        
        if content?["mask_adult"].int < 0 && content?["mask_child"].int < 0 {
            cell.adultMaskLabel.isHidden = true
            cell.childMaskLabel.isHidden = true
        } else {
            cell.adultMaskLabel.text =  (content?["mask_adult"].rawString())!
            cell.childMaskLabel.text = (content?["mask_child"].rawString())!
        }
    
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            cell.pharmacyDistance.text = "距離" + (content?["distance"].rawString())! + " km"
        } else {
            cell.pharmacyDistance.isHidden = true
        }
        
        cell.tapAction = {(cell) in
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.moreInfoPage) as?
                ShowMoreInfoViewController {
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                controller.pharmacyInfo = content
                controller.pharmacyName.text = content?["name"].rawString()
                controller.pharmacyNote.text = content?["note"].rawString()
                self.present(controller, animated: true, completion: nil)
            }
        }
        
//        cell.tapAction = {(cell) in
//            guard let number = URL(string: "tel://" + (content?["phone"].rawString())!) else { return }
//            UIApplication.shared.open(number)
//        }
//
//        cell.tapAction2 = {(cell) in
//            self.coordinates(forAddress: (content?["address"].rawString())!) {(location) in
//                guard let location = location else { return }
//                self.openMapForPlace(lat: location.latitude, long: location.longitude)
//            }
//        }
        return cell
    }

    func coordinates(forAddress address: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            (placemarks, error) in
            guard error == nil else {
                print("Geocoding error: \(error!)")
                completion(nil)
                return
            }
            completion(placemarks?.first?.location?.coordinate)
        }
    }
    
    func openMapForPlace(lat:Double = 0, long:Double = 0, placeName:String = "") {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long

        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
}

extension PharmacyListViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let curLocation:CLLocationCoordinate2D = manager.location!.coordinate
        curLat = curLocation.latitude
        curLon = curLocation.longitude
    }
}

extension String {
    func transformingHalfwidthFullwidth(from aSet: CharacterSet) -> String {
        return String(characters.map {
            if String($0).rangeOfCharacter(from: aSet) != nil {
                let string = NSMutableString(string: String($0))
                CFStringTransform(string, nil, kCFStringTransformFullwidthHalfwidth, true)
                return String(string).characters.first!
            } else {
                return $0
            }
        })
    }
}
