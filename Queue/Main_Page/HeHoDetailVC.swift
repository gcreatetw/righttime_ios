//
//  HeHoDetailVC.swift
//  Queue
//
//  Created by Andrew on 2020/9/13.
//

import UIKit
import WebKit
import PKHUD

class HeHoDetailVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.navigationController?.navigationBar.tintColor = UIColor.white
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        guard let url = URL(string: self.url ?? "") else { return }
        let request = URLRequest(url: url)
        self.webView?.load(request)
        
    }

}

extension HeHoDetailVC: WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        HUD.show(.progress)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUD.hide()
    }
}
