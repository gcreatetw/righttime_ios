//
//  ChooseAlertViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/16.
//

import UIKit

class ChooseAlertViewController: UIViewController {
    
    @IBOutlet weak var contentLabel: UILabel!
    var content: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        contentLabel.text = content
    }
    
    @IBAction func doneClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
