//
//  LightBoxViewController.swift
//  Queue
//
//  Created by Andrew on 2019/3/5.
//

import UIKit
import CoreData
import SwiftyJSON
import Alamofire
import BetterSegmentedControl

class ApptSettingsViewController: UIViewController {
    
    @IBOutlet weak var circleContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.borderWidth = 2
            containerView.layer.borderColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1).cgColor
        }
    }
    
    @IBOutlet weak var circleContainerView: UIView!
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    @IBOutlet weak var scheduleButton: UIButton! {
        didSet {
            scheduleButton.layer.cornerRadius = 3
            scheduleButton.setTitle(self.selectedDate, for: .normal)
        }
    }
    
    @IBOutlet weak var apptNumTextField: UITextField! {
        didSet {
            apptNumTextField.layer.cornerRadius = 3
            //            myNumTextField.becomeFirstResponder()
        }
    }
    
    @IBOutlet weak var notifyNumTextField: UITextField! {
        didSet {
            notifyNumTextField.layer.cornerRadius = 3
            notifyNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
        }
    }
    
    @IBOutlet weak var roomCurNumLabel: UILabel! {
        didSet {
            roomCurNumLabel.text = roomCurNum
        }
    }
    
    @IBOutlet weak var alertLabel: UILabel!    
    @IBOutlet weak var segmentContainerView: UIView!
    
    var clinicID: String?
    var clinicName: String?
    var clinicPicAddress: String?
    var roomID: Int = 0
    var roomName: String?
    var roomCurNum: String?
    var notifyNumBegins: Int = 0
    
    var restApi = RestAPI()
    var dayPartsString: String?
    var dayPartsInt: Int?
    var curDayPartsString: String?
    var curDayPartsInt: Int?
    var selectedDate = DateUtil.shared.getStrSysDate(withFormat: "YYYY-MM-dd")
    var getRoomIDs:[JSON]?
    var isComeFromHome: Bool?
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        circleContainerConstraint.constant = -circleContainerView.frame.size.width * 0.36
        let aDegree = CGFloat.pi / 180
        let semiCircleLayer = CAShapeLayer()
        let center = CGPoint (x: circleContainerView.frame.size.width / 2, y: circleContainerView.frame.size.height / 2)
        let circleRadius = circleContainerView.frame.size.width / 2
        let circlePath = UIBezierPath(arcCenter: center, radius: circleRadius, startAngle: aDegree * 345, endAngle: aDegree * 195, clockwise: false)
        semiCircleLayer.path = circlePath.cgPath
        semiCircleLayer.strokeColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1).cgColor
        semiCircleLayer.fillColor = UIColor.clear.cgColor
        semiCircleLayer.lineWidth = 2
        semiCircleLayer.strokeStart = 0
        semiCircleLayer.strokeEnd  = 1
        circleContainerView.layer.addSublayer(semiCircleLayer)
        circleContainerView.layer.cornerRadius = circleContainerView.frame.size.width * 0.5
        
        var w: CGFloat = 198
        var h: CGFloat = 26.5
        
        switch UIDevice().type {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C:
            w = 198
            h = 26.5
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        case .iPhone6plus, .iPhone6Splus, .iPhone7plus, .iPhone8plus:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        case .iPhoneXR,.iPhoneXSMax:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        case .iPhoneXS,.iPhoneX:
            w = 219
            h = segmentContainerView.frame.size.height
        default:
            w = segmentContainerView.frame.size.width
            h = segmentContainerView.frame.size.height
        }
        
        let SegmentedControl = BetterSegmentedControl (
            frame: CGRect(x: 0, y: 0, width: w, height: h),
            segments: LabelSegment.segments(withTitles: ["上午診", "下午診", "夜間診"],
                                            normalFont: UIFont.systemFont(ofSize: 12),
                                            normalTextColor: UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1),
                                            selectedFont: UIFont.systemFont(ofSize: 12),
                                            selectedTextColor: UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)),
            index: UInt(dayPartsInt!),
            options: [.backgroundColor(UIColor(red:255/255, green:199/255, blue:5/255, alpha:1.00)),
                      .indicatorViewBackgroundColor(UIColor(red:1, green:1, blue:1, alpha:1.00)),
                      .cornerRadius(h/2),
                      .bouncesOnChange(false),
                      .panningDisabled(true)])
        SegmentedControl.indicatorViewInset = 3
        SegmentedControl.addTarget(self, action: #selector(ApptSettingsViewController.SegmentedControlValueChanged(_:)), for: .valueChanged)
        segmentContainerView.addSubview(SegmentedControl)
    }
    
    @objc func SegmentedControlValueChanged(_ sender: BetterSegmentedControl) {
        switch sender.index {
        case 0:
            let oldselectTimeInt = dayPartsInt
            dayPartsString = "M"
            dayPartsInt = 0
            apptNumTextField.text = ""
            notifyNumTextField.text = ""
            alertLabel.isHidden = true
            if dayPartsInt ?? 0 < oldselectTimeInt ?? 0 && dayPartsInt ?? 0 < curDayPartsInt ?? 0 {
                dayPartsInt = oldselectTimeInt
            }
        case 1:
            let oldselectTimeInt = dayPartsInt
            dayPartsString = "A"
            dayPartsInt = 1
            apptNumTextField.text = ""
            notifyNumTextField.text = ""
            alertLabel.isHidden = true
            if dayPartsInt ?? 0 < oldselectTimeInt ?? 0 && dayPartsInt ?? 0 < curDayPartsInt ?? 0 {
                dayPartsInt = oldselectTimeInt
            }
        case 2:
            let oldselectTimeInt = dayPartsInt
            dayPartsString = "E"
            dayPartsInt = 2
            apptNumTextField.text = ""
            notifyNumTextField.text = ""
            alertLabel.isHidden = true
            if dayPartsInt ?? 0 < oldselectTimeInt ?? 0 &&  dayPartsInt ?? 0 < curDayPartsInt ?? 0 {
                dayPartsInt = oldselectTimeInt
            }
        default:
            print("default")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch curDayPartsString {
        case "M":
            dayPartsInt = 0
            dayPartsString = "M"
            curDayPartsInt = 0
        case "A":
            dayPartsInt = 1
            dayPartsString = "A"
            curDayPartsInt = 1
        case "E":
            dayPartsInt = 2
            dayPartsString = "E"
            curDayPartsInt = 2
        default:
            print("default")
        }
    }
    
    @IBAction func scheduleButtonClicked(_ sender: Any) {
        let today = Date()
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        let dateFrom = calendar.startOfDay(for: today)
        let dateTo = calendar.date(byAdding: .day, value: 60, to: dateFrom)
        var tmpSelectDate: String?
        
        let alert = UIAlertController(title: "請選擇您預約的日期", message: "兩個月內的時段", preferredStyle: .actionSheet)
        alert.addDatePicker(mode: .date, date: dateFrom, minimumDate: dateFrom, maximumDate: dateTo) { date in
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
            dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
            tmpSelectDate = dateFormatter.string(from: date)
        }
        alert.addAction(UIAlertAction(title: "完成", style: .cancel, handler: { (UIAlertAction) in
            if let tmpSelectDate = tmpSelectDate {
                self.selectedDate = tmpSelectDate
            } else {
                self.selectedDate = DateUtil.shared.getStrSysDate(withFormat: "YYYY-MM-dd")
            }
            
            if DateUtil.shared.getStrSysDate(withFormat: "YYYY-MM-dd") != self.selectedDate {
                self.dayPartsInt = 0
                self.dayPartsString = "M"
                self.curDayPartsInt = 0
                self.roomCurNumLabel.text = "回診提醒"
            } else {
                /// - Todo: 如讓預約的cell選擇回當日時，秀此間診所當時的號碼，需要向server索取當前號碼。
                RestAPI.shared.getTrackingInfo(roomNumArray: [Int16(self.roomID)], completion: { (response) -> Void in
                    if (response != JSON.null) {
                        let json = JSON(response)
                        self.getRoomIDs = json.array
                        self.roomCurNumLabel.text = self.getRoomIDs![0]["currentNum"].rawString()
                    }
                })
                //                self.roomCurNumLabel.text = self.roomCurNum
                switch self.curDayPartsString {
                case "M":
                    self.dayPartsInt = 0
                    self.dayPartsString = "M"
                    self.curDayPartsInt = 0
                case "A":
                    self.dayPartsInt = 1
                    self.dayPartsString = "A"
                    self.curDayPartsInt = 1
                case "E":
                    self.dayPartsInt = 2
                    self.dayPartsString = "E"
                    self.curDayPartsInt = 2
                default:
                    print("default")
                }
            }
            self.scheduleButton.setTitle(self.selectedDate, for: .normal)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func goBackVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backToClinicPage(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.isComeFromHome ?? false {
                let notiName = Notification.Name("pushToClinicDetailPage")
                NotificationCenter.default.post(name: notiName, object: nil)
            }
        }
    }
    
    func switchTxtFieldColor(withTag tag: Int, withIsInput isInput: Bool) {
        if apptNumTextField.tag == tag {
            apptNumTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            apptNumTextField.layer.backgroundColor = UIColor.white.cgColor
            apptNumTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        } else if notifyNumTextField.tag == tag {
            notifyNumTextField.layer.borderColor = isInput ? UIColor.black.cgColor : UIColor.lightGray.cgColor
            notifyNumTextField.layer.backgroundColor = UIColor.white.cgColor
            notifyNumTextField.layer.borderWidth = isInput ? 1.5 : 0.8
        }
    }
    
    @IBAction func goToHomePage(_ sender: Any) {
        guard !apptNumTextField.text!.isEmpty else {
            alertLabel.text = "請設定您的號碼"
            alertLabel.isHidden = false
            return
        }
        
        guard !notifyNumTextField.text!.isEmpty else {
            alertLabel.text = "請設定提前幾號提醒"
            alertLabel.isHidden = false
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request:NSFetchRequest<Appointment> = Appointment.fetchRequest()
        var readApptList: [Appointment]?
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        calendar.locale = Locale(identifier: "zh_Hant_TW")
        let dateFrom = calendar.startOfDay(for: Date())
        let dateTo = calendar.date(byAdding: .day, value: 60, to: dateFrom)
        
        request.predicate = NSPredicate(format: "roomID == %@ && apptDate BETWEEN {%@, %@}", argumentArray:[self.roomID, dateFrom, dateTo])
        do {
            readApptList = try context.fetch(request)
        } catch {print("error")}
        
        notifyNumBegins = Int(apptNumTextField.text!)! - Int(notifyNumTextField.text!)!
        var tempNotifyNumBeginsArray = [String]()
        for i in readApptList! {
            tempNotifyNumBeginsArray.append(DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: i.apptDate!))
        }
        
        if tempNotifyNumBeginsArray.contains(self.selectedDate) {
            for i in readApptList! {
                if DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: i.apptDate!) == self.selectedDate {
                    i.apptNum = apptNumTextField.text
                    i.apptDayParts = dayPartsString
                    i.apptDate = DateUtil.shared.getDateFromString(withFormat: "YYYY-MM-dd", strDate: self.selectedDate)
                    appDelegate.saveContext()
                    restApi.postTrackingInfo(roomID: String(self.roomID), deviceToken: firebaseToken, targetNum: apptNumTextField.text!, notifyNum: String(notifyNumBegins), period: self.dayPartsString!, date: self.selectedDate) {(response)->Void in
                        if (response != JSON.null) {
                            print("postTrackingInfo:",response)
                        }
                    }
                }
            }
        } else {
            let appt = Appointment(context: context)
            appt.clinicID = self.clinicID
            appt.clinicName = self.clinicName
            appt.clinicPicAddress = self.clinicPicAddress
            appt.roomID = Int16(self.roomID)
            appt.roomName = self.roomName
            appt.curNum = roomCurNumLabel.text
            appt.apptNum = apptNumTextField.text
            appt.apptDayParts = dayPartsString
            appt.apptDate = DateUtil.shared.getDateFromString(withFormat: "YYYY-MM-dd", strDate: self.selectedDate)
            appDelegate.saveContext()
            restApi.postTrackingInfo(roomID: String(self.roomID), deviceToken: firebaseToken, targetNum: apptNumTextField.text!, notifyNum: String(notifyNumBegins), period: self.dayPartsString!, date: self.selectedDate) {(response)->Void in
                if (response != JSON.null) {
                    print("apptInfo_apptSettingPage:",response)
                }
            }
        }
        
        let notiName = Notification.Name("refreshHomePage")
        NotificationCenter.default.post(name: notiName, object: nil)
        self.performSegue(withIdentifier: SegueID.backToHomePage, sender: nil)
    }
}

extension ApptSettingsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //        var apptNumMinusCurNum: Int?
        //        if let tmp = Int(apptNumTextField.text ?? "0"), let tmp2 = Int(roomCurNum ?? "0") {
        //             apptNumMinusCurNum = tmp - tmp2
        //        }
        
        switch textField {
        case notifyNumTextField:
            if let text = textField.text {
                let newStr = (text as NSString).replacingCharacters(in: range, with: string)
                if newStr.isEmpty {
                    return true
                }
                let intvalue = Int(newStr)
                let textLength = text.characters.count + string.characters.count - range.length
                /// - Remark: myNumTextField.text!沒輸入就不給輸入，有輸入不限制只要兩位數大於1小於10
                if apptNumTextField.text! == "" || apptNumTextField.text! == "0" || apptNumTextField.text! == "00" || apptNumTextField.text! == "000" {
                    return (textLength == 0)
                } else if Int(apptNumTextField.text!)! < 10 {
                    return (intvalue! >= 1 && intvalue! <= Int(apptNumTextField.text!)! && textLength <= 2)
                } else {
                    return (intvalue! >= 1 && intvalue! <= 10 && textLength <= 2)
                }
            }
            return true
        case apptNumTextField:
            if let text = textField.text, let Range = Range(range, in: text) {
                var newText = "0"
                /// - Remark: 判斷myNum是否為空
                if text.replacingCharacters(in: Range, with: string) == "" {
                    newText = ""
                } else {
                    newText = text.replacingCharacters(in: Range, with: string)
                }
                
                let textLength = textField.text!.characters.count + string.characters.count - range.length
                
                if newText == "0" || newText == "00" || newText == "000" || Int(newText) == 0 {
                    alertLabel.isHidden = false
                    alertLabel.text = "您的號碼不可以等於0"
                    notifyNumTextField.attributedPlaceholder = NSAttributedString.init(string:"☓", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    return textLength <= 3
                    
                } else if newText == "" {
                    alertLabel.isHidden = true
                    notifyNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    return textLength <= 3
                    
                } else if Int(newText)! < 10 {
                    alertLabel.isHidden = true
                    notifyNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~"+String(Int(newText)!), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    return textLength <= 3
                    
                } else {
                    alertLabel.isHidden = true
                    notifyNumTextField.attributedPlaceholder = NSAttributedString.init(string:"1~10", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:18)])
                    return textLength <= 3
                }
            }
            
            let textLength = textField.text!.characters.count + string.characters.count - range.length
            return textLength <= 3
        default:
            print("default")
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.switchTxtFieldColor(withTag: textField.tag, withIsInput: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.switchTxtFieldColor(withTag: textField.tag, withIsInput: false)
    }
}



