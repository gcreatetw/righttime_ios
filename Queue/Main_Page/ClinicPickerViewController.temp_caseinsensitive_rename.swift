//
//  PickerViewController.swift
//  Queue
//
//  Created by Andrew on 2019/3/11.
//

import UIKit
import SwiftyJSON
import Alamofire

class ClinicPickerViewController: UIViewController {
    
    @IBOutlet weak var myPicker: UIPickerView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var isCityPicker: Bool? /// - Remark: 判斷是city or type
    var cityPickerNum: Int = 0
    var typePickerNum: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPickerList()
    }
    
    func getPickerList() {
        if isCityPicker! {
            if cityLists == nil {
                RestAPI.shared.getCityList { (response)->Void in
                    if (response != JSON.null) {
                        cityLists = response.array!
                        cityLists?.insert(["name": "週邊熱搜","city_id": ""], at: 0)
                        self.selectCityRow()
                    }
                }
            } else {
                self.selectCityRow()
            }
        } else {
            if typeLists == nil {
                RestAPI.shared.getCityList { (response)->Void in
                    if (response != JSON.null) {
                        typeLists = response.array!
                        typeLists?.insert(["type_id": "","name": "全部科別"], at: 0)
                        self.selectTypeRow()
                    }
                }
            } else {
                self.selectTypeRow()
            }
        }
    }
    
    @IBAction func goBackHospotalVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        if cityLists == nil || typeLists == nil {
            
        } else {
            if isCityPicker! {
                guard cityPickerNum == 0 else {
                    UserDefaults.standard.set(cityLists![cityPickerNum]["city_id"].rawString(), forKey: UserDefaultsID.cityID)
                    UserDefaults.standard.set(false, forKey: UserDefaultsID.isCenter)
                    let notiName = Notification.Name("changeCondition")
                    NotificationCenter.default.post(name: notiName, object: nil)
                    self.performSegue(withIdentifier: "goToHospitalVC", sender: nil)
                    return
                }
                UserDefaults.standard.set("", forKey: UserDefaultsID.cityID)
                UserDefaults.standard.set(true, forKey: UserDefaultsID.isCenter)
            } else {
                UserDefaults.standard.set(typeLists![typePickerNum]["type_id"].rawString(), forKey: UserDefaultsID.typeID)
            }
        }
        let notiName = Notification.Name("changeCondition")
        NotificationCenter.default.post(name: notiName, object: nil)
        self.performSegue(withIdentifier: "goToHospitalVC", sender: nil)
    }
    
    
}
extension ClinicPickerViewController: UIPickerViewDataSource,UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isCityPicker! {
            return cityLists?.count ?? 1
        } else {
            return typeLists?.count ?? 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isCityPicker! {
            return cityLists?[row]["name"].rawString() ?? "請確認您的網路狀態"
        } else {
            return typeLists?[row]["name"].rawString() ?? "請確認您的網路狀態"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isCityPicker! {
            cityPickerNum = row
        } else {
            typePickerNum = row
        }
    }
}

extension ClinicPickerViewController {
    func selectCityRow() {
        for i in 0..<cityLists!.count {
            if UserDefaults.standard.string(forKey: UserDefaultsID.cityID) == cityLists![i]["city_id"].rawString()! {
                self.cityPickerNum = i
            }
            self.myPicker.selectRow(self.cityPickerNum, inComponent: 0, animated: true)
            self.myPicker.reloadAllComponents()
        }
    }
    func selectTypeRow() {
        for i in 0..<typeLists!.count {
            if UserDefaults.standard.string(forKey: UserDefaultsID.typeID) == typeLists![i]["type_id"].rawString()! {
                self.typePickerNum = i
            }
            self.myPicker.selectRow(self.typePickerNum, inComponent: 0, animated: true)
            self.myPicker.reloadAllComponents()
        }
    }
}


