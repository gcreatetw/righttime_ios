//
//  TimeTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/3/4.
//

import UIKit

class ClinicTimeTbCell: UITableViewCell {
    
    @IBOutlet weak var clinicTimeTableImg: UIImageView!
    @IBOutlet weak var clinicTimeTitleLabel: UILabel!
    @IBOutlet weak var clinicTimeContentLabel: UILabel!
    @IBOutlet weak var underLine: UIView!
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var clinicStartTime: ClassLabelSizeClass!
    @IBOutlet weak var clinicEndTime: ClassLabelSizeClass!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        switch UIDevice().type {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C:
            self.cellHeightConstraint.constant = 64
        default:
            self.cellHeightConstraint.constant = 80
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
