//
//  DoctorsImgTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/7/2.
//

import UIKit

class DoctorsHeadShotTbCell: UITableViewCell {
    
    @IBOutlet weak var doctorsCollectionView: UICollectionView! 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
