//
//  HospitalDetailTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/3/4.
//

import UIKit
import Lottie

class ClinicRoomsTbCell: UITableViewCell {
    
    @IBOutlet weak var roomNameLabel: UILabel! {
        didSet {
            roomNameLabel.layer.shadowOffset = CGSize(width: 2, height: 0)
        }
    }
    @IBOutlet weak var roomCurrentNumLabel: UILabel!
    @IBOutlet weak var roomUpdateTimeLabel: UILabel!
    @IBOutlet weak var clickedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let clickedAnimationView = AnimationView(name: "clickedRoom")
        clickedAnimationView.frame = CGRect(x: 4, y: -18, width: 80, height: 80)
        clickedAnimationView.contentMode = .scaleAspectFit
        clickedAnimationView.animationSpeed = 0.5
        clickedAnimationView.loopMode = .loop
        clickedAnimationView.play()
        clickedView.addSubview(clickedAnimationView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
