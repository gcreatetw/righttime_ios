//
//  FavoriteTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/3/13.
//

import UIKit

class FavoriteListTbCell: UITableViewCell {
    
    @IBOutlet weak var favoriteImg: UIImageView!
    @IBOutlet weak var favoriteNameLabel: UILabel!
    @IBOutlet weak var favoriteAddressLabel: UILabel!
    @IBOutlet weak var favoritePhoneLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var closeMaskView: UIView! {
        didSet {
            closeMaskView.layer.cornerRadius = 10
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        favoriteImg.layer.cornerRadius = favoriteImg.frame.size.height/2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
