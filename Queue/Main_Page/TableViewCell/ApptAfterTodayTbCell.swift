//
//  ReservationNumTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/7/22.
//

import UIKit

class ApptAfterTodayTbCell: UITableViewCell {
    
    @IBOutlet weak var apptAfterTodayCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func reloadCollectionView() {
        apptAfterTodayCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
