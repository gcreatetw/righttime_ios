//
//  LatestNewsWithoutPicTbCell.swift
//  Queue
//
//  Created by Andrew on 2019/8/21.
//

import UIKit

class LatestNewsWithoutPicTbCell: UITableViewCell {
    
    @IBOutlet weak var clinicHeadShotImg: UIImageView! {
        didSet {
            clinicHeadShotImg.layer.cornerRadius = clinicHeadShotImg.frame.size.width / 2
        }
    }
    @IBOutlet weak var clinicNameLabel: UILabel!
    @IBOutlet weak var postedTimeLabel: UILabel!
    @IBOutlet weak var postedContentLabel: UILabel!
    
}
