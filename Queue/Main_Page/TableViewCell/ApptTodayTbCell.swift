//
//  HomeTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/3/8.
//

import UIKit

class ApptTodayTbCell: UITableViewCell {
    
    @IBOutlet weak var clinicNameLabel: UILabel!
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var curUpdateTimeLabel: UILabel!
    @IBOutlet weak var apptNumLabel: UILabel!
    @IBOutlet weak var curNumLabel: UILabel!
    @IBOutlet weak var expiredImageView: UIImageView!
    @IBOutlet weak var expiredMaskImg: UIImageView!
    
    @IBOutlet weak var maskContainerView: UIView!
    @IBOutlet weak var dayPartsLabel: UILabel!
    @IBOutlet weak var apptDateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var frame: CGRect {
        didSet {
            var newFrame = frame
            newFrame.origin.x += 10
            newFrame.size.width -= 10 * 2
            newFrame.origin.y += 10
            newFrame.size.height -= 10  * 1
            super.frame = newFrame  
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
