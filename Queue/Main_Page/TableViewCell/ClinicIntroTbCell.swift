//
//  ClinicIntroTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/7/10.
//

import UIKit
import SwiftyJSON

class ClinicIntroTbCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var clinicIntroContainer: UIView!
    @IBOutlet weak var clinicIntroContentCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var picsArray = [JSON]()
    var currentPage: Int = 0
    var screenWidth = CGFloat()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCollectionViewWith(picsArray: [JSON], screenWidth: CGFloat) {
        self.picsArray = picsArray
        self.clinicIntroContentCollection.isPagingEnabled = true
        self.clinicIntroContentCollection.dataSource = self
        self.clinicIntroContentCollection.delegate = self
        self.clinicIntroContentCollection.reloadData()
        self.screenWidth = screenWidth
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "clinicIntroCell", for: indexPath) as! ClinicIntroCollCell
        
        pageControl.numberOfPages = picsArray.count
        cell.clinicIntroImg.sd_setImage(with: URL(string: (picsArray[indexPath.row]["url"].rawString()!)), placeholderImage: UIImage(named: "doctor-banner"))
        cell.clinicIntroTitle.text = picsArray[indexPath.row]["title"].rawString()!
        cell.clinicIntroContent.text = picsArray[indexPath.row]["content"].rawString()!
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        //print("currentPage:Cell", currentPage)
        self.pageControl.currentPage = self.currentPage
    }
}

extension ClinicIntroTbCell: UICollectionViewDelegateFlowLayout {
    /// - Remark: 設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth-23, height: (screenWidth-23)/352*284)
    }
}
