//
//  RelativesTableViewCell.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/13.
//

import UIKit

class RelativesTableViewCell: UITableViewCell {
    @IBOutlet weak var relativesNameLabel: UILabel!
    @IBOutlet weak var relativesView: UIView!
    var isCheck = false
    override func awakeFromNib() {
        super.awakeFromNib()
        relativesView.layer.cornerRadius = relativesView.bounds.width/2
        relativesView.layer.borderWidth = 1
        relativesView.layer.borderColor = UIColor(named: "button_boder")?.cgColor
        relativesView.backgroundColor = UIColor.clear
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
