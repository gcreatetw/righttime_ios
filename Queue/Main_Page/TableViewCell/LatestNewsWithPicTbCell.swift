//
//  LatestNewsWithPictureTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/8/20.
//

import UIKit

class LatestNewsWithPicTbCell: UITableViewCell {
    
    @IBOutlet weak var clinicHeadShotImg: UIImageView!{
        didSet {
            clinicHeadShotImg.layer.cornerRadius = clinicHeadShotImg.frame.size.width / 2
        }
    }
    @IBOutlet weak var clinicNameLabel: UILabel!
    @IBOutlet weak var postedTimeLabel: UILabel!
    @IBOutlet weak var postedContentLabel: UILabel!
    @IBOutlet weak var postedPictureImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
