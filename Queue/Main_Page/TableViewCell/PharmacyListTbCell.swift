//
//  PharmacyListTbCell.swift
//  Queue
//
//  Created by Andrew on 2020/2/6.
//

import UIKit

class PharmacyListTbCell: UITableViewCell {


    @IBOutlet weak var moreInfoButton: UIButton! 
    @IBOutlet weak var adultMaskLabel: UILabel!
    @IBOutlet weak var childMaskLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var pharmacyName: ClassCellLabelSizeClass!
    @IBOutlet weak var pharmacyAddress: ClassCellLabelSizeClass!
    @IBOutlet weak var pharmacyPhone: ClassCellLabelSizeClass!
    @IBOutlet weak var pharmacyDistance: ClassCellLabelSizeClass!
    @IBOutlet weak var updateTimeLabel: ClassCellLabelSizeClass!
    //    @IBOutlet weak var phoneButton: UIButton! {
//        didSet {
//            phoneButton.layer.shadowColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1).cgColor
//            phoneButton.layer.shadowRadius = 2
//            phoneButton.layer.shadowOffset = CGSize(width: 2, height: 2)
//            phoneButton.layer.shadowOpacity = 0.25
//        }
//    }
//    @IBOutlet weak var addressButton: UIButton! {
//        didSet {
//            addressButton.layer.shadowColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1).cgColor
//            addressButton.layer.shadowRadius = 2
//            addressButton.layer.shadowOffset = CGSize(width: 2, height: 2)
//            addressButton.layer.shadowOpacity = 0.25
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var tapAction: ((UITableViewCell) -> Void)?
//    @IBAction func makePhoneCall(_ sender: UIButton) {
//        tapAction?(self)
//    }
    @IBAction func goToMoreInfoPage(_ sender: UIButton) {
        tapAction?(self)
    }
    //
//    var tapAction2: ((UITableViewCell) -> Void)?
//    @IBAction func navigationToPharmacy(_ sender: UIButton) {
//        tapAction2?(self)
//    }
    
}
