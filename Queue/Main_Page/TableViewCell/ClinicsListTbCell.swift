//
//  HospitalTableViewCell.swift
//  Queue
//
//  Created by Andrew on 2019/2/22.
//

import UIKit

class ClinicsListTbCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var clinicListImageView: UIImageView!
    @IBOutlet weak var clinicListName: UILabel!
    @IBOutlet weak var clinicListAddress: UILabel!
    @IBOutlet weak var clinicListPhone: UILabel!
    @IBOutlet weak var clinicListPhoneExtension: ClassCellLabelSizeClass!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var maskImgView: UIView!{
        didSet {
            maskImgView.layer.cornerRadius = 10
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        phoneStackView.setCustomSpacing(10, after: clinicListPhone)
        
        
        // Initialization code
    }
    
    override func layoutSubviews() {
        clinicListImageView.layer.cornerRadius = clinicListImageView.frame.size.height/2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
