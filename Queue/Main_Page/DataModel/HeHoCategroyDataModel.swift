//
//  CategroyDataModel.swift
//  Queue
//
//  Created by Andrew on 2020/9/13.
//

import Foundation
import SwiftyJSON

struct HeHoCategroyDataModel {

    var categoryArr: [HeHoCategroyData]
    
    struct HeHoCategroyData {
        var type_id: Int
        var name: String
        
        init(fromJson json: JSON) {
            self.type_id = json["type_id"].intValue
            self.name = json["name"].stringValue
        }
    }
 
    init(fromJson json: JSON) {
        self.categoryArr = json.arrayValue.map() {
            HeHoCategroyData(fromJson: $0)
        }
    }
}
