//
//  HeHoArticleDataModel.swift
//  Queue
//
//  Created by Andrew on 2020/9/13.
//

import Foundation
import SwiftyJSON

struct HeHoArticleDataModel {

    var articleArr: [HeHoArticleData]
    
    struct HeHoArticleData {
        var rowid: String
        var category: String
        var typeId: Int
        var postDate: String
        var postTitle: String
        var guid: String
        var feturedImage: String
        
        init(fromJson json: JSON) {
            self.rowid = json["rowid"].stringValue
            self.category = json["category"].stringValue
            self.typeId = json["type_id"].intValue
            self.postDate = json["post_date"].stringValue
            self.postTitle = json["post_title"].stringValue
            self.guid = json["guid"].stringValue
            self.feturedImage = json["fetured_image"].stringValue
        }
    }
 
    init(fromJson json: JSON) {
        self.articleArr = json.arrayValue.map() {
            HeHoArticleData(fromJson: $0)
        }
    }
}

