//
//  TabBarViewController.swift
//  Queue
//
//  Created by Andrew on 2019/3/14.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let hehoView = self.viewControllers![1] as! UINavigationController
        hehoView.popToRootViewController(animated: false)
        
        let favoriteView = self.viewControllers![2] as! UINavigationController
        favoriteView.popToRootViewController(animated: false)
    }
    
}

