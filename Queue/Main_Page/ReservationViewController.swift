//
//  ReservationViewController.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/13.
//

import UIKit
import Foundation
import EventKit
import EventKitUI

class ReservationViewController: UIViewController {
    let eventStore = EKEventStore()
    var time = Date()
    @IBOutlet weak var noVaccineView: UIView!
    var clinicName = ""
    @IBOutlet weak var clinicApptNameLabel: UILabel!
    @IBOutlet weak var ApptDatePicker: UIDatePicker!
    @IBOutlet weak var ApptScrollView: UIScrollView!
    @IBOutlet weak var ApptClickedBtn: UIButton!
    @IBOutlet weak var ApptView: UIView!
    @IBOutlet var ApptDetailView: UIView!
    @IBOutlet var ApptFinishedView: UIView!
    @IBOutlet weak var choseDoctorBtn: UIButton!
    @IBOutlet weak var choseTimeBtn: UIButton!
    @IBOutlet weak var choseSyncBtn: UIButton!
    @IBOutlet var dayButtons: [UIButton]!
    @IBOutlet var syncBtn: [UIButton]!
    @IBOutlet weak var patientBtn: UIButton!
    @IBOutlet weak var OwnExpenseView: UIView!
    @IBOutlet weak var OwnExpenseTbView: UITableView!
    @IBOutlet weak var ownPatientBtn: UIButton!
    @IBOutlet var VaccineFinishedView: UIView!
    var choseSickDay:String = ""
    //到時候要抓取Api的第一個值
    var choseTime:String = "09:00"
    //
    @IBOutlet var apptDateLabel: [UILabel]!
    @IBOutlet var apptTimeLabel: [UILabel]!
    @IBOutlet var apptClinicLabel: [UILabel]!
    @IBOutlet var apptDoctorLabel: [UILabel]!
    @IBOutlet var apptSymptomLabel: [UILabel]!
    @IBOutlet var apptSickDayLabel: [UILabel]!
    @IBOutlet var patientLabel: [UILabel]!
    @IBOutlet weak var apptPatientIdLabel: UILabel!
    @IBOutlet weak var newRelativesView: UIView!
    @IBOutlet weak var newNameTextField: UITextField!
    //
    @IBOutlet weak var vaccinePatientLabel: UILabel!
    @IBOutlet weak var ownExpenseCatLabel: UILabel!
    @IBOutlet weak var ownExpenseDateLabel: UILabel!
    var chooseCat:String = ""
    var isCollapse:Bool = false
    @IBOutlet weak var bottomView: UIView!
    //
    var ownExpenseDate:String = ""
    var ownExpenseTime:String = ""
    @IBOutlet weak var ownExpenseIdLabel: UILabel!
    //
    @IBOutlet weak var relativesBirthTextField: UITextField!
    @IBOutlet weak var relativesIdTextField: UITextField!
    
    @IBAction func unwindToReservation(_ unwindSegue: UIStoryboardSegue) {
        if let sourceViewController = unwindSegue.source as? RelativesViewController{
            if sourceViewController.patientName != ""{
                for i in patientLabel {
                    i.text = sourceViewController.patientName
                }
                vaccinePatientLabel.text = sourceViewController.patientName
            }
            isCollapse = sourceViewController.isAddBtn
            if isCollapse == true{
                newRelativesView.isHidden = false
            }
            else{
                newRelativesView.isHidden = true
            }
        }
        if let sourceViewController = unwindSegue.source as? ApptPickerViewController{
            if sourceViewController.judgmentlist == 0{
                choseDoctorBtn.titleLabel!.text = "     \(sourceViewController.doctorName)"
                choseDoctorBtn.setTitle("     \(sourceViewController.doctorName)", for: .normal)
                for i in apptDoctorLabel{
                    i.text = sourceViewController.doctorName + "醫生"
                }
            }
            else if sourceViewController.judgmentlist == 1{
                choseTimeBtn.titleLabel?.text = "     \(sourceViewController.timeperiod)"
                choseTimeBtn.setTitle("     \(sourceViewController.timeperiod)", for: .normal)
                for i in apptTimeLabel {
                    let tmpIndex = sourceViewController.timeperiod.index(sourceViewController.timeperiod.startIndex, offsetBy: 2)
                    let tmpTime = Int(sourceViewController.timeperiod.prefix(upTo: tmpIndex))
                    if tmpTime! > 12 {
                        i.text = "下午診\(sourceViewController.timeperiod)"
                    }
                    else if tmpTime > 17{
                        i.text = "晚上診\(sourceViewController.timeperiod)"
                    }
                    else{
                        i.text = "上午診\(sourceViewController.timeperiod)"
                    }
                }
                choseTime = sourceViewController.timeperiod
            }
            else if sourceViewController.judgmentlist == 2{
                choseSyncBtn.titleLabel?.text = "     \(sourceViewController.symptom)"
                choseSyncBtn.setTitle("     \(sourceViewController.symptom)", for: .normal)
                for i in apptSymptomLabel {
                    i.text = sourceViewController.symptom
                }
            }
        }
        if let sourceViewController = unwindSegue.source as? BirthDatePickerViewController{
            relativesBirthTextField.text = sourceViewController.dateString
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initAutoLayout()
        initApptDate()
        ApptScrollView.backgroundColor = UIColor(named: "bg_color")
        ApptDetailView.isHidden = true
        ApptFinishedView.isHidden = true
        VaccineFinishedView.isHidden = true
        newRelativesView.isHidden = true
        clinicApptNameLabel.text = clinicName
        for i in apptClinicLabel{
            i.text = clinicName
        }
        for i in dayButtons{
            i.layer.cornerRadius = i.bounds.width/2
            if i.tag != 0 {
                i.layer.borderWidth = 1
                i.layer.borderColor = UIColor(named: "button_boder")?.cgColor
                i.backgroundColor = UIColor.clear
            }
        }
        for j in syncBtn{
            j.layer.borderWidth = 1
            j.layer.borderColor = UIColor(named: "button_boder")?.cgColor
        }
        for i in apptDateLabel{
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "zh_Hant_TW")
            dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei")
            dateFormatter.dateFormat = "YYYY/MM/dd(E)"
            dateString = dateFormatter.string(from: Date())
            var clickedDate = ""
            for i in dateString{
                if i != "週"{
                    clickedDate.append(i)
                }
            }
            i.text = clickedDate
        }
        for i in apptSickDayLabel {
            i.text = "1天"
        }
        patientBtn.layer.borderWidth = 1
        patientBtn.layer.borderColor = UIColor(named: "button_boder")?.cgColor
        ownPatientBtn.layer.borderWidth = 1
        ownPatientBtn.layer.borderColor = UIColor(named: "button_boder")?.cgColor
        hideKeyboardWhenTappedAround()
    }
    func initApptDate(){
        let today = Date()
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        let dateFrom = calendar.startOfDay(for: today)
        let dateTo = calendar.date(byAdding: .day, value: 13, to: dateFrom)
        ApptDatePicker.minimumDate = dateFrom
        ApptDatePicker.maximumDate = dateTo
        ApptDatePicker.locale = Locale(identifier: "zh_Hant_TW")
    }
    var dateString: String = ""
    @IBAction func changeDate(_ sender: UIDatePicker) {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW")
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        dateFormatter.dateFormat = "YYYY/MM/dd(E)"
        dateString = dateFormatter.string(from: ApptDatePicker.date)
        var clickedDate = ""
        for i in dateString{
            if i != "週"{
                clickedDate.append(i)
            }
        }
        apptDateLabel[0].text = clickedDate
        apptDateLabel[1].text = clickedDate
    }
    
    func initAutoLayout(){
        ApptView.translatesAutoresizingMaskIntoConstraints = false
        ApptView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
    }
    //頁籤切換
    @IBAction func showViewClicked(_ sender: UIButton) {
        if sender.tag == 0 {
            ApptScrollView.isHidden = false
            OwnExpenseView.isHidden = true
            bottomView.isHidden = true
            newRelativesView.isHidden = true
        }
        else if sender.tag == 1{
            ApptScrollView.isHidden = true
            OwnExpenseView.isHidden = false
            bottomView.isHidden = false
            newRelativesView.isHidden = true
            if VaccineFinishedView.isHidden == false{
                OwnExpenseView.isHidden = true
                bottomView.isHidden = true
            }
        }
    }
    @IBAction func goApptPicker(_ sender: UIButton) {
        let userDefault = UserDefaults.standard
        if sender.tag == 0{
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.apptPickerPage) as? ApptPickerViewController {
                controller.judgmentlist = 0
                userDefault.set(choseDoctorBtn.title(for:.normal), forKey: "doctor_name")
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(controller, animated: true, completion: nil)
                }
        }
       else if sender.tag == 1{
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.apptPickerPage) as? ApptPickerViewController {
                controller.judgmentlist = 1
                userDefault.set(choseTimeBtn.title(for:.normal), forKey: "time_period")
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(controller, animated: true, completion: nil)
                }
        }
       else if sender.tag == 2{
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.apptPickerPage) as? ApptPickerViewController {
                controller.judgmentlist = 2
                userDefault.set(choseSyncBtn.title(for:.normal), forKey: "sync_name")
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(controller, animated: true, completion: nil)
                }
        }
    }
    @IBAction func goApptDetail(_ sender: UIButton) {
        ApptView.isHidden = true
        ApptDetailView.isHidden = false
        ApptScrollView.contentSize = CGSize(width: 0, height: 660)
        ApptScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    @IBAction func goNewRelatives(_ sender: UIButton) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.relativesPage) as?
            RelativesViewController {
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            controller.view.layer.speed = 0.5
            present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func backAppt(_ sender: UIButton) {
        ApptView.isHidden = false
        ApptDetailView.isHidden = true
        ApptScrollView.contentSize = CGSize(width: 0, height: 750)
        ApptScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    //還需要存掛號預約資料
    @IBAction func goApptFinished(_ sender: UIButton) {
        ApptDetailView.isHidden = true
        ApptFinishedView.isHidden = false
        ApptScrollView.contentSize = CGSize(width: 0, height: 700)
        ApptScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        let tmpPhone = hidePhoneNumber(number: apptPatientIdLabel.text!)
        apptPatientIdLabel.text = tmpPhone
    }
    
    @IBAction func daySelect(_ sender: UIButton) {
        dayButtons.forEach{ chooseBtn in
            chooseBtn.backgroundColor = UIColor.clear
            chooseBtn.layer.borderWidth = 1
            chooseBtn.layer.borderColor = UIColor(named: "button_boder")?.cgColor
        }
        sender.backgroundColor = UIColor(named: "main_color")
        sender.layer.borderWidth = 0
        for i in apptSickDayLabel {
            i.text = "\(sender.tag + 1)天"
        }
    }
    @IBAction func addCancel(_ sender: UIButton) {
        newRelativesView.isHidden = true
    }
    
    //還需要存親友資料
    @IBAction func addConfirm(_ sender: UIButton) {
        let id = relativesIdTextField.text
        
        let isTrue:Bool = false
        let checkId:Bool = checkID(source: id!, bornAfter20101225: isTrue)
        if checkId == true{
            newRelativesView.isHidden = true
        }
        else{
            //跳出警告視窗
            showAlert(msg: "身分證字號格式錯誤")
        }
    }
    //還需存疫苗預約資料
    @IBAction func goVaccineFinished(_ sender: UIButton) {
        guard !chooseCat.isEmpty else {
            showAlert(msg: "請選擇一種自費項目！")
            return
        }
        OwnExpenseView.isHidden = true
        bottomView.isHidden = true
        VaccineFinishedView.isHidden = false
        let tmpPhone = hidePhoneNumber(number: ownExpenseIdLabel.text!)
        ownExpenseIdLabel.text = tmpPhone
    }
    func showAlert(msg:String) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.chooseAlertPage) as?
            ChooseAlertViewController {
            controller.content = msg
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
    //加入行事曆
    @IBAction func syncCalendar(_ sender: UIButton) {
        eventStore.requestAccess( to: EKEntityType.event, completion:{(granted, error) in
            DispatchQueue.main.async {
                if (granted) && (error == nil) {
                    let event = EKEvent(eventStore: self.eventStore)
                    if self.ApptScrollView.isHidden == false{
                        event.title = "預約看診"
                        let dateFormatter: DateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "hh:mma"
                        let dateString = dateFormatter.string(from: Date())
                        print("choseTime = \(self.choseTime)")
                        let tmpIndex = self.choseTime.index(self.choseTime.startIndex, offsetBy: 2)
                        var tmpTime = Int(self.choseTime.prefix(upTo: tmpIndex))
                        let tmpTime2 = String(self.choseTime.suffix(from: tmpIndex))
                        print("tmpTime2 = \(tmpTime2)")
                        var apptTime:String = "09:00AM"
                        if tmpTime! >= 12{
                            tmpTime! = tmpTime! - 12
                            apptTime = "\(tmpTime ?? 0)\(tmpTime2)PM"
                        }
                        else{
                            apptTime = self.choseTime + "AM"
                        }
                        let formatter = DateFormatter()
                        formatter.dateFormat = "hh:mma"
                        let date1 = formatter.date(from: dateString)!
                        let date2 = formatter.date(from: apptTime)!
                        let elapsedTime = date2.timeIntervalSince(date1)
                        let hours = floor(elapsedTime / 60 / 60)
                        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
                        print("\(Int(hours)) hr and \(Int(minutes)) min")
                        event.startDate = self.ApptDatePicker.date.addingTimeInterval(TimeInterval(Int(hours) * 60 * 60 + Int(minutes) * 60 ))
                        event.endDate = event.startDate
                        event.notes = self.clinicName
                        event.calendar = self.eventStore.defaultCalendarForNewEvents
                    }
                    else{
                        event.title = "預約自費項目"
                        print("print == \(self.ownExpenseDate)")
                        print("print == \(self.ownExpenseTime)") //10:00
                        let sdate = DateUtil.shared.getDateFromString(withFormat: "yyyy/MM/dd, hh:mm", strDate: self.ownExpenseDate + "," + " " + self.ownExpenseTime )
                        print("print == \(sdate)")
                        event.startDate = sdate
                        event.endDate = sdate
                        event.calendar = self.eventStore.defaultCalendarForNewEvents
                    }
                    do {
                        try self.eventStore.save(event, span: .thisEvent)
                         } catch let error as NSError {
                            print("failed to save event with error : \(error)")
                         }
                    print("Saved Event")
                    self.showToast(controller: self, message: "已加入行事曆", seconds: 3)
                }
            }
        })
    }
    func showToast(controller: UIViewController, message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.layer.cornerRadius = 15
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    @IBAction func choseBirth(_ sender: UIButton) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.birthPickerPage) as? BirthDatePickerViewController {
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            present(controller, animated: true, completion: nil)
            }
    }
    func checkID(source: String, bornAfter20101225: Bool = false) -> Bool {
        /// 轉成小寫字母
        let lowercaseSource = source.lowercased()
        /// 檢查格式，是否符合 開頭是英文字母＋後面9個數字
        func validateFormat(str: String) -> Bool {
            let regex: String = "^[a-z]{1}[1-2]{1}[0-9]{8}$"
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES[c] %@", regex)
            return predicate.evaluate(with: str)
        }
        
        if validateFormat(str: lowercaseSource) {
            var cityAlphabets: [String: Int] = [
                "a":10,        "b":11,       "c":12,      "d":13,      "e":14,
                // 臺北市        臺中市        基隆市        臺南市        高雄市
                "f":15,        "g":16,       "h":17,      "i":34,      "j":18,
                // 新北市        宜蘭縣        桃園市        嘉義市        新竹縣
                "k":19,        "m":21,       "n":22,      "o":35,      "p":23,
                // 苗栗縣        南投縣        彰化縣        新竹市        雲林縣
                "q":24,        "t":27,       "u":28,      "v":29,      "w":32,
                // 嘉義縣        屏東縣        花蓮縣        臺東縣        金門縣
                "x":30,        "z":33
                // 澎湖縣        連江縣
            ]
            
            // 2010年12月25日 更新不再賦配代碼的區域，但是 2010 年前還是需要判斷。
            if !bornAfter20101225 {
                let notSupportCities = [
                    "l":20,     "r":25,   "s":26,    "y":31
                    // 台中縣    台南縣      高雄縣     陽明山管理區
                ]
                cityAlphabets.merge(notSupportCities) { current, _ in current }
            }
            
            /// 把 [Character] 轉換成 [Int] 型態
            let ints = lowercaseSource.compactMap{ Int(String($0)) }

            /// 拿取身分證第一位英文字母所對應當前城市的
            guard let key = lowercaseSource.first,
                let cityNumber = cityAlphabets[String(key)] else {
                return false
            }
     
            /// 經過公式計算出來的總和
            let firstNumberConvert = (cityNumber / 10) + ((cityNumber % 10) * 9)
            let section1 = (ints[0] * 8) + (ints[1] * 7) + (ints[2] * 6)
            let section2 = (ints[3] * 5) + (ints[4] * 4) + (ints[5] * 3)
            let section3 = (ints[6] * 2) + (ints[7] * 1) + (ints[8] * 1)
            let total = firstNumberConvert + section1 + section2 + section3

            /// 總和如果除以10是正確的那就是真的
            if total % 10 == 0 { return true }
        }
        
        return false
    }
    func hidePhoneNumber(number: String) -> String {
            if number.count < 5 {
                var str = ""
                for _ in 0 ..< number.count {
                    str += "*"
                }
                return str
            } else {
                //两个参数：替换的范围和用来替换的内容
                let start = number.index(number.startIndex, offsetBy: (number.count - 5) / 2)
                let end = number.index(number.startIndex, offsetBy: (number.count - 5) / 2 + 4)
                let range = Range(uncheckedBounds: (lower: start, upper: end))
                return number.replacingCharacters(in: range, with: "****")
            }
        }
}


extension ReservationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(VaccineTableViewCell.self)", for: indexPath) as? VaccineTableViewCell else { return UITableViewCell()}
        cell.selectionStyle = .none
        cell.vaccineClickedView.layer.cornerRadius = cell.vaccineClickedView.bounds.width/2
        cell.vaccineClickedView.layer.borderWidth = 1
        cell.vaccineClickedView.layer.borderColor = UIColor(named: "button_boder")?.cgColor
        cell.vaccineClickedView.backgroundColor = UIColor.clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = OwnExpenseTbView.cellForRow(at: indexPath) as? VaccineTableViewCell else { return }
        if cell.isCheck == false{
            cell.vaccineClickedView.layer.backgroundColor = UIColor(named: "main_color")?.cgColor
            cell.vaccineClickedView.layer.borderWidth = 0
            cell.isCheck = true
        }
        chooseCat = cell.vaccineCatLabel.text!
        ownExpenseDate = cell.vaccineDateLabel.text!
        ownExpenseTime = cell.vaccineTimeLabel.text!
        ownExpenseCatLabel.text = chooseCat
        ownExpenseDateLabel.text = "\(ownExpenseDate) \(ownExpenseTime)"
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = OwnExpenseTbView.cellForRow(at: indexPath) as? VaccineTableViewCell else { return }
        cell.vaccineClickedView.layer.backgroundColor = UIColor.clear.cgColor
        cell.vaccineClickedView.layer.borderWidth = 1
        cell.isCheck = false
    }
    
}
extension ReservationViewController: EKEventEditViewDelegate{
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        controller.dismiss(animated: true, completion: nil)
    }
}
//離開TextFiled收鍵盤
extension ReservationViewController{
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
           tap.cancelsTouchesInView = false
           view.addGestureRecognizer(tap)
       }
       
       @objc private func dismissKeyboard() {
           view.endEditing(true)
       }
}
