//
//  HomeViewController.swift
//  Queue
//
//  Created by Andrew on 2019/2/20.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreData
import CoreLocation
import UserNotifications
import StoreKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var apptListTableView: UITableView!
    @IBOutlet weak var clinicsTypeCollectionView: UICollectionView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var footerBg: UIView!
    @IBOutlet weak var headerBg: UIView!
    
    var categories = [
        clinicsType(imageView: "category_Otolaryngology", title: "耳鼻喉科", content: "Otolaryngology",city: "",type: "2"),
        clinicsType(imageView: "icon_pediatrics", title: "小兒科", content: "Pediatrics",city: "",type: "4"),
        clinicsType(imageView: "icon_gynecology", title: "婦產科", content: "Gynecology",city: "",type: "3"),
        clinicsType(imageView: "category_psychiatry", title: "身心科", content: "Psychiatrist",city: "",type: "8"),
        clinicsType(imageView: "category_Chinese", title: "中醫", content: "Chinese medicine",city: "",type: "1"),
        clinicsType(imageView: "category_Ophthalmology", title: "眼科", content: "Ophthalmology",city: "",type: "6"),
        clinicsType(imageView: "category_Skin", title: "皮膚科", content: "Dermatology",city: "",type: "7"),
        clinicsType(imageView: "category_internal", title: "內科", content: "Internal medicine",city: "",type: "10"),
        clinicsType(imageView: "icon_family-medicine", title: "家醫科", content: "Family medicine",city: "",type: "9"),
        clinicsType(imageView: "icon_dentistry", title: "牙科", content: "Dentistry",city: "",type: "5"),
        clinicsType(imageView: "icon_other", title: "其他", content: "Other",city: "",type: "11")]
    
    var currentPageNum = 0
    var readApptTodayList: [Appointment] = []
    var readApptAfterTodayList: [Appointment] = []
    
    /// - Remark:post to api
    var postRoomIDs: [Int16] = []
    
    /// - Remark:get from api
    var getRoomIDs: [JSON]?
    
    var refreshControl: UIRefreshControl!
    let locationManager = CLLocationManager()
    var curLat = Double()
    var curLon = Double()
    
    var sectionDataList: [String] = ["回診提醒", "今日就診", "選擇科別"]
    var isExpendDataList: [Bool] = [false, false, false]
    
    let bannerFromApp: [String] = ["Banner3","Banner2","Banner1","Banner4"]
    var bannerImageView: UIImageView!
    var bannerFromServer: [JSON]?
    var selectedClinicID = ""

    
    @IBAction func unwindToHomePage(segue: UIStoryboardSegue) {
        print("unwindToHomePage...")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        /// - Remark: 10 items add 160
        /// - Remark: 8 items iphoneX = 720 , iphone8 plus = 800
        
        switch UIDevice().type {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C:
            footerBg.frame.size.height = 940
            headerBg.frame.size.height = 80
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            footerBg.frame.size.height = 1050
            headerBg.frame.size.height = 90
        case .iPhone6plus, .iPhone6Splus, .iPhone7plus, .iPhone8plus, .iPhoneXSMax, .iPhoneXR, .iPhone11, .iPhone11Max:
            footerBg.frame.size.height = 1170
            headerBg.frame.size.height = 100
        case .iPhoneX, .iPhoneXS, .iPhone11Pro:
            footerBg.frame.size.height = 1050
            headerBg.frame.size.height = 100
        default:
            footerBg.frame.size.height = 1050
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sectionViewNib = UINib(nibName: "SectionView", bundle: nil)
        self.apptListTableView.register(sectionViewNib, forHeaderFooterViewReuseIdentifier: "SectionView")
        
        /// - Remark: applestore star func
        //StoreKitHelper.displayStoreKit()
        
        UserDefaults.standard.set(true, forKey: UserDefaultsID.isNotFirstOpenApp)
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            UserDefaults.standard.set("110", forKey: UserDefaultsID.cityID)
            UserDefaults.standard.set(false, forKey: UserDefaultsID.isCenter)
        }
        
        checkVersion()
        updateBannerImg()
        getTotalApptList()
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(HomeViewController.refreshHomePage), for: UIControl.Event.valueChanged)
        self.apptListTableView?.addSubview(self.refreshControl)
        
        Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(HomeViewController.getTotalApptList), userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.refreshHomePage), name: NSNotification.Name(rawValue: "refreshHomePage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.goToClinicDetailPage), name: NSNotification.Name(rawValue: "pushToClinicDetailPage"), object: nil)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getTotalApptList()
        checkNofityStatus()
        if readApptTodayList.count > 0 && UserDefaults.standard.string(forKey: UserDefaultsID.notifyStatus) == "denied" {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.notifyAlertPage) as?
                ShowNotifyAlertViewController {
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(controller, animated: true, completion: nil)
            }
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        checkNofityStatus()
    }
    
    @objc func goToClinicDetailPage() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicDetailPage) as? ClinicDetailViewController {
            controller.clinicID = self.selectedClinicID
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func checkNofityStatus() {
        if UserDefaults.standard.string(forKey: UserDefaultsID.notifyStatus) == "neverNotify" {
            /// - Remark: 永不提醒
        } else {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { (settings) in
                switch settings.authorizationStatus {
                case .notDetermined:
                    UserDefaults.standard.set("notDetermined", forKey: UserDefaultsID.notifyStatus)
                case .denied:
                    UserDefaults.standard.set("denied", forKey: UserDefaultsID.notifyStatus)
                case .authorized:
                    UserDefaults.standard.set("authorized", forKey: UserDefaultsID.notifyStatus)
                default:
                    print("")
                }
            })
        }
    }
    
    func checkVersion() {
        RestAPI.shared.getStoreVersion {(response)->Void in
            if (response != JSON.null) {
                var storeInfo = response["results"].array
                RestAPI.shared.getRightTimeVersion { (response)->Void in
                    if (response != JSON.null) {
                        var versionFromRightTime = response
                        switch versionFromRightTime["APP_iOS"]["isOpen"].rawString() {
                        case "false":
                            if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.serverStopPage) as?
                            ShowServerAlertViewController {
                                controller.content = response["APP_iOS"]["message"].string
                                controller.modalPresentationStyle = .fullScreen
                                self.present(controller, animated: true, completion: nil)
                            }
                        case "true":
                            let version = versionFromRightTime["APP_iOS"]["verion"].array
                            var versionArray = [String]()
                            for i in version! {
                                versionArray.append(i["version"].string!)
                            }
                            if let currentUserInstallVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                                if !versionArray.contains(currentUserInstallVersion) {
                                    if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.versionAlertPage) as?
                                        ShowVersionAlertViewController {
                                        /// - Remark: 小更新(不強制可選擇忽略)
                                        controller.isNeedUpdate = true
                                        controller.url = storeInfo![0]["trackViewUrl"].rawString()
                                        controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                        self.present(controller, animated: true, completion: nil)
                                    }
                                }
                            }
                        default:
                            print("default.....BENBEN")
                        }
                    }
                }
//                guard storeInfo != [] else { return }
//                if let currentUserInstallVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
//                    //                      print("currentUserInstallVersion",currentUserInstallVersion)
//                    //                      print("storeInfo",storeInfo![0]["version"].rawString())
//                    //                      print("reponse", response)
//
//                    /// - Remark: currentUserInstallVersion 抓 xcode 的 version 去跟 server 左邊 version 比
//                    let currentVersionArray = currentUserInstallVersion.components(separatedBy: ".")
//                    let storeVersionArray = storeInfo![0]["version"].rawString()?.components(separatedBy: ".")
//
//                    if (storeVersionArray![0] > currentVersionArray[0]) {
//                        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.versionAlertPage) as?
//                            ShowVersionAlertViewController {
//                            /// - Remark: 大更新(強制更新)
//                            /// - Remark: isNeedUpdate＝ true 強制升級版本 ＝false 不強制，有按鈕可以關掉畫面
//                            controller.isNeedUpdate = true
//                            controller.url = storeInfo![0]["trackViewUrl"].rawString()
//                            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//                            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
//                            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//                            self.present(controller, animated: true, completion: nil)
//                        }
//                    } else if (storeVersionArray![0] == currentVersionArray[0]) && (storeVersionArray![1] > currentVersionArray[1]) {
//                        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.versionAlertPage) as?
//                            ShowVersionAlertViewController {
//                            /// - Remark: 小更新(不強制可選擇忽略)
//                            controller.isNeedUpdate = false
//                            controller.url = storeInfo![0]["trackViewUrl"].rawString()
//                            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//                            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
//                            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//                            self.present(controller, animated: true, completion: nil)
//                        }
//                    }
//                }
            } else {
                print("error: \(response.error)")
            }
        }
    }
    
    func updateBannerImg() {
        let w: CGFloat = self.view.frame.size.width
        self.bannerScrollView.delegate = self
        RestAPI.shared.getBannerListcompletion { (response)->Void in
            if (response != JSON.null) {
                aPrint(response)
                self.bannerFromServer = response.array
                if self.bannerFromServer?.count > 0 {
                    self.bannerScrollView.contentSize = CGSize(width: w * CGFloat(response.count), height: self.headerBg.frame.size.height)
                    
                    for i in 0 ..< self.bannerFromServer!.count {
                        self.bannerImageView = UIImageView()
                        self.bannerImageView.frame = CGRect(x: w * CGFloat(i), y: 0, width: w, height: self.headerBg.frame.size.height)
                        var tempInt = i
                        if tempInt > self.bannerFromApp.count - 1 {
                            tempInt = 1
                        }
                        self.bannerImageView.sd_setImage(with: URL(string: (self.bannerFromServer![i]["url"].string!)), placeholderImage: UIImage(named: self.bannerFromApp[tempInt]))
                        self.bannerScrollView.addSubview(self.bannerImageView)
                    }
                    
                    let bannerTouch = UITapGestureRecognizer(target: self, action: #selector(self.bannerImg_Touch))
                    self.headerBg.addGestureRecognizer(bannerTouch)
                    self.headerBg.isUserInteractionEnabled = true
                } else {
                    for i in 0 ..< self.bannerFromApp.count {
                        self.bannerScrollView.contentSize = CGSize(width: w * CGFloat(self.bannerFromApp.count - 1), height: self.headerBg.frame.size.height)
                        self.bannerImageView = UIImageView()
                        self.bannerImageView.frame = CGRect(x: w * CGFloat(i), y: 0, width: w, height: self.headerBg.frame.size.height)
                        self.bannerImageView.image = UIImage(named: self.bannerFromApp[i])
                        self.bannerScrollView.addSubview(self.bannerImageView)
                    }
                }
            } else {
                for i in 0 ..< self.bannerFromApp.count {
                    self.bannerScrollView.contentSize = CGSize(width: w * CGFloat(self.bannerFromApp.count - 1), height: self.headerBg.frame.size.height)
                    self.bannerImageView = UIImageView()
                    self.bannerImageView.frame = CGRect(x: w * CGFloat(i), y: 0, width: w, height: self.headerBg.frame.size.height)
                    self.bannerImageView.image = UIImage(named: self.bannerFromApp[i])
                    self.bannerScrollView.addSubview(self.bannerImageView)
                }
            }
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(HomeViewController.onAutoChangeBannerAction), userInfo: nil, repeats: true)
        }
    }
    
    @objc func bannerImg_Touch() {
        guard let url = URL(string: self.bannerFromServer![self.currentPageNum]["link"].string!) else { return }
        UIApplication.shared.open(url)
    }
    
    @objc func onAutoChangeBannerAction() {
        bannerScrollView.setContentOffset(CGPoint(x: bannerScrollView.frame.size.width * CGFloat(self.currentPageNum), y: 0), animated: true)
        self.currentPageNum += 1
        if self.currentPageNum == self.bannerFromServer?.count && self.bannerFromServer?.count > 0 {
            self.currentPageNum = 0
        } else if self.currentPageNum == self.bannerFromApp.count {
            self.currentPageNum = 0
        }
    }
    
    @IBAction func goToQRcodePage(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.qRcodePage) as?
            QRcodeViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func goToTeachingPage(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.teachingPage) as?
            TeachingViewController {
            controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @objc func refreshHomePage() {
        getTotalApptList()
        self.refreshControl.endRefreshing()
    }
    
    /// - Remark: getNum from coreData and post to server
    @objc func getTotalApptList() {
        getApptTodayList()
        getApptAfterTodayList()
        
        if readApptTodayList.count == 0 && readApptAfterTodayList.count == 0 {
            sectionDataList = ["選擇科別"]
        } else if readApptTodayList.count == 0 && readApptAfterTodayList.count > 0 {
            sectionDataList = ["回診提醒", "選擇科別"]
        } else if readApptTodayList.count > 0 && readApptAfterTodayList.count == 0 {
            sectionDataList = ["今日就診", "選擇科別"]
        } else {
            sectionDataList = ["回診提醒", "今日就診", "選擇科別"]
        }
        
        RestAPI.shared.getTrackingInfo(roomNumArray: postRoomIDs){(response)->Void in
            if (response != JSON.null) {
                let json = JSON(response)
                self.getRoomIDs = json.array
                self.apptListTableView.reloadData()
            }
        }
    }
    
    func getApptAfterTodayList() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request:NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        calendar.locale = Locale(identifier: "zh_Hant_TW")
        let dateFrom = calendar.startOfDay(for: Date())
        let dateFromTomorrow = calendar.date(byAdding: .day, value: 1, to: dateFrom)
        let dateTo = calendar.date(byAdding: .day, value: 60, to: dateFrom)
        
        let tmp = readApptAfterTodayList.count
        
        request.predicate = NSPredicate(format: "apptDate BETWEEN {%@, %@}",argumentArray:[dateFromTomorrow, dateTo])
        do {
            readApptAfterTodayList = try context.fetch(request)
        } catch {
            print("error")
        }
        
        if tmp < readApptAfterTodayList.count {
            isExpendDataList[0] = true
        }
        
    }
    
    func getApptTodayList() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request:NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "Asia/Taipei")!
        calendar.locale = Locale(identifier: "zh_Hant_TW")
        let dateFrom = calendar.startOfDay(for: Date())
        //        let dateTo = calendar.date(byAdding: .day, value: 1, to: dateFrom)
        let dateTo = calendar.date(byAdding: .minute, value: 24*60-1, to: dateFrom)
        //        print("HomePage", dateFrom,dateTo)
        
        request.predicate = NSPredicate(format: "apptDate BETWEEN {%@, %@}",argumentArray:[dateFrom, dateTo])
        do {
            readApptTodayList = try context.fetch(request)
        } catch {
            print("error")
        }
        
        var tmpPostRoomIDs:[Int16] = []
        for i in readApptTodayList {
            tmpPostRoomIDs.append(i.roomID)
            postRoomIDs = tmpPostRoomIDs.removeDuplicates()
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionDataList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 && sectionDataList[0] == "回診提醒" {
            if self.isExpendDataList[0] {
                return 1
            } else {
                return 0
            }
        } else if section == 0 && sectionDataList[0] == "今日就診" {
            return readApptTodayList.count
        } else if section == 1 && sectionDataList[1] == "今日就診" {
            return readApptTodayList.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SectionView") as! SectionView
        
        sectionView.delegate = self
        sectionView.isExpand = self.isExpendDataList[section]
        sectionView.buttonTag = section
        sectionView.arrowButton.setImage(self.isExpendDataList[section] == true ? UIImage(named: "arrow-up") : UIImage(named: "arrow-down"), for: .normal)
        sectionView.sectionTitleLabel.text = self.sectionDataList[section]
        
        if section == 0 && sectionDataList[0] == "回診提醒" {
            sectionView.arrowButton.isHidden = false
            //            isExpendDataList[0] = false
            //            self.apptListTableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        } else {
            sectionView.arrowButton.isHidden = true
        }
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 && sectionDataList[0] == "回診提醒" {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellID.apptAfterTodayTableCell, for: indexPath) as! ApptAfterTodayTbCell
            cell.reloadCollectionView()
            return cell
        } else if indexPath.section == 0 && sectionDataList[0] == "今日就診" {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellID.apptTodayCell, for: indexPath) as! ApptTodayTbCell
            let tmp = readApptTodayList.count-1-indexPath.row
            let apptTodayList = readApptTodayList[tmp]
            
            cell.clinicNameLabel.text = apptTodayList.clinicName
            cell.roomNameLabel.text = apptTodayList.roomName
            //            cell.roomNameLabel.text?.removeLast()
            cell.apptNumLabel.text = apptTodayList.apptNum
            cell.curUpdateTimeLabel.text = DateUtil.shared.getStringFromDate(date: Date())
            
            RestAPI.shared.getTrackingInfo(roomNumArray: postRoomIDs){(response)->Void in
                if (response != JSON.null) {
                    let json = JSON(response)
                    self.getRoomIDs = json.array
                    /// - Precondition: server 切換的時候有時候 roomNum 在另外一會沒有這個號碼
                    if self.getRoomIDs!.count > 0 {
                        for i in 0 ... self.getRoomIDs!.count-1 {
                            if String(apptTodayList.roomID) == self.getRoomIDs?[i]["room_id"].rawString() {
                                apptTodayList.curNum = self.getRoomIDs?[i]["currentNum"].rawString()
                                cell.curNumLabel.text = apptTodayList.curNum
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.saveContext()
                            }
                        }
                    }
                }
            }
            return cell
        }
        
        if indexPath.section == 1 && sectionDataList[1] == "今日就診" {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellID.apptTodayCell, for: indexPath) as! ApptTodayTbCell
            let tmp = readApptTodayList.count-1-indexPath.row
            let apptTodayList = readApptTodayList[tmp]
            
            cell.clinicNameLabel.text = apptTodayList.clinicName
            cell.roomNameLabel.text = apptTodayList.roomName
            //            cell.roomNameLabel.text?.removeLast()
            cell.apptNumLabel.text = apptTodayList.apptNum
            cell.curUpdateTimeLabel.text = DateUtil.shared.getStringFromDate(date: Date())
            
            RestAPI.shared.getTrackingInfo(roomNumArray: postRoomIDs){(response)->Void in
                if (response != JSON.null) {
                    let json = JSON(response)
                    self.getRoomIDs = json.array
                    for i in 0 ... self.getRoomIDs!.count-1 {
                        if String(apptTodayList.roomID) == self.getRoomIDs?[i]["room_id"].rawString() {
                            apptTodayList.curNum = self.getRoomIDs?[i]["currentNum"].rawString()
                            cell.curNumLabel.text = apptTodayList.curNum
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.saveContext()
                        }
                    }
                }
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 0 && sectionDataList[0] == "今日就診") || (indexPath.section == 1 && sectionDataList[1] == "今日就診") {
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.apptSettingsPage) as?
                ApptSettingsViewController {
                let tmp = readApptTodayList.count-1-indexPath.row
                let apptTodayList = readApptTodayList[tmp]
                self.selectedClinicID = apptTodayList.clinicID!
                controller.clinicID = apptTodayList.clinicID
                controller.roomName = apptTodayList.roomName
                controller.roomCurNum = apptTodayList.curNum
                controller.roomID = Int(apptTodayList.roomID)
                controller.clinicName = apptTodayList.clinicName
                controller.isComeFromHome = true
                controller.clinicPicAddress = apptTodayList.clinicPicAddress
                controller.curDayPartsString = "M"
                controller.selectedDate = DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: apptTodayList.apptDate ?? Date())
                
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
                present(controller, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (indexPath.section == 0 && sectionDataList[0] == "今日就診") || (indexPath.section == 1 && sectionDataList[1] == "今日就診") {
            let Action = UIContextualAction(style: .normal, title: "") { (action, view, completionHandler) in
                let tmp = self.readApptTodayList.count-1-indexPath.row
                let apptToadyList = self.readApptTodayList[tmp]
                
                RestAPI.shared.postTrackingInfo(roomID: String(apptToadyList.roomID), deviceToken: firebaseToken, targetNum: "", notifyNum:"", period: "M", date: DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: apptToadyList.apptDate ?? Date())){(response)->Void in
                    if (response != JSON.null) {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let context = appDelegate.persistentContainer.viewContext
                        self.readApptTodayList.remove(at: tmp)
                        context.delete(apptToadyList)
                        appDelegate.saveContext()
                        self.getTotalApptList()
                        //                        self.apptListTableView.reloadData()
                    }
                }
                completionHandler(true)
            }
            Action.backgroundColor = UIColor(red: 255/255, green: 60/255, blue: 60/255, alpha: 1)
            Action.image = UIImage(named: "delete")
            return UISwipeActionsConfiguration(actions: [Action])
        } else {
            return .none
        }
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return readApptAfterTodayList.count
        } else {
            return categories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.apptAfterTodayCollectionCell, for: indexPath) as! ApptAfterTodayCollCell
            
            let tmp = self.readApptAfterTodayList.count-1-indexPath.row
            let apptAfterTodayList = readApptAfterTodayList[tmp]
            cell.apptClinicName.text = apptAfterTodayList.clinicName
            cell.apptClinicPicImg.sd_setImage(with: URL(string: (apptAfterTodayList.clinicPicAddress ?? "")), placeholderImage: UIImage(named: "page-icon"))
            cell.apptDate.text = DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: apptAfterTodayList.apptDate ?? Date())
            
            switch (apptAfterTodayList.apptDayParts) {
            case "M": cell.apptDayParts.text = "上午診"
            case "A": cell.apptDayParts.text = "下午診"
            case "E": cell.apptDayParts.text = "夜間診"
            default: print("default")
            }
            
            cell.apptClinicRoomName.text = apptAfterTodayList.roomName
            cell.apptNum.text = (apptAfterTodayList.apptNum ?? "") + "號"
            cell.tapAction = { (cell) in
                let tmp = self.readApptAfterTodayList.count-1-indexPath.row
                let apptAfterToadyList = self.readApptAfterTodayList[tmp]
                
                RestAPI.shared.postTrackingInfo(roomID: String(apptAfterToadyList.roomID), deviceToken: firebaseToken, targetNum: "", notifyNum:"", period: "M", date: DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: apptAfterToadyList.apptDate ?? Date())){(response)->Void in
                    if (response != JSON.null) {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let context = appDelegate.persistentContainer.viewContext
                        self.readApptAfterTodayList.remove(at: tmp)
                        context.delete(apptAfterToadyList)
                        appDelegate.saveContext()
                        self.getTotalApptList()
                    }
                }
            }
            
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 2, height: 2)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 0.24
            cell.layer.masksToBounds = false
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.clinicsTypeCell, for: indexPath) as! ClinicsTypeCollCell
            
            let Category = categories[indexPath.row]
            cell.CategoryTitle.text = Category.title
            cell.CategoryContent.text = Category.content
            cell.CategoryImageView.image = UIImage(named: Category.imageView)
            
            cell.backgroundColor = UIColor.white
            cell.layer.cornerRadius = 15
            cell.layer.shadowOpacity = 0.24
            cell.layer.shadowOffset = CGSize(width: 2, height: 5)
            cell.layer.shadowColor = UIColor.black.cgColor
        
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(categories[indexPath.row].type, forKey: UserDefaultsID.typeID)
        
        if collectionView.tag == 1 {
            if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.apptSettingsPage) as?
                ApptSettingsViewController {
                let tmp = readApptAfterTodayList.count-1-indexPath.row
                let apptAfterTodayList = readApptAfterTodayList[tmp]
                self.selectedClinicID = apptAfterTodayList.clinicID!
                controller.roomName = apptAfterTodayList.roomName
                controller.roomCurNum = apptAfterTodayList.curNum
                controller.roomID = Int(apptAfterTodayList.roomID)
                controller.clinicID = apptAfterTodayList.clinicID
                controller.clinicName = apptAfterTodayList.clinicName
                controller.isComeFromHome = true
                controller.clinicPicAddress = apptAfterTodayList.clinicPicAddress
                controller.curDayPartsString = "M"
                controller.selectedDate = DateUtil.shared.getStrFromDate(strFormat: "YYYY-MM-dd", date: apptAfterTodayList.apptDate ?? Date())
                
                controller.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
                present(controller, animated: true, completion: nil)
            }
        }
        
        if collectionView.tag == 2 {
//            if indexPath.row == 0 {
//                if let controller = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.pharmacyListPage) as?
//                           PharmacyListViewController {
//                           self.navigationController?.pushViewController(controller, animated: true)
//                       }
//            } else {
                if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicListPage) as?
                    ClinicListViewController {
                    controller.curLat = curLat
                    controller.curLon = curLon
                    navigationController?.pushViewController(controller, animated: true)
                }
//            }
        }
    }
}

// MARK: - 設定 CollectionView Cell 與 Cell 之間的間距、距確 Super View 的距離等等
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    /// - Remark: 設定 Collection View 距離 Super View上、下、左、下間的距離
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.tag == 1 {
            return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        } else {
            return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        }
    }
    
    /// - Remark: 設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1 {
            return CGSize(width: 120, height: 170)
        } else {
            return CGSize(width: (self.view.frame.size.width - 60) / 2 , height: (self.view.frame.size.width - 80) / 2)
        }
    }
    
    /// - Remark: 滑動方向為「垂直」的話即「上下」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    /// - Remark: 滑動方向為「垂直」的話即「左右」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension Array where Element:Equatable {
    /// - Remark: 去掉重複的元素
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
    
    mutating func remove(at indexs: [Int]) {
        guard !isEmpty else { return }
        let newIndexs = Set(indexs).sorted(by: >)
        newIndexs.forEach {
            guard $0 < count, $0 >= 0 else { return }
            remove(at: $0)
        }
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let curLocation:CLLocationCoordinate2D = manager.location!.coordinate
        curLat = curLocation.latitude
        curLon = curLocation.longitude
        locationAddress()
        locationManager.stopUpdatingLocation()
    }
    
    /// - Remark:for iOS 11.0
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ()) {
        let locale = Locale(identifier: "zh_TW")
        let loc: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
        if #available(iOS 11.0, *) {
            CLGeocoder().reverseGeocodeLocation(loc, preferredLocale: locale) { placemarks, error in
                guard let placemark = placemarks?.first, error == nil else {
                    UserDefaults.standard.removeObject(forKey: "AppleLanguages")
                    completion(nil, error)
                    return
                }
                completion(placemark, nil)
            }
        }}
    
    /// - Remark: CLGeocoder地理編碼 經緯度轉換地址位置
    func locationAddress() {
       var cityDict = ["台北": "110", "台中": "170", "基隆": "100", "台南": "230", "高雄": "240", "新北": "宜蘭","桃園": "130","嘉義": "210", "新竹縣": "150", "苗栗": "160", "南投": "190", "彰化": "180", "新竹市": "140", "雲林": "200", "屏東": "250", "花蓮": "270", "台東": "260", "金門": "300", "澎湖": "290", "連江": "310"]
        geocode(latitude: curLat, longitude: curLon) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            /// - Remark: 如果同意開定位又定不到位置，預設給台北（例如模擬器狀況）
            UserDefaults.standard.set("110", forKey: UserDefaultsID.cityID)
            /// you should always update your UI in the main thread
            DispatchQueue.main.async {
                for pharmacyCity in PharmacyList.addressDict.keys {
                    let startIndex = pharmacyCity.index(pharmacyCity.startIndex, offsetBy: 3)
                    let endIndex = pharmacyCity.index(pharmacyCity.startIndex, offsetBy: 4)
                    var tmpItme = pharmacyCity
                    tmpItme.removeSubrange(startIndex...endIndex)
                       
                    if placemark.description.contains(tmpItme) {
                        UserDefaults.standard.set(pharmacyCity, forKey: UserDefaultsID.pharmacyCity)
                        for j in PharmacyList.addressDict[pharmacyCity]! {
                            if placemark.description.contains(j) {
                                UserDefaults.standard.set(j, forKey: UserDefaultsID.pharmacyDistrict)
                            } else {
                                UserDefaults.standard.set(PharmacyList.addressDict[pharmacyCity]![0], forKey: UserDefaultsID.pharmacyDistrict)
                            }
                        }
                    }
                }
                ///  update UI here
                if let cityLists = cityLists {
                    for i in cityLists {
                        for j in cityDict {
                            if placemark.description.contains(j.key) && (i["name"].rawString()?.contains(j.key))! {
                                UserDefaults.standard.set(j.value, forKey: UserDefaultsID.cityID)
                                return
                            }
                        }
//                        if (placemark.description.contains("台北")) && (i["name"].rawString()?.contains("台北"))!{
//                            UserDefaults.standard.set("110", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("台中")) && (i["name"].rawString()?.contains("台中"))! {
//                            UserDefaults.standard.set("170", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("基隆")) && (i["name"].rawString()?.contains("基隆"))! {
//                            UserDefaults.standard.set("100", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("台南")) && (i["name"].rawString()?.contains("台南"))! {
//                            UserDefaults.standard.set("230", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("高雄")) && (i["name"].rawString()?.contains("高雄"))! {
//                            UserDefaults.standard.set("240", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("新北")) && (i["name"].rawString()?.contains("新北"))! {
//                            UserDefaults.standard.set("120", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("宜蘭")) && (i["name"].rawString()?.contains("宜蘭"))! {
//                            UserDefaults.standard.set("280", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("桃園")) && (i["name"].rawString()?.contains("桃園"))! {
//                            UserDefaults.standard.set("130", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("嘉義")) && (i["name"].rawString()?.contains("嘉義"))! {
//                            UserDefaults.standard.set("210", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("新竹縣")) && (i["name"].rawString()?.contains("新竹縣"))! {
//                            UserDefaults.standard.set("150", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("苗栗")) && (i["name"].rawString()?.contains("苗栗"))! {
//                            UserDefaults.standard.set("160", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("南投")) && (i["name"].rawString()?.contains("南投"))! {
//                            UserDefaults.standard.set("190", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("彰化")) && (i["name"].rawString()?.contains("彰化"))! {
//                            UserDefaults.standard.set("180", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("新竹市")) && (i["name"].rawString()?.contains("新竹市"))! {
//                            UserDefaults.standard.set("140", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("雲林")) && (i["name"].rawString()?.contains("雲林"))! {
//                            UserDefaults.standard.set("200", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("嘉義")) && (i["name"].rawString()?.contains("嘉義"))! {
//                            UserDefaults.standard.set("210", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("屏東")) && (i["name"].rawString()?.contains("屏東"))! {
//                            UserDefaults.standard.set("250", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("花蓮")) && (i["name"].rawString()?.contains("花蓮"))! {
//                            UserDefaults.standard.set("270", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("台東")) && (i["name"].rawString()?.contains("台東"))! {
//                            UserDefaults.standard.set("260", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("金門")) && (i["name"].rawString()?.contains("金門"))! {
//                            UserDefaults.standard.set("300", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("澎湖")) && (i["name"].rawString()?.contains("澎湖"))! {
//                            UserDefaults.standard.set("290", forKey: UserDefaultsID.cityID)
//                            return
//                        } else if (placemark.description.contains("連江")) && (i["name"].rawString()?.contains("連江"))! {
//                            UserDefaults.standard.set("310", forKey: UserDefaultsID.cityID)
//                            return
//                        }
                    }
                }
            }
        }
    }
}

extension HomeViewController: SectionViewDelegate {
    func sectionView(_ sectionView: SectionView, _ didPressTag: Int, _ isExpand: Bool) {
        self.isExpendDataList[didPressTag] = !isExpand
        self.apptListTableView.reloadSections(IndexSet(integer: didPressTag), with: .automatic)
    }
    
}


