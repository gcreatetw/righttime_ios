//
//  FavoriteViewController.swift
//  Queue
//
//  Created by Andrew on 2019/3/13.
//

import UIKit
import Lottie
import SwiftyJSON
import Alamofire

class FavoriteListViewController: UIViewController {
    
    @IBOutlet weak var favoriteTableView: UITableView!
    @IBOutlet weak var zeroFavoriteView: UIView!
    
    var readFavoriteInfos:[Favorite] = []
    var favoriteList:[JSON] = []
    var clinicImglist:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getFavoriteInfo()
        favoriteTableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(FavoriteListViewController.addBadge), name: NSNotification.Name(rawValue: "addBadge"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FavoriteListViewController.cancelBadge), name: NSNotification.Name(rawValue: "cancelBadge"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFavoriteInfo()
        favoriteTableView.reloadData()
        
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if let controller = segue.destination as? HospitalDetailViewController, let row = self.favoriteTableView.indexPathForSelectedRow?.row{
    //            let favorite = readFavoriteInfos[row]
    //            controller.clinicID = favorite.clinicID
    //            print("Andrew_clinicID",controller.clinicID)
    //        }
    //    }
    
    @objc func addBadge() {
        self.navigationController!.tabBarItem.badgeValue = "New"
    }
    
    @objc func cancelBadge() {
        self.navigationController!.tabBarItem.badgeValue = nil
    }
    
    @objc func getFavoriteInfo() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        do {
            readFavoriteInfos = try context.fetch(Favorite.fetchRequest())
        } catch {
            print("error")
        }
        self.navigationController!.tabBarItem.badgeValue = nil
        if readFavoriteInfos.count == 0 {
            zeroFavoriteView.isHidden = false
        } else {
            zeroFavoriteView.isHidden = true
        }
        
        for i in readFavoriteInfos {
            clinicImglist.append(i.clinicID!)
        }
        
        RestAPI.shared.postFavoriteList(clinics: clinicImglist.removeDuplicates()) { (response) in
            if (response != JSON.null) {
                print("response:",response)
            }
        }
    }
}

extension FavoriteListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return readFavoriteInfos.count
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.favoriteClinicsListCell, for: indexPath) as! FavoriteListTbCell
        //        cell.favoriteImg.layer.cornerRadius = cell.favoriteImg.frame.size.height / 2
        
        let favorite = readFavoriteInfos[indexPath.section]
        cell.favoriteAddressLabel.text = favorite.clinicAddress
        cell.favoriteNameLabel.text = favorite.clinicName
        cell.favoritePhoneLabel.text = favorite.clinicPhone
        cell.favoriteImg.sd_setImage(with: URL(string: (favorite.clinicPicAddress ?? "www")), placeholderImage: UIImage(named: "page-icon"))
        
        RestAPI.shared.getFavoriteList(clinic_id: clinicImglist ?? [""]) { (response)->Void in
            if (response != JSON.null) {
                self.favoriteList = response.array!
                if self.favoriteList.count > 0 {
                    for i in 0...self.favoriteList.count-1 {
                        if favorite.clinicID == self.favoriteList[i]["id"].rawString() {
                            
                            if self.favoriteList[i]["open"].rawString() == "true" {
                                cell.closeMaskView.isHidden = true
                            } else {
                                cell.closeMaskView.isHidden = false
                            }
                            // 如果有更新資料，刷新 core data 內最愛的內容
                            guard favorite.clinicUpdateTime == self.favoriteList[i]["updated_time"].rawString() else {
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                let context = appDelegate.persistentContainer.viewContext
                                favorite.clinicAddress = self.favoriteList[i]["address"].rawString()
                                favorite.clinicName = self.favoriteList[i]["name"].rawString()
                                favorite.clinicPhone = self.favoriteList[i]["contact"].rawString()
                                favorite.clinicUpdateTime = self.favoriteList[i]["updated_time"].rawString()
                                favorite.clinicPicAddress = self.favoriteList[i]["pic"].rawString()
                                appDelegate.saveContext()
                                tableView.reloadData()
                                return
                            }
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: StoryboardID.clinicDetailPage) as? ClinicDetailViewController {
            let favorite = readFavoriteInfos[indexPath.section]
            controller.clinicID = favorite.clinicID
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let Action = UIContextualAction(style: .normal, title: "") { (action, view, completionHandler) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            //            do {
            //                self.readFavoriteInfos = try context.fetch(Favorite.fetchRequest())
            //            }
            //            catch {
            //                print("error")
            //            }
            for i in self.readFavoriteInfos {
                if i.clinicName == self.readFavoriteInfos[indexPath.section].clinicName {
                    context.delete(i)
                    appDelegate.saveContext()
                }
            }
            completionHandler(true)
            self.getFavoriteInfo()
            tableView.reloadData()
        }
        Action.backgroundColor = UIColor(red: 255/255, green: 60/255, blue: 60/255, alpha: 1)
        Action.image = UIImage(named: "delete")
        return UISwipeActionsConfiguration(actions: [Action])
    }
    
}
