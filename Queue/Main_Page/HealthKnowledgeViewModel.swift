//
//  HealthKnowledgeViewModel.swift
//  Queue
//
//  Created by Andrew on 2020/9/10.
//

import Foundation
import UIKit
import SwiftyJSON
import PKHUD

protocol HealthKnowledgeViewModelDelegate: NSObject {
    func onFetchCategoryCompleted(json: JSON)
    func onFetchArticleCompleted(json: JSON)
    func onFetchFailed()
}

class HealthKnowledgeViewModel: NSObject {
    weak var delegate: HealthKnowledgeViewModelDelegate?
    weak var paretController: UIViewController?
    var isFetching = false
    
    func callHeHoCategory() {
        HUD.show(.progress)
        RestAPI.shared.getHeHoCategory { (response) in
            if response != JSON.null {
                self.delegate?.onFetchCategoryCompleted(json: response)
            }
            HUD.hide()
        }
    }
    
    func callHeHoArticle(type_id: String, before: String) {
        guard isFetching == false else { return }
        isFetching = true
        HUD.show(.progress)
        RestAPI.shared.getHeHoArticle(type_id: type_id, before: before) { (response) in
            if response != JSON.null {
                self.delegate?.onFetchArticleCompleted(json: response)
            }
            HUD.hide()
        }
        isFetching = false
    }
}
