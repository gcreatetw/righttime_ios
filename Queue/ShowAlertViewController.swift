//
//  ShowAlertViewController.swift
//  Queue
//
//  Created by Andrew on 2019/4/1.
//

import UIKit

class ShowAlertViewController: UIViewController {
    
    @IBOutlet weak var contentLabel: ClassLabelSizeClass!
    var content:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentLabel.text = content
        
    }
    @IBAction func doneClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name("popPage"), object: nil)
        });
    }
}


