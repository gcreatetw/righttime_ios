//
//  VaccineTableViewCell.swift
//  Queue
//
//  Created by Yang Nina on 2021/7/22.
//

import UIKit

class VaccineTableViewCell: UITableViewCell {

    @IBOutlet weak var vaccineCatLabel: UILabel!
    @IBOutlet weak var vaccineDateLabel: UILabel!
    @IBOutlet weak var vaccineTimeLabel: UILabel!
    @IBOutlet weak var vaccineCountLabel: UILabel!
    @IBOutlet weak var vaccineClickedView: UIView!
    var isCheck = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
